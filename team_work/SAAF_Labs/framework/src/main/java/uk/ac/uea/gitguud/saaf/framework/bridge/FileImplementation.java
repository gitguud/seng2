package uk.ac.uea.gitguud.saaf.framework.bridge;

import android.app.Activity;

import java.io.File;

/**
 * FileImplementation.java
 *
 * Created by Pavel Solodilov on 29/10/16.
 */
public abstract class FileImplementation {
    protected Activity activity;

    /**
     * Constructor.
     *
     * @param activity Activity that uses this implementation; used to access files with appropriate
     *                 context permissions.
     */
    protected FileImplementation(Activity activity) {
        this.activity = activity;
    }

    public abstract String read(String fileName);
    public abstract boolean write(String fileName, String contents);
}

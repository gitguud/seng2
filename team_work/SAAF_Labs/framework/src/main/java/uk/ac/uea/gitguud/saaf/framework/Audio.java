package uk.ac.uea.gitguud.saaf.framework;

public interface Audio {
    public Music createMusic(String file);

    public Sound createSound(String file);
}

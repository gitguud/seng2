package uk.ac.uea.gitguud.saaf.framework.implementation;

import java.util.ArrayList;
import java.util.List;

import android.view.MotionEvent;
import android.view.View;

import uk.ac.uea.gitguud.saaf.framework.Pool;
import uk.ac.uea.gitguud.saaf.framework.Input.TouchEvent;
import uk.ac.uea.gitguud.saaf.framework.Pool.PoolObjectFactory;

/**
 * SingleTouchHandler class is used to handle single touch events
 * Implements the TouchHandler interface
 */
public class SingleTouchHandler implements TouchHandler {
    private boolean isTouched;
    private int touchX;
    private int touchY;
    private Pool<TouchEvent> touchEventPool;
    private List<TouchEvent> touchEvents = new ArrayList<TouchEvent>();
    private List<TouchEvent> touchEventsBuffer = new ArrayList<TouchEvent>();
    private float scaleX;
    private float scaleY;

    /**
     * Constructor for SingleTouchHandler object
     * @param view current/active view
     * @param scaleX view width
     * @param scaleY view height
     */
    public SingleTouchHandler(View view, float scaleX, float scaleY) {
        PoolObjectFactory<TouchEvent> factory = new PoolObjectFactory<TouchEvent>() {
            @Override
            public TouchEvent createObject() {
                return new TouchEvent();
            }            
        };
        touchEventPool = new Pool<TouchEvent>(factory, 100);
        view.setOnTouchListener(this);

        this.scaleX = scaleX;
        this.scaleY = scaleY;
    }

    /**
     * Handle new single touch event
     * @param v current/active view
     * @param event object used to report movement events
     * @return true if new event has been successfully registered
     */
    @Override
    public boolean onTouch(View v, MotionEvent event) {
        synchronized(this) {
            TouchEvent touchEvent = touchEventPool.newObject();
            switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                touchEvent.type = TouchEvent.TOUCH_DOWN;
                isTouched = true;
                break;
            //A change has happened during a press gesture (between ACTION_DOWN and ACTION_UP)
            case MotionEvent.ACTION_MOVE:
                touchEvent.type = TouchEvent.TOUCH_DRAGGED;
                isTouched = true;
                break;
            case MotionEvent.ACTION_CANCEL:
            case MotionEvent.ACTION_UP:
                touchEvent.type = TouchEvent.TOUCH_UP;
                isTouched = false;
                break;
            }

            touchEvent.x = touchX = (int)(event.getX() * scaleX);
            touchEvent.y = touchY = (int)(event.getY() * scaleY);

            touchEventsBuffer.add(touchEvent);                        
            
            return true;
        }
    }

    /**
     * Accessor method for the state of a touch event
     * @param pointer pointer to the touch event
     * @return true if touch event found, false otherwise
     */
    @Override
    public boolean isTouchDown(int pointer) {
        synchronized(this) {
            if(pointer == 0)
                return isTouched;
            else
                return false;
        }
    }

    /**
     * Accessor method for the X coordinate of the touch event
     * @param pointer pointer to the touch event
     * @return the X coordinate of this touch event
     */
    @Override
    public int getTouchX(int pointer) {
        synchronized(this) {
            return touchX;
        }
    }

    /**
     * Accessor method for the Y coordinate of the touch event
     * @param pointer pointer to the touch event
     * @return the Y coordinate of this touch event
     */
    @Override
    public int getTouchY(int pointer) {
        synchronized(this) {
            return touchY;
        }
    }

    /**
     * Accessor method for the list of touch events
     * @return list of registered touch events
    */
    @Override
    public List<TouchEvent> getTouchEvents() {
        synchronized(this) {     
            int len = touchEvents.size();
            for( int i = 0; i < len; i++ )
                touchEventPool.free(touchEvents.get(i));
            touchEvents.clear();
            touchEvents.addAll(touchEventsBuffer);
            touchEventsBuffer.clear();
            return touchEvents;
        }
    }
}

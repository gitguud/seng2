package uk.ac.uea.gitguud.saaf.framework.strategy;

/**
 * HTMLStrategy.java
 *
 * Created by Pavel Solodilov on 29/10/16.
 */
public class HTMLStrategy extends FormatStrategy {

    /**
     * Formats the provided parameters as HTML.
     *
     * @param heading Heading of the text.
     * @param body Body of the text.
     * @return String in HTML format.
     */
    @Override
    public String format(String heading, String body) {
        return "<html>\n<head>\n<title>" + heading +
                "</title>\n\n<body>\n" + body +
                "\n</body>\n</html>";
    }
}

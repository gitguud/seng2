package uk.ac.uea.gitguud.saaf.framework.abstractfactory;

import android.app.Activity;

import uk.ac.uea.gitguud.saaf.framework.bridge.FileImplementation;
import uk.ac.uea.gitguud.saaf.framework.bridge.InternalFileImplementation;
import uk.ac.uea.gitguud.saaf.framework.strategy.FormatStrategy;
import uk.ac.uea.gitguud.saaf.framework.strategy.StringStrategy;

/**
 * StringInternalFactory.java
 *
 * Created by Pavel Solodilov on 29/10/16.
 */
public class StringInternalFactory extends AbstractStorageFactory {

    /**
     *
     * @param activity
     */
    public StringInternalFactory(Activity activity) {
        super(activity);
    }

    /**
     *
     * @return
     */
    @Override
    public FormatStrategy getStrategy() {
        return new StringStrategy();
    }

    /**
     *
     * @return
     */
    @Override
    public FileImplementation getFileImplementation() {
        return new InternalFileImplementation(this.activity);
    }
}
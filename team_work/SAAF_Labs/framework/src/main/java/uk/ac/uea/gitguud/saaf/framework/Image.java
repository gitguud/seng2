package uk.ac.uea.gitguud.saaf.framework;

import uk.ac.uea.gitguud.saaf.framework.Graphics.ImageFormat;

public interface Image {
    public int getWidth();
    public int getHeight();
    public ImageFormat getFormat();
    public void dispose();
}

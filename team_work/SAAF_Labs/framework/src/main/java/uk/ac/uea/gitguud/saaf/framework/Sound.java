package uk.ac.uea.gitguud.saaf.framework;

public interface Sound {
    public void play(float volume);

    public void dispose();

    public void stop();
}


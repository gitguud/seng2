package uk.ac.uea.gitguud.saaf.framework.bridge;

import android.app.Activity;
import android.content.Context;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * InternalFileImplementation.java
 *
 * Created by Pavel Solodilov on 29/10/16.
 */

public class InternalFileImplementation extends FileImplementation {

    /**
     * Constructor.
     *
     * @param activity Activity that uses this implementation; used to access files with appropriate
     *                 context permissions.
     */
    public InternalFileImplementation(Activity activity) {
        super(activity);
    }

    /**
     *
     * @param fileName
     * @return
     */
    @Override
    public String read(String fileName) {
        FileInputStream fis;
        try {
            fis = this.activity.openFileInput(fileName);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return "ERROR_FILE_NOT_FOUND";
        }

        InputStreamReader isr = new InputStreamReader(fis);
        BufferedReader bufferedReader = new BufferedReader(isr);
        StringBuilder sb = new StringBuilder();
        String line;

        try {
            while((line = bufferedReader.readLine()) != null) {
                sb.append(line);
            }
            return sb.toString();
        } catch (IOException e) {
            e.printStackTrace();
            return "ERROR_WHILE_READING_FILE";
        }
    } // end read

    /**
     *
     * @param fileName
     * @param contents
     * @return
     */
    @Override
    public boolean write(String fileName, String contents) {
        FileOutputStream fos;

        try {
            fos = this.activity.openFileOutput(fileName, Context.MODE_PRIVATE);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return false;
        }

        try {
            fos.write(contents.getBytes());
            fos.close();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    } // end write

}

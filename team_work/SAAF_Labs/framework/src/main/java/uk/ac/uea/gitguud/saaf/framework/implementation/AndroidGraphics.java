package uk.ac.uea.gitguud.saaf.framework.implementation;

import java.io.IOException;
import java.io.InputStream;

import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Rect;

import uk.ac.uea.gitguud.saaf.framework.Graphics;
import uk.ac.uea.gitguud.saaf.framework.Image;

/**
 * The AndroidGraphics class provides low level graphics tools
 * such as canvases, color filters, points, and rectangles
 * that let you handle drawing to the screen directly.
 * <p>
 *
 */

public class AndroidGraphics implements Graphics {
    AssetManager assets;
    Bitmap frameBuffer;
    Canvas canvas;
    Paint paint;
    Rect srcRect = new Rect();
    Rect dstRect = new Rect();

    /**
     *  Constructor for the AndroidGraphics object.
     *
     * @param assets Takes the assets for the graphics render.
     * @param frameBuffer Takes the bitmap buffer for putting the assets in.
     */

    public AndroidGraphics(AssetManager assets, Bitmap frameBuffer) {
        this.assets = assets;
        this.frameBuffer = frameBuffer;
        this.canvas = new Canvas(frameBuffer);
        this.paint = new Paint();
    }

    /**
     * Creates a new image from the file and the format provided.
     *
     * @param fileName The file that we use to create the image.
     * @param format The file format of type RGB 565 / ARGB 4444 / ARGB 8888
     *
     * @return a new AndroidImage object with the given properties.
     */

    @Override
    public Image newImage(String fileName, ImageFormat format) {
        Config config = null;
        if (format == ImageFormat.RGB565)
            config = Config.RGB_565;
        else if (format == ImageFormat.ARGB4444)
            config = Config.ARGB_4444;
        else
            config = Config.ARGB_8888;

        Options options = new Options();
        options.inPreferredConfig = config;
        
        
        InputStream in = null;
        Bitmap bitmap = null;
        try {
            in = assets.open(fileName);
            bitmap = BitmapFactory.decodeStream(in, null, options);
            if (bitmap == null)
                throw new RuntimeException("Couldn't load bitmap from asset '"
                        + fileName + "'");
        } catch (IOException e) {
            throw new RuntimeException("Couldn't load bitmap from asset '"
                    + fileName + "'");
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                }
            }
        }

        if (bitmap.getConfig() == Config.RGB_565)
            format = ImageFormat.RGB565;
        else if (bitmap.getConfig() == Config.ARGB_4444)
            format = ImageFormat.ARGB4444;
        else
            format = ImageFormat.ARGB8888;

        return new AndroidImage(bitmap, format);
    }

    /**
     *  Clears the screen using a given color.
     *
     * @param color The color for the screen to be cleared in.
     */

    @Override
    public void clearScreen(int color) {
        canvas.drawRGB((color & 0xff0000) >> 16, (color & 0xff00) >> 8,
                (color & 0xff));
    }

    /**
     * Draws a line from given coordinates to given coordinates in a given color.
     *
     * @param x The starting coordinate on the X-axis.
     * @param x2 The end coordinate on the X-axis.
     * @param y The starting coordinate on the Y-axis.
     * @param y2 The end coordinate on the Y-axis.
     * @param color The color for the line to be drawn in.
     */

    @Override
    public void drawLine(int x, int y, int x2, int y2, int color) {
        paint.setColor(color);
        canvas.drawLine(x, y, x2, y2, paint);
    }

    /**
     * Draws a rectangle from given coordinates, with a given height, width and color.
     *
     * @param x The coordinate for the start point on the X-axis.
     * @param y The coordinate for the start point on the Y-axis.
     * @param width The width of the rectangle.
     * @param height The height of the rectangle.
     * @param color The color of the rectangle to be written in.
     *
     */

    @Override
    public void drawRect(int x, int y, int width, int height, int color) {
        paint.setColor(color);
        paint.setStyle(Style.FILL);
        canvas.drawRect(x, y, x + width - 1, y + height - 1, paint);
    }

    /**
     * Fill the bitmap with the given A R G B color.
     *
     * @param a alpha component given.
     * @param r red component given
     * @param g green component given.
     * @param b blue component given.
     */

    @Override
    public void drawARGB(int a, int r, int g, int b) {
        paint.setStyle(Style.FILL);
       canvas.drawARGB(a, r, g, b);
    }


    /**
     * Draws text with the specified color at the specified starting point.
     *
     * @param text The text to be drawn on screen.
     * @param x The starting point on the X-axis.
     * @param y The starting point on the Y-axis.
     * @param paint The color for the text to be drawn in.
     */

    @Override
    public void drawString(String text, int x, int y, Paint paint){
        canvas.drawText(text, x, y, paint);
    }


    /**
     * Draws a rectangle and fills it with an image with the specified properties.
     *
     * @param Image The Image object to be drawn.
     * @param x The origin point on the X-axis.
     * @param y The origin point on the Y-axis.
     * @param srcX The rectangle's origin point on the X-axis.
     * @param srcY The rectangle's origin point on the Y-axis.
     * @param srcWidth The rectangle's width.
     * @param srcHeight The rectangle's height.
     */

    public void drawImage(Image Image, int x, int y, int srcX, int srcY,
            int srcWidth, int srcHeight) {
        srcRect.left = srcX;
        srcRect.top = srcY;
        srcRect.right = srcX + srcWidth;
        srcRect.bottom = srcY + srcHeight;
        
        
        dstRect.left = x;
        dstRect.top = y;
        dstRect.right = x + srcWidth;
        dstRect.bottom = y + srcHeight;

        canvas.drawBitmap(((AndroidImage) Image).bitmap, srcRect, dstRect,
                null);
    }

    /**
     * Draws an image with the specified properties.
     *
     * @param Image The Image object to be drawn.
     * @param x The origin point for the image on the X-axis.
     * @param y The origin point for the image on the Y-axis.
     */


    @Override
    public void drawImage(Image Image, int x, int y) {
        canvas.drawBitmap(((AndroidImage)Image).bitmap, x, y, null);
    }
    
    public void drawScaledImage(Image Image, int x, int y, int width, int height, int srcX, int srcY, int srcWidth, int srcHeight){
        
        
     srcRect.left = srcX;
        srcRect.top = srcY;
        srcRect.right = srcX + srcWidth;
        srcRect.bottom = srcY + srcHeight;
        
        
        dstRect.left = x;
        dstRect.top = y;
        dstRect.right = x + width;
        dstRect.bottom = y + height;
        
   
        
        canvas.drawBitmap(((AndroidImage) Image).bitmap, srcRect, dstRect, null);
        
    }
   
    @Override
    public int getWidth() {
        return frameBuffer.getWidth();
    }

    @Override
    public int getHeight() {
        return frameBuffer.getHeight();
    }
}

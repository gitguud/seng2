package uk.ac.uea.gitguud.saaf.framework.observer;

/**
 * ConcreteSubject.java
 *
 * Concrete implementation of the Subject class, allowing to notify all of the observers whenever
 * an update happens.
 * Created by Pavel Solodilov on 21/10/16.
 */
public class ConcereteSubject<T> extends Subject {
    private T state;

    /**
     * State accessor.
     *
     * @return State of the subject.
     */
    public T getState() {
        return this.state;
    }

    /**
     * State mutator.
     *
     * @param newState New state of the object.
     */
    public void setState(T newState) {
        this.state = newState;
        this.notifyObservers();
    }
}

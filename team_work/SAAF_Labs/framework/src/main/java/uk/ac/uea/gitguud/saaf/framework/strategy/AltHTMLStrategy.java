package uk.ac.uea.gitguud.saaf.framework.strategy;

/**
 * AltHTMLStrategy.java
 *
 * Created by Pavel Solodilov on 29/10/16.
 */

public class AltHTMLStrategy extends FormatStrategy {

    /**
     * Formats the provided parameters as HTML, putting the heading as text heading
     * instead of using it as the title.
     *
     * @param heading Heading of the text.
     * @param body Body of the text.
     * @return String in HTML format.
     */
    @Override
    public String format(String heading, String body) {
        return "<html>\n<head>\n<title>Test Title</title>\n\n<body>\n<h1>" + heading +
                "</h1>\n\n" + body +
                "\n</body>\n</html>";
    }
}

package uk.ac.uea.gitguud.saaf.framework.implementation;

import java.util.List;

import android.view.View.OnTouchListener;

import uk.ac.uea.gitguud.saaf.framework.Input.TouchEvent;

/**
 * Interface used to describe how to handle touch events
 * Extends OnTouchListener - interface definition for a callback to be invoked
 * when a touch event is dispatched to a view
 */
public interface TouchHandler extends OnTouchListener {
    public boolean isTouchDown(int pointer);
    
    public int getTouchX(int pointer);
    
    public int getTouchY(int pointer);
    
    public List<TouchEvent> getTouchEvents();
}

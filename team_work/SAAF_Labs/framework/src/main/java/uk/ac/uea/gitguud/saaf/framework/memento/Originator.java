package uk.ac.uea.gitguud.saaf.framework.memento;

/**
 * Originator.java
 *
 * This class provides facilities to save and load object states using the memento pattern.
 * Created by Pavel Solodilov on 21/10/16.
 */
public abstract class Originator {
    protected MementoState state;

    /**
     * Allows to set the state of the object.
     *
     * @param state New state to set.
     */
    public final void setState(MementoState state) {
        this.state = state;
    }

    /**
     * Allows to save the current state of the object into a memento.
     *
     * @return Saved state of the object.
     */
    public final Memento saveToMemento() {
        return new Memento(this.state);
    }

    /**
     * Allows to restore a previous state from a stored memento.
     *
     * @param m Memento to restore the object's state from.
     */
    public final void restoreFromMemento(Memento m) {
        this.state = m.getState();
    }
}

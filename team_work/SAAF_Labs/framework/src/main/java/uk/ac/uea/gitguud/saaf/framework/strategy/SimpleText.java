package uk.ac.uea.gitguud.saaf.framework.strategy;

import uk.ac.uea.gitguud.saaf.framework.abstractfactory.AbstractStorageFactory;
import uk.ac.uea.gitguud.saaf.framework.bridge.AbstractFile;
import uk.ac.uea.gitguud.saaf.framework.bridge.FileImplementation;

/**
 * SimpleText.java
 *
 * Created by Pavel Solodilov on 29/10/16.
 */
public class SimpleText extends AbstractFile{
    private String heading = "";
    private String body = "";

    private FormatStrategy formatStrategy = new StringStrategy();

    /**
     * Constructor.
     *
     */
    public SimpleText(AbstractStorageFactory factory) {
        super(factory.getFileImplementation());
        this.formatStrategy = factory.getStrategy();
    }

    /**
     * Setter for a heading.
     *
     * @param heading New heading value.
     */
    public void setHeading(String heading) {
        this.heading = heading;
    }


    /**
     * Setter for a body.
     *
     * @param body New body value.
     */
    public void setBody(String body) {
        this.body = body;
    }


    /**
     * Setter for format strategy.
     *
     * @param formatStrategy New format strategy to use.
     */
    public void setFormatStrategy(FormatStrategy formatStrategy) {
        this.formatStrategy = formatStrategy;
    }


    /**
     * Formats the simple text using the stored strategy.
     */
    public String formatOutput() {
        return this.formatStrategy.format(this.heading, this.body);
    }


    /**
     * Bridge pattern implementation.
     *
     * @param fileName File to write this simple text to.
     * @return True if the simple text was written, and false if it was not.
     */
    public boolean writeToFile(String fileName) {
        return this.write(fileName, this.formatOutput());
    }


    /**
     * Reads info from file into this object's body, resetting the heading.
     *
     * @param filename File to read from.
     * @return True if file was read from successfully.
     */
    public boolean readFromFile(String filename) {
        String fileContents = this.read(filename);
        this.heading = "";
        this.body = fileContents;

        return !fileContents.contains("ERROR_");
    }
}

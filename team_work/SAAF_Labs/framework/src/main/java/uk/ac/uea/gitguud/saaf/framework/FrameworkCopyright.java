package uk.ac.uea.gitguud.saaf.framework;

/**
 * FrameworkCopyright.java
 *
 * This class is used to provide copyright information for an app.
 * Created by Pavel Solodilov on 21/10/16.
 */
public abstract class FrameworkCopyright {
    private String copyrightText = "This application is based on the Simple Android Application" +
                                   "Framework. (c) University of east Anglia 2016";

    /**
     * Copyright accessor. Copyright text consists of the SAAF copyright and app-specific copyright.
     *
     * @return The copyright text of the app.
     */
    public final String getCopyright() {
        return this.copyrightText + "\n\n" + getAppCopyRight();
    }

    /**
     * Returns app-specific copyright.
     *
     * @return App-specific copyright string.
     */
    protected abstract String getAppCopyRight();
}

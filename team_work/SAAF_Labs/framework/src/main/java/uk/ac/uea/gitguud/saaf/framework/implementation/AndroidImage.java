package uk.ac.uea.gitguud.saaf.framework.implementation;

import android.graphics.Bitmap;

import uk.ac.uea.gitguud.saaf.framework.Image;
import uk.ac.uea.gitguud.saaf.framework.Graphics.ImageFormat;

/**
 * Class for creating an Image based on the bitmap
 * and format given for the constructor.
 */

public class AndroidImage implements Image {
    Bitmap bitmap;
    ImageFormat format;
    
    public AndroidImage(Bitmap bitmap, ImageFormat format) {
        this.bitmap = bitmap;
        this.format = format;
    }

    /**
     * @return the width of the bitmap.
     */
    @Override
    public int getWidth() {
        return bitmap.getWidth();
    }

    /**
     * @return the height of the bitmap.
     */
    @Override
    public int getHeight() {
        return bitmap.getHeight();
    }

    /**
     * @return the format of the image.
     */
    @Override
    public ImageFormat getFormat() {
        return format;
    }

    /**
     * Method for disposing of the bitmap.
     */
    @Override
    public void dispose() {
        bitmap.recycle();
    }      
}

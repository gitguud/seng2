package uk.ac.uea.gitguud.saaf.framework.implementation;

import java.io.IOException;

import android.app.Activity;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.media.AudioManager;
import android.media.SoundPool;

import uk.ac.uea.gitguud.saaf.framework.Audio;
import uk.ac.uea.gitguud.saaf.framework.Music;
import uk.ac.uea.gitguud.saaf.framework.Sound;


/**
 * AndroidAudio.java
 *
 * This class implements the Audio interface, and is used to control all of the audio aspects of the
 * framework, including setting the audio controls to 'music', loading the audio assets and managing
 * the sound pool.
 *
 * It is essentially is used to access the music resources from the app.
 */
public class AndroidAudio implements Audio {
    private AssetManager assets;
    private SoundPool soundPool;

    /**
     * Constructor, which sets the volume control stream to music stream of the provided activity.
     *
     * @param activity Activity to attach this audio controller to.
     */
    public AndroidAudio(Activity activity) {
        activity.setVolumeControlStream(AudioManager.STREAM_MUSIC);
        this.assets = activity.getAssets();
        this.soundPool = new SoundPool(20, AudioManager.STREAM_MUSIC, 0);
    }

    /**
     * Returns a Music object allowing to use it to play music.
     *
     * @param filename Name of the music file.
     * @return Music object.
     */
    @Override
    public Music createMusic(String filename) {
        try {
            AssetFileDescriptor assetDescriptor = assets.openFd(filename);
            return AndroidMusic.getInstance(assetDescriptor);
        } catch (IOException e) {
            throw new RuntimeException("Couldn't load music '" + filename + "'");
        }
    }

    /**
     * Returns a sound object allowing to use it to play the sound.
     *
     * @param filename Name of the sound file.
     * @return Sound object.
     */
    @Override
    public Sound createSound(String filename) {
        try {
            AssetFileDescriptor assetDescriptor = assets.openFd(filename);
            int soundId = soundPool.load(assetDescriptor, 0);
            return new AndroidSound(soundPool, soundId);
        } catch (IOException e) {
            throw new RuntimeException("Couldn't load sound '" + filename + "'");
        }
    }
}

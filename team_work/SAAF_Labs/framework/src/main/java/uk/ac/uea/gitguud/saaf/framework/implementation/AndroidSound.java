package uk.ac.uea.gitguud.saaf.framework.implementation;

import android.media.SoundPool;

import uk.ac.uea.gitguud.saaf.framework.Sound;

/**
 * The AndroidSound class implements the Sound interface,
 * and manages and plays audio resources for the application
 */
public class AndroidSound implements Sound {
    private int soundId;
    private SoundPool soundPool;

    /**
     *Constructor for a AndroidSound object
     * @param soundPool collection of samples that can be loaded into memory from a resource inside
     *                                                   the APK or from a file in the file system.
     * @param soundId id of the sound
     */
    public AndroidSound(SoundPool soundPool, int soundId) {
        this.soundId = soundId;
        this.soundPool = soundPool;
    }

    /**
     * Play the sound specified by the soundID.
     * @param volume left/right volume value (range = 0.0 to 1.0)
     */
    @Override
    public void play(float volume) {
        soundPool.play(soundId, volume, volume, 0, 0, 1);
    }

    /**
     * Stop the sound from playing.
     */
    @Override
    public void stop() {
        this.soundPool.stop(this.soundId);
    }

    /**
     *  Unloads the sound specified by the soundID.
     */
    @Override
    public void dispose() {
        soundPool.unload(soundId);
    }

}

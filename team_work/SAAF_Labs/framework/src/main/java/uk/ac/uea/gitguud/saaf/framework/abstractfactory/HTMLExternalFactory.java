package uk.ac.uea.gitguud.saaf.framework.abstractfactory;

import android.app.Activity;

import uk.ac.uea.gitguud.saaf.framework.bridge.ExternalFileImplementation;
import uk.ac.uea.gitguud.saaf.framework.bridge.FileImplementation;
import uk.ac.uea.gitguud.saaf.framework.strategy.FormatStrategy;
import uk.ac.uea.gitguud.saaf.framework.strategy.HTMLStrategy;

/**
 * HTMLExternalFactory.java
 *
 * Created by Pavel Solodilov on 29/10/16.
 */

public class HTMLExternalFactory extends AbstractStorageFactory {

    /**
     *
     * @param activity
     */
    public HTMLExternalFactory(Activity activity) {
        super(activity);
    }

    /**
     *
     * @return
     */
    @Override
    public FormatStrategy getStrategy() {
        return new HTMLStrategy();
    }

    /**
     *
     * @return
     */
    @Override
    public FileImplementation getFileImplementation() {
        return new ExternalFileImplementation(this.activity);
    }
}

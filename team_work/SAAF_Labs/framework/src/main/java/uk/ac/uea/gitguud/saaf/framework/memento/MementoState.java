package uk.ac.uea.gitguud.saaf.framework.memento;

/**
 * MementoState.java
 *
 * Contains a 'snapshot' of an app state in order to keep track of changes. It is useful to create
 * functionality like an undo mechanism.
 * Created by Pavel Solodilov on 21/10/16.
 */
public abstract class MementoState {
}

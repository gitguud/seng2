package uk.ac.uea.gitguud.saaf.framework.observer;

import java.util.HashSet;

/**
 * Subject.java
 *
 * This class provides facilities to work objects observing it.
 * Created by Pavel Solodilov on 21/10/16.
 */
public abstract class Subject {
    private HashSet<StateObserver> observers = new HashSet<>();

    /**
     * Attach an observer to this class. Duplicates will not be added.
     *
     * @param obs Observer to attach.
     */
    public final void attach(StateObserver obs) {
        if(!this.observers.contains(obs)) {
            this.observers.add(obs);
        }
    }

    /**
     * Detaches an observer from this class. If the provided observer does not observe the object,
     * this method call will be ignored.
     *
     * @param obs Observer to detach.
     */
    public final void detach(StateObserver obs) {
        if (this.observers.contains(obs)) {
            this.observers.remove(obs);
        }
    }

    /**
     * Notifies all of the observers of this object that an update has happened.
     */
    protected final void notifyObservers() {
        for (StateObserver obs : this.observers) {
            obs.update();
        }
    }
}

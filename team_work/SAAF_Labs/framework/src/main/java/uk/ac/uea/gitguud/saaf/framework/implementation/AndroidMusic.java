package uk.ac.uea.gitguud.saaf.framework.implementation;

import java.io.IOException;

import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.media.MediaPlayer.OnSeekCompleteListener;
import android.media.MediaPlayer.OnVideoSizeChangedListener;

import uk.ac.uea.gitguud.saaf.framework.Music;

/**
 * AndroidMusic.java
 *
 * Class for creating an AndroidMusic object with a media player inside,
 * to play and manipulate media within the Android framework.
 */
public class AndroidMusic implements Music, OnCompletionListener, OnSeekCompleteListener, OnPreparedListener, OnVideoSizeChangedListener {
    private static AndroidMusic instance;
    private MediaPlayer mediaPlayer;
    private boolean isPrepared = false;

    /**
     * Constructor that takes the file descriptor of an entry in the asset manager
     * and creates an AndroidMusic object.
     *
     * @param assetDescriptor Asset descriptor pointing to a music file.
     */
    private AndroidMusic(AssetFileDescriptor assetDescriptor) {
        this.load(assetDescriptor);
    }

    /**
     * Allows to access an instance of AndroidMusic object, which is single for the whole app.
     *
     * @param assetDescriptor Asset descriptor pointing to a music file.
     * @return Singleton instance of an AndroidMusic class.
     */
    public static AndroidMusic getInstance(AssetFileDescriptor assetDescriptor) {

        if (AndroidMusic.instance == null) {
            // Create an instance if not yet created.
            AndroidMusic.instance = new AndroidMusic(assetDescriptor);
        } else {
            // Load the new music file into the instance.
            AndroidMusic.instance.stop();
            AndroidMusic.instance.load(assetDescriptor);
        }

        return AndroidMusic.instance;
    }

    /**
     * Loads a music file from the provided asset file descriptor.
     *
     * @param assetDescriptor Asset descriptor pointing to a music file.
     */
    private void load(AssetFileDescriptor assetDescriptor) {
        this.mediaPlayer = new MediaPlayer();
        try {
            // Load the music file from the asset descriptor.
            this.mediaPlayer.setDataSource(assetDescriptor.getFileDescriptor(),
                                           assetDescriptor.getStartOffset(),
                                           assetDescriptor.getLength());
            this.mediaPlayer.prepare();
            this.isPrepared = true;

            // Set up listeners.
            this.mediaPlayer.setOnCompletionListener(this);
            this.mediaPlayer.setOnSeekCompleteListener(this);
            this.mediaPlayer.setOnPreparedListener(this);
            this.mediaPlayer.setOnVideoSizeChangedListener(this);
        } catch (Exception e) {
            throw new RuntimeException("Couldn't load music");
        }
    }

    /**
     * A method to dispose of the media player when it has finished
     */
    @Override
    public void dispose() {
    
         if (this.mediaPlayer.isPlaying()){
               this.mediaPlayer.stop();
                }
        this.mediaPlayer.release();
    }

    /**
     * @return true if the media player is looping, false otherwise.
     */
    @Override
    public boolean isLooping() {
        return mediaPlayer.isLooping();
    }

    /**
     * @return true if the media player is playing, false otherwise.
     */
    @Override
    public boolean isPlaying() {
        return this.mediaPlayer.isPlaying();
    }

    /**
     * @return true if the media player is stopped, false otherwise.
     */
    @Override
    public boolean isStopped() {
        return !isPrepared;
    }

    /**
     * Pauses the current media player (if anything's playing).
     */
    @Override
    public void pause() {
        if (this.mediaPlayer.isPlaying())
            mediaPlayer.pause();
    }


    /**
     * Method to start playing the media player. Checks to see if the player is already
     * playing, and if it is it returns true. If it is not, it prepares it and starts
     * playing, and returns true.
     */

    @Override
    public void play() {
        if (this.mediaPlayer.isPlaying())
            return;

        try {
            synchronized (this) {
                if (!isPrepared)
                    mediaPlayer.prepare();
                mediaPlayer.start();
            }
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Method to set the looping state of the media player.
     *
     * @param isLooping true if wanted the media player to loop, false otherwise.
     */

    @Override
    public void setLooping(boolean isLooping) {
        mediaPlayer.setLooping(isLooping);
    }

    /**
     * Sets the volume of the media player.
     *
     * @param volume float value for the amount of volume to be applied to the media player.
     */

    @Override
    public void setVolume(float volume) {
        mediaPlayer.setVolume(volume, volume);
    }

    /**
     * Method to stop the media player. First checks if it is playing, and if it is
     * it stops it, then sets the prepared state of the media player to false.
     */

    @Override
    public void stop() {
         if (this.mediaPlayer.isPlaying() == true){
        this.mediaPlayer.stop();
        
       synchronized (this) {
           isPrepared = false;
        }}
    }

    /**
     * Method called when the media player finishes playing the media.
     *
     * @param player the media player to check the completion on.
     */
    @Override
    public void onCompletion(MediaPlayer player) {
        synchronized (this) {
            isPrepared = false;
        }
    }

    /**
     * Seeks to the beginning of the media.
     */
    @Override
    public void seekBegin() {
        mediaPlayer.seekTo(0);
        
    }


    @Override
    public void onPrepared(MediaPlayer player) {
        // TODO Auto-generated method stub
         synchronized (this) {
               isPrepared = true;
            }
        
    }

    @Override
    public void onSeekComplete(MediaPlayer player) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void onVideoSizeChanged(MediaPlayer player, int width, int height) {
        // TODO Auto-generated method stub
        
    }
}

package uk.ac.uea.gitguud.saaf.framework.implementation;

import java.util.List;

import android.content.Context;
import android.os.Build.VERSION;
import android.view.View;

import uk.ac.uea.gitguud.saaf.framework.Input;

/**
 * Class for handling input that's being passed to the TouchHandler.
 *
 */

public class AndroidInput implements Input {    
    TouchHandler touchHandler;

    public AndroidInput(Context context, View view, float scaleX, float scaleY) {
        if(Integer.parseInt(VERSION.SDK) < 5) 
            touchHandler = new SingleTouchHandler(view, scaleX, scaleY);
        else
            touchHandler = new MultiTouchHandler(view, scaleX, scaleY);        
    }

    /**
     * Method for getting the state of a touch action.
     *
     * @param pointer the pointer location of the touch action.
     * @return true if the touch action is undergoing, false otherwise.
     */

    @Override
    public boolean isTouchDown(int pointer) {
        return touchHandler.isTouchDown(pointer);
    }

    /**
     * Method for getting the X-axis coordinate of the touch with the given pointer.
     *
     * @param pointer the pointer of the touch.
     * @return the integer position of the touch action on the X-axis.
     */

    @Override
    public int getTouchX(int pointer) {
        return touchHandler.getTouchX(pointer);
    }

    /**
     * Method for getting the Y-axis coordinate of the touch with the given pointer.
     *
     * @param pointer the pointer of the touch.
     * @return the integer position of the touch action on the Y-axis.
     */

    @Override
    public int getTouchY(int pointer) {
        return touchHandler.getTouchY(pointer);
    }

    /**
     * Method for getting a list of touch events.
     *
     * @return a list of TouchEvent objects containing the touch events.
     */

    @Override
    public List<TouchEvent> getTouchEvents() {
        return touchHandler.getTouchEvents();
    }
    
}

package uk.ac.uea.gitguud.saaf.framework.observer;

/**
 * ConcreteStateObserver.java
 *
 * This class is a concrete implementation of a state observer, allowing to subscribe to other
 * subjects and receive 'update notifications'.
 * Created by Pavel Solodilov on 21/10/16.
 */
public final class ConcreteStateObserver<T> extends StateObserver {
    private ConcereteSubject<T> mySubject;
    private StateObserverClient<T> myClient;

    /**
     * Constructor. This attaches this object to the provided subject.
     *
     * @param subject Subject to attach this observer to.
     * @param client Client to register with this observer; to notify when an update happens.
     */
    public ConcreteStateObserver(ConcereteSubject<T> subject, StateObserverClient<T> client) {
        this.mySubject = subject;
        this.mySubject.attach(this);
        this.myClient = client;
    }

    /**
     * Detaches the observer from the subject.
     */
    public void detach() {
        this.mySubject.detach(this);
    }

    /**
     * Signals the client of an update in the subject.
     */
    @Override
    public void update() {
        this.myClient.update(this.mySubject.getState());
    }
}

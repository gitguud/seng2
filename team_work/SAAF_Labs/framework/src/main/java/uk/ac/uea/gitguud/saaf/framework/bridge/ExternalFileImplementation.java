package uk.ac.uea.gitguud.saaf.framework.bridge;

import android.app.Activity;
import android.os.Environment;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * ExternalFileImplementation.java
 *
 * Created by Pavel Solodilov on 29/10/16.
 */
public class ExternalFileImplementation extends FileImplementation {
    boolean mExternalStorageAvailable = false;
    boolean mExternalStorageWriteable = false;

    /**
     * Constructor.
     *
     * @param activity Activity that uses this implementation; used to access files with appropriate
     *                 context permissions.
     */
    public ExternalFileImplementation(Activity activity) {
        super(activity);
    }


    /**
     *
     * @param fileName
     * @return
     */
    @Override
    public String read(String fileName) {
        this.updateExternalStorageState();
        if (!this.mExternalStorageAvailable) return "ERROR_EXTERNAL_STORAGE_NOT_AVAILABLE";

        File file = new File(this.activity.getExternalFilesDirs(null)[0], fileName); // HACK
        FileInputStream fis;

        try {
            fis = new FileInputStream(file);
        } catch (IOException e) {
            e.printStackTrace();
            return "ERROR_FILE_DOES_NOT_EXIST_ON_EXT";
        }

        InputStreamReader isr = new InputStreamReader(fis);
        BufferedReader bufferedReader = new BufferedReader(isr);
        StringBuilder sb = new StringBuilder();
        String line;

        try {
            while ((line = bufferedReader.readLine()) != null) {
                sb.append(line);
            }
            return sb.toString();
        } catch (IOException e) {
            e.printStackTrace();
            return "ERROR_WHILE_READING_FILE";
        }
    }


    /**
     *
     * @param fileName
     * @param contents
     * @return
     */
    @Override
    public boolean write(String fileName, String contents) {
        this.updateExternalStorageState();

        // Done if not writeable.
        if (!this.mExternalStorageWriteable) return false;

        // getExternalFilesDir(null) return the root of the external storage
        File file = new File(this.activity.getExternalFilesDir(null), fileName);
        FileOutputStream fos;

        try {
            fos = new FileOutputStream(file);
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }

        try {
            fos.write(contents.getBytes());
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    /**
     * Updates the state of the external storage to set the flags if it is available and writable.
     */
    public void updateExternalStorageState() {
        String state = Environment.getExternalStorageState();

        if (Environment.MEDIA_MOUNTED.equals(state)) {
            // We can read and write the media.
            this.mExternalStorageAvailable = true;
            this.mExternalStorageWriteable = true;
        } else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            // We can only read the media.
            this.mExternalStorageAvailable = true;
            this.mExternalStorageWriteable = false;
        } else {
            // Something is wrong and we can neither read nor write.
            this.mExternalStorageAvailable = false;
            this.mExternalStorageWriteable = false;
        }
    }
}

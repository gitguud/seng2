package uk.ac.uea.gitguud.saaf.framework.implementation;

import android.app.Activity;

import uk.ac.uea.gitguud.saaf.framework.Audio;
import uk.ac.uea.gitguud.saaf.framework.Sound;

/**
 * SoundResource.java
 *
 * Created by Pavel Solodilov on 16/10/16.
 */
public class SoundResource {
    private Audio myAudio;
    private Sound mySound;

    /**
     * Constructor.
     *
     * @param act Activity to associate the sound resource with in order to allow asset loading.
     */
    public SoundResource(Activity act) {
        this.myAudio = new AndroidAudio(act);
    }

    /**
     * Loads a sound file from the provided path.
     *
     * @param resourcePath Path to the audio file in the assets folder.
     */
    public void load(String resourcePath) {
        this.mySound = myAudio.createSound(resourcePath);
    }

    /**
     * Plays the loaded sound asset file.
     * The default volume is 90% of the maximum for both of the sound channels.
     *
     * @throws NullPointerException if no sound was loaded.
     */
    public void play() {
        this.mySound.play((float)0.9);
    }

    /**
     * Stops the playing sound file.
     *
     * @throws NullPointerException if no sound was loaded.
     */
    public void stop() {
        this.mySound.stop();
    }
}

package uk.ac.uea.gitguud.saaf.framework.observer;

/**
 * StateObserver.java
 *
 * This class provides facilities for another class to be a state observer of a subject in observer
 * pattern.
 * Created by Pavel Solodilov on 21/10/16.
 */
public abstract class StateObserver {

    /**
     * This method is called whenever the subject has to notify the observer of an update.
     */
    public abstract void update();
}

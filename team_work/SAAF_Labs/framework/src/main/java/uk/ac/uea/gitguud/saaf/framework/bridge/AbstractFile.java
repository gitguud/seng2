package uk.ac.uea.gitguud.saaf.framework.bridge;

/**
 * AbstractFile.java
 *
 * Created by Pavel Solodilov on 29/10/16.
 */
public abstract class AbstractFile {
    private FileImplementation fileImplementation;

    /**
     * Constructor.
     *
     * @param fileImplementation File implementation to use.
     */
    protected AbstractFile(FileImplementation fileImplementation) {
        this.fileImplementation = fileImplementation;
    }

    /**
     * Reads contents of a file.
     *
     * @param fileName Name of the file to read.
     * @return Contents of the file.
     */
    protected final String read(String fileName) {
        return this.fileImplementation.read(fileName);
    }

    /**
     * Writes provided contents into a file.
     *
     * @param fileName Name of the file to write into.
     * @param contents Contents to write into a file.
     * @return True if the file was successfully written to, and false otherwise.
     */
    protected final boolean write(String fileName, String contents) {
        return this.fileImplementation.write(fileName, contents);
    }

    /**
     * Setter for a file implementation.
     *
     * @param fileImplementation New file implementation to use.
     */
    public void setFileImplementation(FileImplementation fileImplementation) {
        this.fileImplementation = fileImplementation;
    }
}

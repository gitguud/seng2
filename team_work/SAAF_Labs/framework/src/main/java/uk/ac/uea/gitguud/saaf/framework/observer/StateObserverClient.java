package uk.ac.uea.gitguud.saaf.framework.observer;

/**
 * StateObserverClient.java
 *
 * This interface allows an object to receive updates from a subject by using a state observer.
 * Created by Pavel Solodilov on 21/10/16.
 */

public interface StateObserverClient<T> {

    /**
     * Notifies the client that an update has occurred.
     *
     * @param stateUpdate State of the update that happened.
     */
    public void update(T stateUpdate);

}

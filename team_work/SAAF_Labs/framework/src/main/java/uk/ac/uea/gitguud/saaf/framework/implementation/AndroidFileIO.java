package uk.ac.uea.gitguud.saaf.framework.implementation;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.os.Environment;
import android.preference.PreferenceManager;

import uk.ac.uea.gitguud.saaf.framework.FileIO;

/**
 * AndroidFileIO.java
 *
 * This class, implementing the FileIO interface, provides a uniform method to access different
 * storage resources and assets.
 *
 * It essentially encapsulates all of the Android file access methods.
 */
public class AndroidFileIO implements FileIO {
    Context context;
    AssetManager assets;
    String externalStoragePath;

    /**
     * Constructor attaching this object to the application context.
     *
     * @param context Context of the application.
     */
    public AndroidFileIO(Context context) {
        this.context = context;
        this.assets = context.getAssets();
        this.externalStoragePath = Environment.getExternalStorageDirectory()
                .getAbsolutePath() + File.separator;
    }

    /**
     * Reads an app asset and returns the result as a stream.
     *
     * @param file Name of the asset to load.
     * @return InputStream representing the loaded asset.
     * @throws IOException If file with the provided name does not exist.
     */
    @Override
    public InputStream readAsset(String file) throws IOException {
        return assets.open(file);
    }

    /**
     * Reads a file from storage and returns the result as a stream.
     *
     * @param file Name of the file to load.
     * @return InputStream representing the requested file.
     * @throws IOException If the requested file was not found or you have insufficient permissions
     *                     to access it.
     */
    @Override
    public InputStream readFile(String file) throws IOException {
        return new FileInputStream(externalStoragePath + file);
    }

    /**
     * Allows to write to a file in external memory with specified name, creating it if not
     * existent.
     *
     * @param file Name of the file to access.
     * @return OutputStream representing the opened file.
     * @throws IOException If the requested file was not found and could not be created, or could
     *                     not be accessed.
     */
    @Override
    public OutputStream writeFile(String file) throws IOException {
        return new FileOutputStream(externalStoragePath + file);
    }

    /**
     * Allows to get access to the shared preferences file of this application.
     *
     * @return Shared preferences handler for this application context.
     */
    public SharedPreferences getSharedPref() {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }
}

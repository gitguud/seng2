package uk.ac.uea.gitguud.saaf.framework.strategy;

/**
 * FormatStrategy.java
 *
 * Created by Pavel Solodilov on 29/10/16.
 */
public abstract class FormatStrategy {
    public abstract String format(String heading, String body);
}

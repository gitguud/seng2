package uk.ac.uea.gitguud.saaf.framework.memento;

import java.util.ArrayList;

/**
 * CareTaker.java
 *
 * Stores all of the memento states to allow implementation of undo functionality.
 * Created by Pavel Solodilov on 21/10/16.
 */
public class CareTaker {
    private ArrayList<Memento> savedStates = new ArrayList<Memento>();

    /**
     * Adds a memento to saved states.
     *
     * @param m Memento to add to saved states.
     */
    public void addMemento(Memento m) {
        this.savedStates.add(m);
    }

    /**
     * Allows to retrieve a memento state by index.
     *
     * @param index Index of the memento to return.
     * @return Memento stored at the provided index without removing it.
     */
    public Memento getMemento(int index) {
        return this.savedStates.get(index);
    }
}

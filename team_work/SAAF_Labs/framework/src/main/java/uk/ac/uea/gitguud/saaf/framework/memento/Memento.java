package uk.ac.uea.gitguud.saaf.framework.memento;

/**
 * Memento.java
 *
 * This class is used to keep information about a state of the app to implement an undo
 * functionality, for example.
 * Created by Pavel Solodilov on 21/10/16.
 */
public class Memento {
    private MementoState state;

    /**
     * Constructor.
     *
     * @param newState State of the app to save.
     */
    public Memento(MementoState newState) {
        this.state = newState;
    }


    /**
     * Accessor for the saved app state.
     *
     * @return Saved app state.
     */
    public MementoState getState() {
        return this.state;
    }
}

package uk.ac.uea.gitguud.saaf.framework.strategy;

/**
 * StringStrategy.java
 *
 * Created by Pavel Solodilov on 29/10/16.
 */
public class StringStrategy extends FormatStrategy {

    /**
     * Formats the provided parameters as plain text.
     *
     * @param heading Heading of the text.
     * @param body Body of the text.
     * @return String in plain text format.
     */
    @Override
    public String format(String heading, String body) {
        return heading + "\n\n" + body;
    }
}

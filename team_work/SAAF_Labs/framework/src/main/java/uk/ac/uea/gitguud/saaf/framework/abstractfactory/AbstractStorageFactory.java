package uk.ac.uea.gitguud.saaf.framework.abstractfactory;

import android.app.Activity;

import uk.ac.uea.gitguud.saaf.framework.bridge.FileImplementation;
import uk.ac.uea.gitguud.saaf.framework.strategy.FormatStrategy;

/**
 * AbstractStorageFactory.java
 *
 * Created by Pavel Solodilov on 29/10/16.
 */
public abstract class AbstractStorageFactory {
    protected Activity activity;

    public AbstractStorageFactory(Activity activity) {
        this.activity = activity;
    }

    public abstract FormatStrategy getStrategy();
    public abstract FileImplementation getFileImplementation();
}

package uk.ac.uea.gitguud.activityprogramapp.model;


import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

import static org.junit.Assert.*;

/**
 * Created by Katrina on 07/11/2016.
 */
public class SchoolTest {

    //school object with empty list of open days
    String name ="CMP";
    public School buildSchoolInstance() {
        return new School(name);
    }

    String text = "Welcome";
    //open day
    private Calendar cal = Calendar.getInstance();
    private Date openDayDate = createDate(2016, 11, 10);

    public Date createDate(int year, int month, int date) {
        this.cal.set(year, month, date);
        return this.cal.getTime();
    }

    public SchoolOpenDay buildOpenDayInstance() {
        return new SchoolOpenDay(openDayDate);
    }

    @Test
    public void testGetName() {
        System.out.println("Test getName()");

        School instance = buildSchoolInstance();
        assertEquals(name, instance.getName());
    }

    @Test
    public void testSetName() {
        System.out.println("Test setName()");
        School instance = buildSchoolInstance();
        String newName = "newName";
        instance.setName(newName);
        assertEquals(newName, instance.getName());
    }

    @Test(expected = NullPointerException.class)
    public void setNameAsNull (){
        School instance = buildSchoolInstance();
        instance.setName(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void setNameAsEmpty(){
        School instance = buildSchoolInstance();
        instance.setName("");
    }

    @Test
    public void testAddOpenDay() throws Exception {
        System.out.println("Test addOpenDay()");

        School instance = buildSchoolInstance();
        SchoolOpenDay day1 = buildOpenDayInstance();
        SchoolOpenDay day2 = buildOpenDayInstance();

        SchoolOpenDay [] openDaysAdd = new SchoolOpenDay[2];
        openDaysAdd[0] = day1;
        openDaysAdd[1] = day2;

        assertFalse(instance.addOpenDay(null));//should not be added
        assertTrue(instance.addOpenDay(day1));
        assertTrue(instance.addOpenDay(day2));

        assertArrayEquals(openDaysAdd, instance.getOpenDays());
    }

    @Test
    public void testGetOpenDays() throws Exception {
        System.out.println("Test getOpenDays()");

        School instance = buildSchoolInstance();
        SchoolOpenDay day1 = buildOpenDayInstance();
        SchoolOpenDay day2 = buildOpenDayInstance();

        SchoolOpenDay [] openDays=new SchoolOpenDay[2];
        openDays[0] = day1;
        openDays[1] = day2;

        instance.addOpenDay(day1);
        instance.addOpenDay(day2);

        assertArrayEquals(openDays, instance.getOpenDays());
    }

    @Test
    public void getText() throws Exception {
        School instance = buildSchoolInstance();
        assertEquals(text, instance.getText());
    }

    @Test
    public void addOpenDays() throws Exception {
        School instance = buildSchoolInstance();

        SchoolOpenDay day1 = buildOpenDayInstance();
        SchoolOpenDay day2 = buildOpenDayInstance();

        ArrayList<SchoolOpenDay> openDays2 = new ArrayList<SchoolOpenDay>();
        openDays2.add(day1);
        openDays2.add(day2);

        instance.addOpenDays(openDays2);

        assertEquals(openDays2,instance.getOpenDays());
    }

    @Test
    public void getOpenDaysAsArrayList() throws Exception {
        School instance = buildSchoolInstance();

        SchoolOpenDay day1 = buildOpenDayInstance();
        SchoolOpenDay day2 = buildOpenDayInstance();

        ArrayList<SchoolOpenDay> openDays2 = new ArrayList<SchoolOpenDay>();
        openDays2.add(day1);
        openDays2.add(day2);

        instance.addOpenDays(openDays2);
        assertEquals(openDays2, instance.getOpenDaysAsArrayList());
    }

    @Test
    public void equals() throws Exception {
        //????????????????????
        School instance = buildSchoolInstance();
        School instance2 = buildSchoolInstance();

        assertEquals(instance, instance2);
    }
}
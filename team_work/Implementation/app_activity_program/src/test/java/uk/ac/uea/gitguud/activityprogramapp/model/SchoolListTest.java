package uk.ac.uea.gitguud.activityprogramapp.model;

import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

/**
 * Created by Anastassia Segeda on 11/7/2016.
 */
public class SchoolListTest {
    @Test
    public void getInstance() throws Exception {

    }

    @Test
    public void path() throws Exception {

    }

    @Test
    public void clearSchools() throws Exception {

    }

    @Test
    public void testSetDefaultSchool() throws Exception {
        System.out.println("Test setDefaultSchool()");
        //not checking for null - it might be null for no default

        SchoolList instance = SchoolList.getInstance().clearSchools();
        School defaultSchool = new School("Default");

        instance.setDefaultSchool(defaultSchool);
        assertEquals(defaultSchool, instance.getDefaultSchool());
    }

    @Test
    public void testGetDefaultSchool() throws Exception {
        System.out.println("Test getDefaultSchool()");

        SchoolList instance = SchoolList.getInstance().clearSchools();
        School defaultSchool = new School("Default");

        instance.setDefaultSchool(defaultSchool);
        assertEquals(defaultSchool, instance.getDefaultSchool());
    }

    @Test
    public void testAddSchool() throws Exception {
        System.out.println("Test addSchool()");

        SchoolList instance = SchoolList.getInstance().clearSchools();
        School school1 = new School("school1");
        School school2 = new School("school2");
        assertTrue(instance.addSchool(school1));
        assertTrue(instance.addSchool(school2));
        assertFalse(instance.addSchool(null)); //should not be added

        //checking last added school
        int last = instance.getSchools().length-1;
        School result = instance.getSchools()[last];

        assertEquals(school2, result);
    }

    @Test
    public void testGetSchools() throws Exception {
        System.out.println("Test getSchools()");

        SchoolList instance = SchoolList.getInstance().clearSchools();
        School school1 = new School("school1");
        School school2 = new School("school2");
        instance.addSchool(school1);
        instance.addSchool(school2);

        School [] schools = new School[2];
        schools[0] = school1;
        schools[1] = school2;

        assertArrayEquals(schools, instance.getSchools());

    }

    @Test
    public void testUpdateEntries() throws Exception {
        System.out.println("Test updateEntries()");

        SchoolList instance = SchoolList.getInstance().clearSchools();
        School school1 = new School("school1"); instance.addSchool(school1);
        School school2 = new School("school2"); instance.addSchool(school2);
        School school3 = new School("school3"); instance.addSchool(school3);

        //new set of school information to add
        ArrayList<School> newSchools = new ArrayList<>();
        School newSchool1 = new School("newSchool1"); newSchools.add(newSchool1);
        //has the same name as one of the schools already contained in the list of schools
        School newSchool2 = new School("school2"); newSchools.add(newSchool2);
        School newSchool3 = new School("newSchool3"); newSchools.add(newSchool3);

        instance.updateEntries(newSchools);

        ArrayList<School> exp = new ArrayList<>();
        exp.add(school1); exp.add(school3);
        exp.add(newSchool1); exp.add(newSchool2); exp.add(newSchool3);

        assertArrayEquals(exp.toArray(),instance.getSchools());
    }

    @Test
    public void getSchoolsAsArrayList() throws Exception {
        SchoolList instance = SchoolList.getInstance();

        School school = new School("school 1");
        assertEquals(school,instance.getSchoolsAsArrayList());
    }
}
package uk.ac.uea.gitguud.activityprogramapp.model;

import android.location.Location;

import org.junit.Test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Date;
import  java.util.Calendar;

import uk.ac.uea.gitguud.framework.convenience.TimeSpan;

/**
 * Created by Anastassia Segeda on 11/7/2016.
 */
public class SchoolOpenDayTest {


    @Test
    public void readFromParcel() throws Exception {

    }

    @Test
    public void describeContents() throws Exception {

    }

    @Test
    public void writeToParcel() throws Exception {

    }

    //open day
    private Calendar cal = Calendar.getInstance();
    private Date openDayDate = createDate(2016, 11, 10);

    public Date createDate(int year, int month, int date) {
        this.cal.set(year, month, date);
        return this.cal.getTime();
    }

    public SchoolOpenDay buildOpenDayInstance() {
        return new SchoolOpenDay(openDayDate);
    }

    //school event
    private Date start = createEventDate(2016, 11, 10, 10, 30);
    private Date end = createEventDate(2016, 11, 10, 11, 30);

    public Date createEventDate(int year, int month, int date, int hour, int minutes) {
        this.cal.set(year, month, date, hour, minutes);
        return this.cal.getTime();
    }

    private Location location = new Location("Location");
    private String locationString = "JSC 2.01";
    private String title = "Event title";
    private String description = "Event description";

    public SchoolEvent buildEventInstance() {
        return new SchoolEvent
                (title, location, locationString, new TimeSpan(start, end), description);
    }


    @Test
    public void testSetDate() throws Exception {
        System.out.println("Test setDate()");

        SchoolOpenDay  openDay = buildOpenDayInstance();
        assertEquals(openDayDate,openDay.getDate());
    }

    @Test(expected = NullPointerException.class)
    public void setDateAsNull(){
        SchoolOpenDay openDay = buildOpenDayInstance();
        openDay.setDate(null);
    }

    @Test
    public void testGetDate() throws Exception {
        System.out.println("Test getDate()");

        SchoolOpenDay  openDay = buildOpenDayInstance();
        assertEquals(openDayDate, openDay.getDate());;
    }

    @Test
    public void testAddSchoolEvent() throws Exception {
        System.out.println("Test addSchoolEvent()");

        SchoolEvent event1 = buildEventInstance();
        SchoolOpenDay openDay = buildOpenDayInstance();

        assertTrue(openDay.addSchoolEvent(event1));
        assertFalse(openDay.addSchoolEvent(null));//should not be added

        //get the last event added to this open day, should not be null
        int last = openDay.getSchoolEvents().length-1;
        SchoolEvent exp = openDay.getSchoolEvents()[last];

        assertEquals(event1,exp);
    }

    @Test

    public void testGetSchoolEvents() throws Exception {
        System.out.println("Test getSchoolEvents()");

        SchoolEvent event1 = buildEventInstance();
        SchoolEvent event2 = buildEventInstance();
        SchoolOpenDay openDay = buildOpenDayInstance();

        openDay.addSchoolEvent(event1);
        openDay.addSchoolEvent(event2);

        SchoolEvent [] expResult = new SchoolEvent[2];
        expResult[0]=event1; expResult[1]=event2;

        assertArrayEquals(expResult,openDay.getSchoolEvents());
    }

    @Test
    public void getText() throws Exception {
        System.out.println("Test getText()");
        SchoolOpenDay  openDay = buildOpenDayInstance();
        //2016, 11, 10?
        String exp = "10 December 2016";
        assertEquals(exp,openDay.getText());
    }

    @Test
    public void getEventsAsArrayList(){
        System.out.println("Test getEventsAsArrayList()");

       // return new ArrayList<>(this.SCHOOL_EVENTS);
        SchoolEvent event1 = buildEventInstance();
        SchoolEvent event2 = buildEventInstance();
        SchoolOpenDay openDay = buildOpenDayInstance();
        openDay.addSchoolEvent(event1);
        openDay.addSchoolEvent(event2);

        ArrayList<SchoolEvent> exp = new ArrayList<>();
        exp.add(event1); exp.add(event2);

        if(exp==openDay.getEventsAsArrayList()){
            fail("Should have copied the array list");
        }

        assertEquals(exp,openDay.getEventsAsArrayList());
    }
}
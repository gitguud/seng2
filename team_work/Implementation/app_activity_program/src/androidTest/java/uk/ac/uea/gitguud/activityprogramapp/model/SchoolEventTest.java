package uk.ac.uea.gitguud.activityprogramapp.model;

import org.junit.Test;

import static org.junit.Assert.*;

import android.location.*;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

import uk.ac.uea.gitguud.activityprogramapp.model.School;
import uk.ac.uea.gitguud.activityprogramapp.model.SchoolEvent;
import uk.ac.uea.gitguud.framework.convenience.TimeSpan;

/**
 * Created by Katrina on 07/11/2016.
 */
public class SchoolEventTest {
    /**
     * In order to test the Location class is working in the expected way,
     * these tests must be run using a mocked version, with an emulator.
     */
    private Calendar cal = Calendar.getInstance();

    private Date start = createDate(2016, 11, 10, 10, 30);
    private Date end = createDate(2016, 11, 10, 11, 30);

    private TimeSpan time = new TimeSpan(start,end);

    private Location location = createLocation("loc1", 10, 10, 10);
    private String locationString = "JSC 2.01";

    private String title = "Introduction Talk";
    private String description = "A small talk to introduce UEA to prospective students.";

    public Date createDate(int year, int month, int date, int hour, int minutes) {
        this.cal.set(year, month, date, hour, minutes);
        return this.cal.getTime();
    }

    private Location createLocation(String provider, double altitude, double longitude,
                                    double latitude) {
        Location location = new Location(provider);
        location.setAltitude(altitude);
        location.setLatitude(latitude);
        location.setLongitude(longitude);
        return location;
    }

    public SchoolEvent buildInstance() {
        return new SchoolEvent
                (title, location, locationString, new TimeSpan(start, end), description);
    }

    private boolean equalLocations(Location loc1, Location loc2) {
        if (loc1.getAltitude() != loc2.getAltitude()) {
            return false;
        } else if(loc1.getLatitude() != loc2.getLatitude()){
            return false;
        } else if (loc1.getLongitude() != loc2.getLongitude()){
            return false;
        }
        return true;
    }


    // ------------------------------------ ACCESSOR TESTS -------------------------------------- //

    @Test
    public void testGetLocation() throws Exception {
        System.out.println("Test getLocation()");
        SchoolEvent instance = buildInstance();

        instance.setLocation(location, locationString);
        assertTrue(equalLocations(location, instance.getLocation()));
    }

    @Test
    public void testGetLocationString() throws Exception {
        System.out.println("Test getLocationString()");
        SchoolEvent instance = buildInstance();

        assertEquals(locationString, instance.getLocationString());
    }

    @Test
    public void testGetStartTime() throws Exception {
        System.out.println("Test getStartTime()");
        SchoolEvent instance = buildInstance();

        if (start == instance.getStartTime()) {
            fail("Should have copied the location object.");
        }
        assertEquals(start, instance.getStartTime());
    }

    @Test
    public void testGetEndTime() throws Exception {
        System.out.println("Test getEndTime()");
        SchoolEvent instance = buildInstance();

        if (end == instance.getEndTime()) {
            fail("Should have copied the location object.");
        }
        assertEquals(end, instance.getEndTime());
    }

    @Test
    public void testGetTitle() throws Exception {
        System.out.println("Test getTitle()");
        SchoolEvent instance = buildInstance();

        assertEquals(title, instance.getTitle());
    }

    @Test
    public void testGetDescription() {
        System.out.println("Test getDescription()");
        SchoolEvent instance = buildInstance();

        assertEquals(description, instance.getDescription());
    }


    @Test
    public void getTimeSpan() throws Exception {
        System.out.println("Test getTimeSpan()");
        SchoolEvent instance = buildInstance();
        assertEquals(time,instance.getTimeSpan());
    }
    // ------------------------------------ MUTATOR TESTS --------------------------------------- //

    @Test
    public void setLocation() throws Exception {
        System.out.println("Test SetLocation");
        SchoolEvent instance = buildInstance();
        instance.setLocation(location, locationString);
        assertEquals(location, instance.getLocation());
    }

    @Test(expected = NullPointerException.class)
    public void testSetLocationAndProviderasNull () {
        System.out.println("Test setLocation(null, null)");
        SchoolEvent instance = buildInstance();

        instance.setLocation(null, null);
    }

    @Test(expected = NullPointerException.class)
    public void testSetLocationAndProviderAsNullAndEmpty () {
        System.out.println("Test setLocation(null, \"\")");
        SchoolEvent instance = buildInstance();

        instance.setLocation(null, "");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSetLocationandProviderAsLocationAndEmpty () {
        System.out.println("Test setLocation(Location, \"\")");
        SchoolEvent instance = buildInstance();

        instance.setLocation(location, "");
    }

    @Test(expected = NullPointerException.class)
    public void testSetLocationAndProviderAsLocationAndNull () {
        System.out.println("Test setLocation(Location, null)");
        SchoolEvent instance = buildInstance();

        instance.setLocation(location, null);
    }


    @Test(expected = NullPointerException.class)
    public void testSetTimeToNull () {
        System.out.println("Test setTime(null)");
        SchoolEvent instance = buildInstance();

        instance.setTime(null);
    }

    @Test
    public void testSetTime() throws Exception {
        System.out.println("Test setTime()");
        SchoolEvent instance = buildInstance();

        instance.setTime(new TimeSpan(start, end));
        assertEquals(start, instance.getStartTime());
    }


    @Test(expected = NullPointerException.class)
    public void testSetTitleToNull() {
        System.out.println("Test setTitle(null)");
        SchoolEvent instance = buildInstance();

        instance.setTitle(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSetTitleToEmpty() {
        System.out.println("Test setTitle(\"\")");
        SchoolEvent instance = buildInstance();

        instance.setTitle("");
    }

    @Test
    public void testSetTitle() throws Exception {
        System.out.println("Test setTitle()");
        SchoolEvent instance = buildInstance();

        instance.setTitle(title);
        assertEquals(title, instance.getTitle());
    }


    @Test(expected = NullPointerException.class)
    public void testSetDescriptionToNull() {
        System.out.println("Test setDescription(null)");
        SchoolEvent instance = buildInstance();

        instance.setDescription(null);
    }

    @Test
    public void testSetDescription() {
        System.out.printf("Test setDescription()");
        SchoolEvent instance = buildInstance();

        instance.setDescription(description);
        assertEquals(description, instance.getDescription());
    }


    @Test
    public void testGetDurationInMs() throws Exception {
        System.out.println("Test getDurationInMs()");
        SchoolEvent instance = buildInstance();

        long expResult = end.getTime() - start.getTime();

        assertEquals(expResult, instance.getDurationInMs());
    }

    @Test
    public void testGetDurationInMinutes() throws Exception {
        System.out.println("Test getDurationinMinutes()");
        SchoolEvent instance = buildInstance();

        long expResult = (end.getTime() - start.getTime()) / 60000;

        assertEquals(expResult, instance.getDurationInMinutes());
    }

    @Test
    public void isSetToNotify() throws Exception {

    }

    @Test
    public void toggleNotify() throws Exception {

    }

    @Test
    public void equals() throws Exception {

    }

    @Test
    public void describeContents() throws Exception {

    }

    @Test
    public void writeToParcel() throws Exception {

    }

}
package uk.ac.uea.gitguud.activityprogramapp.model;

import android.content.Context;
import android.location.Location;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import uk.ac.uea.gitguud.framework.convenience.DateParser;
import uk.ac.uea.gitguud.framework.convenience.TimeSpan;

import static junit.framework.Assert.*;

/**
 * Created by Pavel Solodilov on 21/11/16.
 */
@RunWith(AndroidJUnit4.class)
public class ICalToSchoolParserTest {
    private static final Context CONTEXT = InstrumentationRegistry.getTargetContext();

    private boolean equalLocations(Location loc1, Location loc2) {
        if (loc1.getAltitude() != loc2.getAltitude()) {
            return false;
        } else if(loc1.getLatitude() != loc2.getLatitude()){
            return false;
        } else if (loc1.getLongitude() != loc2.getLongitude()){
            return false;
        }
        return true;
    }

    @Test
    public void testParse() {
        System.out.println("Test parse()");

        //create expected list of schools
        ArrayList<School> expected = new ArrayList<>();

        //school 1
        School school1 = new School("PPL");
        expected.add(school1);

        //open days and events for school 1
        try {
            //open day 1
            Date date1 = DateParser.toISO8601("20170127T123000Z", Locale.UK);
            SchoolOpenDay openDay1 = new SchoolOpenDay(date1);school1.addOpenDay(openDay1);

            //school events for day 1
            Date start1 = DateParser.toISO8601("20170127T123000Z", Locale.UK);
            Date end1 = DateParser.toISO8601("20170127T130000Z", Locale.UK);
            String title1 = "Language and Communication Studies";
            Location location1 = new Location("UNKNOWN");
            location1.setLatitude(52.621877);
            location1.setLongitude(1.240029);
            String locString1 = "Arts Building 01.02";
            String description1 = "";
            SchoolEvent event1 = new SchoolEvent(title1, location1, locString1, new TimeSpan(start1, end1), description1);

            Date start2 = DateParser.toISO8601("20170127T103000Z", Locale.UK);
            Date end2 = DateParser.toISO8601("20170127T110000Z", Locale.UK);
            String title2 = "Language and Communications Studies Talk";
            Location location2 = new Location("UNKNOWN");
            location2.setLatitude(52.621877);
            location2.setLongitude(1.240029);
            String locString2 = "Arts Building 01.02";
            String description2 = "";
            SchoolEvent event2 = new SchoolEvent(title2, location2, locString2, new TimeSpan(start2, end2), description2);

            Date start3 = DateParser.toISO8601("20170127T100000Z", Locale.UK);
            Date end3 = DateParser.toISO8601("20170127T150000Z", Locale.UK);
            String title3 = "Tours of James Platt Centre for Language Learning";
            Location location3 = new Location("UNKNOWN");
            location3.setLatitude(52.62186);
            location3.setLongitude(1.24027);
            String locString3 = "James Platt Language Centre";
            String description3 = "";
            SchoolEvent event3 = new SchoolEvent(title3, location3, locString3, new TimeSpan(start3, end3), description3);

            Date start4 = DateParser.toISO8601("20170127T100000Z", Locale.UK);
            Date end4 = DateParser.toISO8601("20170127T150000Z", Locale.UK);
            String title4 = "Meet Students";
            Location location4 = new Location("UNKNOWN");
            location4.setLatitude(52.621877);
            location4.setLongitude(1.240029);
            String locString4 = "Arts Building Hub";
            String description4 = "";
            SchoolEvent event4 = new SchoolEvent(title4, location4, locString4, new TimeSpan(start4, end4), description4);

            openDay1.addSchoolEvent(event1);
            openDay1.addSchoolEvent(event2);
            openDay1.addSchoolEvent(event3);
            openDay1.addSchoolEvent(event4);

            //open day 2
            Date date2 = DateParser.toISO8601("20161215T100000Z", Locale.UK);
            SchoolOpenDay openDay2 = new SchoolOpenDay(date2);school1.addOpenDay(openDay2);

            //school events for day 2
            Date start5 = DateParser.toISO8601("20161215T100000Z", Locale.UK);
            Date end5 = DateParser.toISO8601("20161215T150000Z", Locale.UK);
            String title5 = "Meet Students";
            Location location5 = new Location("UNKNOWN");
            location5.setLatitude(52.621877);
            location5.setLongitude(1.240029);
            String locString5 = "Arts Building Hub";
            String description5 = "";
            SchoolEvent event5 = new SchoolEvent(title5, location5, locString5, new TimeSpan(start5, end5), description5);

            Date start6 = DateParser.toISO8601("20161215T100000Z", Locale.UK);
            Date end6 = DateParser.toISO8601("20161215T150000Z", Locale.UK);
            String title6 = "Tours of James Platt Centre for Language Learning";
            Location location6 = new Location("UNKNOWN");
            location6.setLatitude(52.62186);
            location6.setLongitude(1.24027);
            String locString6 = "James Platt Language Centre";
            String description6 = "";
            SchoolEvent event6 = new SchoolEvent(title6, location6, locString6, new TimeSpan(start6, end6), description6);

            Date start7 = DateParser.toISO8601("20161215T123000Z", Locale.UK);
            Date end7 = DateParser.toISO8601("20161215T130000Z", Locale.UK);
            String title7 = "Language and Communication Studies";
            Location location7 = new Location("UNKNOWN");
            String locString7 = "Not specified";
            String description7 = "";
            SchoolEvent event7 = new SchoolEvent(title7, location7, locString7, new TimeSpan(start7, end7), description7);

            Date start8 = DateParser.toISO8601("20161215T103000Z", Locale.UK);
            Date end8 = DateParser.toISO8601("20161215T110000Z", Locale.UK);
            String title8 = "Language and Communications Studies Talk";
            Location location8 = new Location("UNKNOWN");
            String locString8 = "Not specified";
            String description8 = "";
            SchoolEvent event8 = new SchoolEvent(title8, location8, locString8, new TimeSpan(start8, end8), description8);

            openDay2.addSchoolEvent(event5);
            openDay2.addSchoolEvent(event6);
            openDay2.addSchoolEvent(event7);
            openDay2.addSchoolEvent(event8);

        } catch (ParseException e) {
            //throw new MalformedICalException("Wrong date format." + e.getMessage());
        }


        //parse calendar
        ArrayList<School> schools = new IcalToSchoolParser().parse(this.testICal);

        if (schools.isEmpty()) {
            fail("Could not parse school even though the input is in correct format.");
        }

        //check if number of schools is correct
        if (schools.size() != expected.size()) {
            fail("Number of schools must be equal to " + expected.size() +
                    ". Actual: " + schools.size());
        }

        //for each school
        for (int i = 0; i < expected.size(); i++) {

            School actSchool = schools.get(i);
            School expSchool = expected.get(i);

            //check if all school names are correct
            if (!(actSchool.getName().equals(expSchool.getName()))) {
                fail("School name must be equal to " + expSchool.getName() + ". Actual: " + actSchool.getName());
            }

            //check if number of open days for each school is correct
            if (schools.get(i).getOpenDays().length != expected.get(i).getOpenDays().length) {
                fail("Number of open days must be equal to " + expected.get(i).getOpenDays().length +
                        ". Actual: " + schools.get(i).getOpenDays().length);}

            //for each open day
            for (int j = 0; j < expSchool.getOpenDays().length; j++) {

                SchoolOpenDay expOpenD = expSchool.getOpenDays()[j];
                SchoolOpenDay actOpenD = actSchool.getOpenDays()[j];

                //check if dates of open days are correct
                if (!(expOpenD.getDate().equals(actOpenD.getDate()))) {
                    fail("Open day date must be equal to " + expOpenD.getDate() +
                             ". Actual: " + actOpenD.getDate());}

                //for each school event
                for (int k = 0; k < expOpenD.getSchoolEvents().length; k++) {

                    SchoolEvent actEvent = actOpenD.getSchoolEvents()[k];
                    SchoolEvent expEvent = expOpenD.getSchoolEvents()[k];

                    //check if number of events is correct
                    if(expOpenD.getSchoolEvents().length != actOpenD.getSchoolEvents().length){
                        fail("The number of events for this open day must be equal to " + expOpenD.getSchoolEvents().length +
                                ". Actual: " + actOpenD.getSchoolEvents().length);}

                    //check if names of events are correct
                    if(!(expEvent.getTitle().equals(actEvent.getTitle()))){
                        fail("Event name must be equal to " + expEvent.getTitle() +
                        ". Actual: " + actEvent.getTitle());}

                    //check if location strings are correct
                    if(!(expEvent.getLocationString().equals(actEvent.getLocationString()))){
                        fail("Event name must be equal to " + expEvent.getLocationString() +
                                ". Actual: " + actEvent.getLocationString());}

                    //check if locations are correct
                    if(!(equalLocations(expEvent.getLocation(),actEvent.getLocation()))&&
                            expEvent.getLocation().getProvider().equals(actEvent.getLocation().getProvider())){
                        fail("Location must be equal to " + expEvent.getLocation() +
                                ". Actual: " + actEvent.getLocation());}

                    //check if start times are correct
                    if(!(expEvent.getStartTime().equals(actEvent.getStartTime()))){
                        fail("Start date must be equal to " + expEvent.getStartTime() +
                                ". Actual: " + actEvent.getStartTime());}

                    //check if end times are correct
                    if(!(expEvent.getEndTime().equals(actEvent.getEndTime()))){
                        fail("End date must be equal to " + expEvent.getEndTime() +
                                ". Actual: " + actEvent.getEndTime());}

                }//end for school event
            }//end for open days
        }//end for school

    }

    @Test
    public void testMissingCalendarBegin() {
        System.out.println("Test missing BEGIN:VCALENDAR tag");

        ArrayList<School> schools = new IcalToSchoolParser().parse(this.noBeginCal);

         if(schools!=null){
            fail();
        }
    }

    @Test
    public void testMissingCalendarEnd()throws MalformedICalException {
        System.out.println("Test missing END:VCALENDAR tag");

        ArrayList<School> schools = new IcalToSchoolParser().parse(this.noEndCal);
        if(schools!=null){
            fail();
        }
    }

    @Test
    public void testCalName()throws MalformedICalException {
        System.out.println("Test missing X-WR-CALNAME: tag");
        ArrayList<School> schools = new IcalToSchoolParser().parse(this.noCalName);

        if(schools!=null){
            fail();
        }
    }

    @Test
    public void testWrongDataFormat()throws MalformedICalException {
        System.out.println("Test when the data format is wrong");
        ArrayList<School> schools = new IcalToSchoolParser().parse(this.wrongDataFormat);

        if(schools!=null){
            fail();
        }
    }

    @Test
    public void testMissingBeginEvent()throws MalformedICalException {
        System.out.println("Test missing BEGIN:VEVENT tag");
        ArrayList<School> schools = new IcalToSchoolParser().parse(this.noBeginEvent);

        if(schools!=null){
            fail();
        }
    }

    @Test
    public void testMissingEndEvent()throws MalformedICalException {
        System.out.println("Test missing END:VEVENT tag");
        ArrayList<School> schools = new IcalToSchoolParser().parse(this.noEndEvent);

        if(schools!=null){
            fail();
        }
    }

    @Test
    public void testMissingEventStartDate()throws MalformedICalException {
        System.out.println("Test missing DTSTART: tag");
        ArrayList<School> schools = new IcalToSchoolParser().parse(this.noStartDate);

        if(schools!=null){
            fail();
        }
    }

    @Test
    public void testMissingEventEndDate()throws MalformedICalException {
        System.out.println("Test missing DTEND: tag");
        ArrayList<School> schools = new IcalToSchoolParser().parse(this.noEndDate);

        if(schools!=null){
            fail();
        }
    }

    @Test
    public void testMissingEventTitle()throws MalformedICalException {
        System.out.println("Test missing SUMMARY: tag");
        ArrayList<School> schools = new IcalToSchoolParser().parse(this.noTitle);

        if(schools!=null){
            fail();
        }
    }

    @Test
    public void testMissingEventLocation()throws MalformedICalException {
        System.out.println("Test missing LOCATION: tag");
        ArrayList<School> schools = new IcalToSchoolParser().parse(this.noLocation);

        if(schools!=null){
            fail();
        }
    }

    @Test
    public void testMissingEventDescription()throws MalformedICalException {
        System.out.println("Test missing DESCRIPTION: tag");
        ArrayList<School> schools = new IcalToSchoolParser().parse(this.noDescription);

        if(schools!=null){
            fail();
        }
    }


    private String noCalName ="BEGIN:VCALENDAR"+
            //"X-WR-CALNAME:UEA Open Days Test Calendar"+
            "BEGIN:VEVENT"+
            "DTSTART:20161120T143000Z"+
            "DTEND:20161120T153000Z"+
            "DESCRIPTION:Description goes here"+
            "LOCATION:LSB"+
            "SUMMARY:CMP Labs Guide"+
            "END:VEVENT"+
            "END:VCALENDAR";

    private String wrongDataFormat ="BEGIN:VCALENDAR"+
            "X-WR-CALNAME:UEA Open Days Test Calendar"+
            "BEGIN:VEVENT"+
            "DTSTART:20161120T143000"+//missing Z at the end
            "DTEND:20161120T153000Z"+
            "DESCRIPTION:Description goes here"+
            "LOCATION:LSB"+
            "SUMMARY:CMP Labs Guide"+
            "END:VEVENT"+
            "END:VCALENDAR";

    private String noBeginCal =//"BEGIN:VCALENDAR"+
            "X-WR-CALNAME:UEA Open Days Test Calendar"+
            "BEGIN:VEVENT"+
            "DTSTART:20161120T143000Z"+
            "DTEND:20161120T153000Z"+
            "DESCRIPTION:Description goes here"+
            "LOCATION:LSB"+
            "SUMMARY:CMP Labs Guide"+
            "END:VEVENT"+
            "END:VCALENDAR";

    private String noEndCal ="BEGIN:VCALENDAR"+
            "X-WR-CALNAME:UEA Open Days Test Calendar"+
            "BEGIN:VEVENT"+
            "DTSTART:20161120T143000Z"+
            "DTEND:20161120T153000Z"+
            "DESCRIPTION:Description goes here"+
            "LOCATION:LSB"+
            "SUMMARY:CMP Labs Guide"+
            "END:VEVENT";
            //  "END:VCALENDAR";

    private String noBeginEvent ="BEGIN:VCALENDAR"+
            "X-WR-CALNAME:UEA Open Days Test Calendar"+
            //"BEGIN:VEVENT"+
            "DTSTART:20161120T143000Z"+
            "DTEND:20161120T153000Z"+
            "DESCRIPTION:Description goes here"+
            "LOCATION:LSB"+
            "SUMMARY:CMP Labs Guide"+
            "END:VEVENT"+
            "END:VCALENDAR";

    private String noEndEvent ="BEGIN:VCALENDAR"+
            "X-WR-CALNAME:UEA Open Days Test Calendar"+
            "BEGIN:VEVENT"+
            "DTSTART:20161120T143000Z"+
            "DTEND:20161120T153000Z"+
            "DESCRIPTION:Description goes here"+
            "LOCATION:LSB"+
            "SUMMARY:CMP Labs Guide"+
            //"END:VEVENT"+
            "END:VCALENDAR";

    private String noStartDate ="BEGIN:VCALENDAR"+
            "X-WR-CALNAME:UEA Open Days Test Calendar"+
            "BEGIN:VEVENT"+
            //"DTSTART:20161120T143000Z"+
            "DTEND:20161120T153000Z"+
            "DESCRIPTION:Description goes here"+
            "LOCATION:LSB"+
            "SUMMARY:CMP Labs Guide"+
            "END:VEVENT"+
            "END:VCALENDAR";

    private String noEndDate ="BEGIN:VCALENDAR"+
            "X-WR-CALNAME:UEA Open Days Test Calendar"+
            "BEGIN:VEVENT"+
            "DTSTART:20161120T143000Z"+
            //"DTEND:20161120T153000Z"+
            "DESCRIPTION:Description goes here"+
            "LOCATION:LSB"+
            "SUMMARY:CMP Labs Guide"+
            "END:VEVENT"+
            "END:VCALENDAR";

    private String noDescription ="BEGIN:VCALENDAR"+
            "X-WR-CALNAME:UEA Open Days Test Calendar"+
            "BEGIN:VEVENT"+
            "DTSTART:20161120T143000Z"+
            "DTEND:20161120T153000Z"+
            //"DESCRIPTION:Description goes here"+
            "LOCATION:LSB"+
            "SUMMARY:CMP Labs Guide"+
            "END:VEVENT"+
            "END:VCALENDAR";

    private String noLocation ="BEGIN:VCALENDAR"+
            "X-WR-CALNAME:UEA Open Days Test Calendar"+
            "BEGIN:VEVENT"+
            "DTSTART:20161120T143000Z"+
            "DTEND:20161120T153000Z"+
            "DESCRIPTION:Description goes here"+
           // "LOCATION:Lawrence Stenhouse Building, Norwich NR4, UK"+
            "SUMMARY:CMP Labs Guide"+
            "END:VEVENT"+
            "END:VCALENDAR";

    private String noTitle ="BEGIN:VCALENDAR"+
            "X-WR-CALNAME:UEA Open Days Test Calendar"+
            "BEGIN:VEVENT"+
            "DTSTART:20161120T143000Z"+
            "DTEND:20161120T153000Z"+
            "DESCRIPTION:Description goes here"+
            "LOCATION:Lawrence Stenhouse Building, Norwich NR4, UK"+
            //"SUMMARY:CMP Labs Guide"+
            "END:VEVENT"+
            "END:VCALENDAR";

    private String testICal = "BEGIN:VCALENDAR\n" +
            "PRODID:-//Google Inc//Google Calendar 70.9054//EN\n" +
            "VERSION:2.0\n" +
            "CALSCALE:GREGORIAN\n" +
            "METHOD:PUBLISH\n" +
            "X-WR-CALNAME:PPL\n"+
            "X-WR-TIMEZONE:Europe/London\n" +
            "X-WR-CALDESC:\n" +
            "BEGIN:VEVENT\n" +
            "DTSTART:20170127T123000Z\n" +
            "DTEND:20170127T130000Z\n" +
            "DTSTAMP:20161121T193532Z\n" +
            "UID:tlrlarl64g9cbuagovgtb8r39o@google.com\n" +
            "CREATED:20161121T122322Z\n" +
            "DESCRIPTION:\n" +
            "LAST-MODIFIED:20161121T122322Z\n" +
            "LOCATION:ARTS-01.02\n" +
            "SEQUENCE:0\n" +
            "STATUS:CONFIRMED\n" +
            "SUMMARY:Language and Communication Studies\n" +
            "TRANSP:OPAQUE\n" +
            "END:VEVENT\n" +
            "BEGIN:VEVENT\n" +
            "DTSTART:20170127T103000Z\n" +
            "DTEND:20170127T110000Z\n" +
            "DTSTAMP:20161121T193532Z\n" +
            "UID:9fis1dctnmjlabh9o48rdnlakk@google.com\n" +
            "CREATED:20161121T122258Z\n" +
            "DESCRIPTION:\n" +
            "LAST-MODIFIED:20161121T122258Z\n" +
            "LOCATION:ARTS-01.02\n" +
            "SEQUENCE:0\n" +
            "STATUS:CONFIRMED\n" +
            "SUMMARY:Language and Communications Studies Talk\n" +
            "TRANSP:OPAQUE\n" +
            "END:VEVENT\n" +
            "BEGIN:VEVENT\n" +
            "DTSTART:20170127T100000Z\n" +
            "DTEND:20170127T150000Z\n" +
            "DTSTAMP:20161121T193532Z\n" +
            "UID:hq4ss9sdt6o5g0a9tgu7pevm8o@google.com\n" +
            "CREATED:20161121T122048Z\n" +
            "DESCRIPTION:\n" +
            "LAST-MODIFIED:20161121T122048Z\n" +
            "LOCATION:JPLC\n" +
            "SEQUENCE:0\n" +
            "STATUS:CONFIRMED\n" +
            "SUMMARY:Tours of James Platt Centre for Language Learning\n" +
            "TRANSP:OPAQUE\n" +
            "END:VEVENT\n" +
            "BEGIN:VEVENT\n" +
            "DTSTART:20170127T100000Z\n" +
            "DTEND:20170127T150000Z\n" +
            "DTSTAMP:20161121T193532Z\n" +
            "UID:hvasatuvp1i5pticrvqp0u67bc@google.com\n" +
            "CREATED:20161121T122036Z\n" +
            "DESCRIPTION:\n" +
            "LAST-MODIFIED:20161121T122036Z\n" +
            "LOCATION:ARTS-Hub\n" +
            "SEQUENCE:0\n" +
            "STATUS:CONFIRMED\n" +
            "SUMMARY:Meet Students\n" +
            "TRANSP:OPAQUE\n" +
            "END:VEVENT\n" +
            "BEGIN:VEVENT\n" +
            "DTSTART:20161215T100000Z\n" +
            "DTEND:20161215T150000Z\n" +
            "DTSTAMP:20161121T193532Z\n" +
            "UID:5ccbink38e3g8b7k1fps0hcvmk@google.com\n" +
            "CREATED:20161121T115144Z\n" +
            "DESCRIPTION:\n" +
            "LAST-MODIFIED:20161121T115144Z\n" +
            "LOCATION:ARTS-Hub\n" +
            "SEQUENCE:0\n" +
            "STATUS:CONFIRMED\n" +
            "SUMMARY:Meet Students\n" +
            "TRANSP:OPAQUE\n" +
            "END:VEVENT\n" +
            "BEGIN:VEVENT\n" +
            "DTSTART:20161215T100000Z\n" +
            "DTEND:20161215T150000Z\n" +
            "DTSTAMP:20161121T193532Z\n" +
            "UID:mjvufnat6hpv0csco7g5975cvc@google.com\n" +
            "CREATED:20161121T115106Z\n" +
            "DESCRIPTION:\n" +
            "LAST-MODIFIED:20161121T115106Z\n" +
            "LOCATION:JPLC\n" +
            "SEQUENCE:0\n" +
            "STATUS:CONFIRMED\n" +
            "SUMMARY:Tours of James Platt Centre for Language Learning\n" +
            "TRANSP:OPAQUE\n" +
            "END:VEVENT\n" +
            "BEGIN:VEVENT\n" +
            "DTSTART:20161215T123000Z\n" +
            "DTEND:20161215T130000Z\n" +
            "DTSTAMP:20161121T193532Z\n" +
            "UID:bilsgn0n9bc0gs20rohf0or7o0@google.com\n" +
            "CREATED:20161121T114956Z\n" +
            "DESCRIPTION:\n" +
            "LAST-MODIFIED:20161121T114956Z\n" +
            "LOCATION:Blahblah\n" +
            "SEQUENCE:0\n" +
            "STATUS:CONFIRMED\n" +
            "SUMMARY:Language and Communication Studies\n" +
            "TRANSP:OPAQUE\n" +
            "END:VEVENT\n" +
            "BEGIN:VEVENT\n" +
            "DTSTART:20161215T103000Z\n" +
            "DTEND:20161215T110000Z\n" +
            "DTSTAMP:20161121T193532Z\n" +
            "UID:6micnd7q48vokr2jgbme10m4q8@google.com\n" +
            "CREATED:20161121T114905Z\n" +
            "DESCRIPTION:\n" +
            "LAST-MODIFIED:20161121T114905Z\n" +
            "LOCATION: \n" +
            "SEQUENCE:0\n" +
            "STATUS:CONFIRMED\n" +
            "SUMMARY:Language and Communications Studies Talk\n" +
            "TRANSP:OPAQUE\n" +
            "END:VEVENT\n" +
            "END:VCALENDAR";
}


package uk.ac.uea.gitguud.activityprogramapp.model;

import android.location.Location;
import android.support.v4.util.Pair;
import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Scanner;

import uk.ac.uea.gitguud.activityprogramapp.controller.NotificationSpawner;
import uk.ac.uea.gitguud.framework.convenience.AndroidSpecific;
import uk.ac.uea.gitguud.framework.convenience.DateParser;
import uk.ac.uea.gitguud.framework.convenience.TimeSpan;
import uk.ac.uea.gitguud.framework.interfaces.ParseStrategy;

/**
 * IcalToSchoolParser.java
 *
 * Provides a facility to parse an iCal string (Google Calendar) into a list of schools.
 * Each calendar is assumed to be a different school.
 *
 * A typical schema looks like this:
 * BEGIN:VCALENDAR
 * PRODID:-//Google Inc//Google Calendar 70.9054//EN   # IGNORED
 * VERSION:2.0                                         # IGNORED
 * CALSCALE:GREGORIAN                                  # IGNORED
 * METHOD:PUBLISH                                      # IGNORED
 * X-WR-CALNAME:UEA Open Days Test Calendar
 * X-WR-TIMEZONE:Europe/London                         # IGNORED
 * X-WR-CALDESC:                                       # IGNORED
 * BEGIN:VEVENT
 * DTSTART:20161120T143000Z
 * DTEND:20161120T153000Z
 * DTSTAMP:20161119T144857Z                            # IGNORED
 * UID:8uucqqolupm7mj990nheb78jgc@google.com
 * CREATED:20161118T143008Z                            # IGNORED
 * DESCRIPTION:Description goes here
 * LAST-MODIFIED:20161118T143008Z                      # IGNORED TODO check if could be used
 * LOCATION:Lawrence Stenhouse Building\, Norwich NR4\, UK
 * SEQUENCE:0                                          # IGNORED
 * STATUS:CONFIRMED                                    # IGNORED
 * SUMMARY:CMP Labs Guide
 * TRANSP:OPAQUE                                       # IGNORED
 * END:VEVENT
 * END:VCALENDAR
 * BEGIN:VCALENDAR
 * ...
 * END:VCALENDAR
 *
 *
 * Created by mircea on 07/11/16.
 */
public class IcalToSchoolParser implements ParseStrategy<School> {

    // Calendar container.
    private static final String PREFIX_CAL_BEGIN = "BEGIN:VCALENDAR";
    private static final String PREFIX_CAL_END   = "END:VCALENDAR";

    // Calendar contents.
    private static final String PREFIX_CALNAME = "X-WR-CALNAME:";

    // Event container.
    private static final String PREFIX_EVENT_BEGIN = "BEGIN:VEVENT";
    private static final String PREFIX_EVENT_END   = "END:VEVENT";

    // Event contents.
    private static final String PREFIX_EVENT_DATE_START  = "DTSTART:";
    private static final String PREFIX_EVENT_DATE_END    = "DTEND:";
    private static final String PREFIX_EVENT_DESCRIPTION = "DESCRIPTION:";
    private static final String PREFIX_EVENT_LOCATION    = "LOCATION:";
    private static final String PREFIX_EVENT_TITLE       = "SUMMARY:";
    private static final String PREFIX_UID               = "UID:";

    // TODO make the locale configurable.
    private Locale locale = Locale.UK;

    /**
     * Constructs an ArrayList of School objects out of an iCalendar (.ics) formatted data.
     * Note that names of schools will be set to names of calendars.
     *
     * @param iCalString iCal string to parse.
     * @return An empty ArrayList if the given string was empty, an array list with school entries,
     *         on successful parse, or null if the provided data was malformed.
     */
    @Override
    public ArrayList<School> parse(String iCalString) {

        // Go through each line with a scanner.
        Scanner scanner = new Scanner(iCalString);
        School school = null;

        try {
            String line;

            // Run till the end of the scanner.
            while (scanner.hasNextLine()) {
                // Find the beginning line of a calendar.
                line = scanner.nextLine();

                if (line.startsWith(PREFIX_CAL_BEGIN)) {
                    // Save the school that was found using the beginning line.
                    // Only one school per calendar.
                    school = (scanCalendar(scanner));
                }
            } // end while
        } catch (MalformedICalException e) {
            Log.e(AndroidSpecific.getLogTag(this), "Failed to parse iCal: " + e.getMessage());
            return null;
        } finally {
            scanner.close();
        }

        if (school == null) {
            // If the school was not found, it means that the calendar begin prefix was not
            // encountered.
            return null;
        } else {
            // Otherwise create and return an array list of a single school.
            ArrayList<School> schools = new ArrayList<>();
            schools.add(school);

            return schools;
        }
    }

    /**
     * Scans a single calendar string into a single school.
     * The name of the school will be set to the name of the calendar.
     *
     * @param scanner Scanner to get the calendar data from.
     * @return School object constructed from calendar data.
     * @throws MalformedICalException if the iCal data is malformed.
     */
    private School scanCalendar(Scanner scanner) throws MalformedICalException {
        String schoolName = scanCalendarName(scanner);
        Iterable<SchoolOpenDay> schoolOpenDays = scanOpenDays(scanner);

        return new School(schoolName).addOpenDays(schoolOpenDays);
    }


    /**
     * Scans for the first found entry of a calendar name.
     *
     * @param scanner Scanner to use to get the data stream from.
     * @return Name of the calendar.
     * @throws MalformedICalException if the iCal data is malformed.
     */
    private String scanCalendarName(Scanner scanner) throws MalformedICalException {
        String line;
        while (scanner.hasNextLine()) {
            line = scanner.nextLine();
            if (line.startsWith(PREFIX_CALNAME)) {
                // Return only the calendar name, removing the prefix before it.
                return line.substring(PREFIX_CALNAME.length());
            }
        }

        throw new MalformedICalException("Failed to find calendar name.");
    }


    /**
     * Scans open days into a school. Note that an open day is a set of school events that happen on
     * the same day.
     * Note that each event will be assigned to an open day by its date, and thus events running
     * over midnight might be assigned to the next open day. Start date of the event is used for
     * assignment.
     *
     * @param scanner Scanner to use to get the data stream from.
     * @return List of school open days. Will be empty if none are there.
     * @throws MalformedICalException if the iCal data is malformed.
     */
    private ArrayList<SchoolOpenDay> scanOpenDays(Scanner scanner) throws MalformedICalException {

        // Holds open day values.
        HashMap<String, SchoolOpenDay> openDayHashMap = new HashMap<>();

        // Temporary object holder variables.
        String openDayDate;
        SchoolOpenDay openDay;
        SchoolEvent event;

        String line;
        while (scanner.hasNextLine()) {
            line = scanner.nextLine();

            if (line.startsWith(PREFIX_CAL_END)) {
                // If reached the end of calendar, we are done:
                // construct an return an array list from the hash map values.
                return new ArrayList<>(openDayHashMap.values());

            } else if (line.startsWith(PREFIX_EVENT_BEGIN)) {
                // If found the start of event, scan it.
                event = scanEvent(scanner);

                // Use the days in day-month-year format in order to keep track of open days.
                openDayDate = new SimpleDateFormat("dd-MM-yyyy").format(event.getStartTime());
                openDay = openDayHashMap.get(openDayDate);

                // If the open day object does not exist yet,
                // create it and insert it into the hash map.
                if (openDay == null) {
                    openDay = new SchoolOpenDay(event.getStartTime());
                    openDayHashMap.put(openDayDate, openDay);
                }

                // Add the event to the corresponding open day.
                openDay.addSchoolEvent(event);
            }
        } // end while

        throw new MalformedICalException("No calendar closing tag found.");
    }

    /**
     * Scans a single event into a school event.
     * Note that dates are supposed to be in the most minimal ISO 8601 without colons,
     * e.g. 20161120T143000Z is 20th November 2016, 14:30:00 UTC
     *
     * @param scanner Scanner to get event data from.
     * @return a single school event.
     * @throws MalformedICalException if the iCal data is malformed.
     */
    private SchoolEvent scanEvent(Scanner scanner) throws MalformedICalException {
        CampusLocations locationsData = CampusLocations.getInstance();

        Date   startTime      = null;
        Date   endTime        = null;
        String description    = null;
        String locationString = null;
        String title          = null;
        Location location     = new Location("UNKNOWN"); // TODO use the actual building's location
        String uid            = null;

        String line;
        while(scanner.hasNextLine()) {
            line = scanner.nextLine();

            // The line.substring(PREFIX.length()) bits
            // remove the prefixes to acquire the actual data.

            // Put values into the variables.
            if (line.startsWith(PREFIX_EVENT_DATE_START)) {
                startTime = this.parseEventDate(
                        line.substring(PREFIX_EVENT_DATE_START.length()), locale);

            } else if (line.startsWith(PREFIX_EVENT_DATE_END)) {
                endTime = this.parseEventDate(
                        line.substring(PREFIX_EVENT_DATE_END.length()), locale);

            } else if (line.startsWith(PREFIX_EVENT_DESCRIPTION)) {
                description = line.substring(PREFIX_EVENT_DESCRIPTION.length());

            } else if (line.startsWith(PREFIX_EVENT_LOCATION)) {
                String locationDetails = line.substring(PREFIX_EVENT_LOCATION.length());
                //location is represented by abbreviation-room details, where "-" is delimiter
                //for example: ARTS-1 01.02 or CMP-3.02 or ARTS-1 Hub
                String[] loc = locationDetails.split("-");
                //location was not specified or abbreviation does not match any locations
                if(loc[0] == null || locationsData.getLocationByKey(loc[0]) == null) {
                    locationString = "Not specified";
                    //default coordinates if location was not found (The Square)
                    //otherwise shows just a blue screen
                    location.setLatitude(52.621742);
                    location.setLongitude(1.240992);
                } else {
                    Pair<String, Location> locationPair = locationsData.getLocationByKey(loc[0]);
                    if (locationPair != null) {
                        locationString = locationPair.first;
                        //if location string contained any information after "-" delimiter
                        if (loc.length > 1)
                            locationString += " " + loc[1];

                        //assign coordinates for this location
                        Location coords = locationsData.getLocationByKey(loc[0]).second;
                        location.setLatitude(coords.getLatitude());
                        location.setLongitude(coords.getLongitude());
                    }
                }

            } else if (line.startsWith(PREFIX_EVENT_TITLE)) {
                title = line.substring(PREFIX_EVENT_TITLE.length());
            } else if (line.startsWith(PREFIX_UID)) {
                uid = line.substring(PREFIX_UID.length());

            } else if (line.startsWith(PREFIX_EVENT_END)) {
                try {
                    SchoolEvent event =
                            new SchoolEvent(title, location, locationString,
                                    new TimeSpan(startTime, endTime), description);

                    if (NotificationSpawner.instance != null) {
                        if (NotificationSpawner.instance.isSetNotification(event.getUid())) {
                            event.toggleNotify();
                        }
                    }

                    return event.setUid(uid);
                } catch (Exception e) {
                    throw new MalformedICalException("Failed to parse a school event: "
                            + e.getMessage());
                }
            } // end if
        } // end while

        throw new MalformedICalException("No event closing tag found.");
    }

    /**
     * Parses an event date from ISO 8601 date string from iCal to a date.
     *
     * @param dateString Date string to parse.
     * @param locale Locale to use for the date.
     * @return Converted date.
     * @throws MalformedICalException If the date was in a wrong format.
     */
    private Date parseEventDate(String dateString, Locale locale) throws MalformedICalException {
        try {
            return DateParser.toISO8601(dateString, locale);
        } catch (ParseException e) {
            throw new MalformedICalException("Wrong date format." + e.getMessage());
        }
    }

}

package uk.ac.uea.gitguud.activityprogramapp.view;

import android.view.View;

/**
 * ContainerOnClickListener.java
 *
 * An on click listener that can contain model objects.
 * Created by Pavel Solodilov on 26/11/16.
 */
public interface ContainerOnClickListener<T> extends View.OnClickListener {

    /**
     * This method binds/adds a model class to the listener in order for it to perform operations
     * on it.
     *
     * @param object Model object to bind this listener to.
     * @return This listener object for chaining.
     */
    ContainerOnClickListener<T> bindModel(T object);
}

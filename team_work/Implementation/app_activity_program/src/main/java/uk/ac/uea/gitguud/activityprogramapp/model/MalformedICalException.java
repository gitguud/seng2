package uk.ac.uea.gitguud.activityprogramapp.model;

/**
 * MalformedICalException.java
 *
 * Thrown whenever an iCal parser encounters malformed data.
 * Created by Pavel Solodilov on 20/11/16.
 */
public class MalformedICalException extends Exception {
    public MalformedICalException(String message){
        super(message);
    }
}

package uk.ac.uea.gitguud.activityprogramapp.controller;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import uk.ac.uea.gitguud.activityprogramapp.R;
import uk.ac.uea.gitguud.activityprogramapp.model.SchoolList;
import uk.ac.uea.gitguud.framework.interfaces.SyncCallback;

/**
 * SupportController.java
 *
 * Created by Pavel Solodilov on 12/12/16.
 */
public class SupportController {

    /**
     * Creates the action bar.
     *
     * @param activity Activity to attach the action bar to.
     * @param titleColour Give -0.5 if you do not want to set it here.
     * @param toolbarText Give null if you do not want to set it here.
     */
    public static void createToolbar(AppCompatActivity activity, double titleColour,
                                                  String toolbarText) {
        Toolbar toolbar = (Toolbar) activity.findViewById(R.id.toolbar);

        if (titleColour != -0.5) {
            toolbar.setTitleTextColor((int)titleColour);
        }

        if (toolbarText != null) {
            toolbar.setTitle(toolbarText);
        }

        toolbar.showOverflowMenu();
        activity.setSupportActionBar(toolbar);
    }


    public static boolean onOptionsItemSelected(MenuItem item, Context context,
                                                   SyncCallback callback) {
        switch (item.getItemId()) {
            case R.id.action_sync:
                SchoolList.getInstance().synchroniseSchools(context, callback, true);
                return true;
            case R.id.action_settings:
                context.startActivity(new Intent(context, SettingsActivity.class));
                return true;
            default:
                return false;
        }
    }

    /**
     * Helper method.
     * Returns an appropriate coloured notification (bell) image and text (set/unset).
     * The image will be coloured into the primary colour (R.color.colorPrimary) of the context.
     *
     * @param context Context of the application. Note that the resource IDs are hard-coded; default
     *                string IDs are that for the toast notifications.
     * @param isSet True if set to notify (e.g. filled bell), and false otherwise.
     * @return The correct image of a notification's state as item 1, and a corresponding text
     *         string as item 2.
     */
    public static Pair<Drawable, String> getNotificationImageAndText(Context context,
                                                                     boolean isSet) {
        return getNotificationImageAndText(context, isSet, R.string.notification_set,
                R.string.notification_unset);
    }

    /**
     * Helper method.
     * Returns an appropriate coloured notification (bell) image and text (set/unset).
     * The image will be coloured into the primary colour (R.color.colorPrimary) of the context.
     *
     * @param context Context of the application. Note that the image resource IDs are hard-coded.
     * @param isSet True if set to notify (e.g. filled bell), and false otherwise.
     * @param setTextId ID of the text to return when the reminder has been set.
     * @param unsetTextId ID of the text to return when the reminder has been unset.
     * @return The correct image of a notification's state as item 1, and a corresponding text
     *         string as item 2.
     */
    public static Pair<Drawable, String> getNotificationImageAndText(Context context, boolean isSet,
                                                                     int setTextId,
                                                                     int unsetTextId) {
        // Short-hand notation for resources.
        Resources res = context.getResources();

        // Resulting values.
        String text;
        Drawable image;

        // Get appropriate text and image.
        if (isSet) {
            text = res.getString(setTextId);
            image = res.getDrawable(R.drawable.ic_notifications_active_black_24dp);
        } else {
            text = res.getString(unsetTextId);
            image = res.getDrawable(R.drawable.ic_notifications_none_black_24dp);
        }

        // Colour the image into the primary colour.
        image.setColorFilter(res.getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_ATOP);

        return new Pair<>(image, text);
    }

}

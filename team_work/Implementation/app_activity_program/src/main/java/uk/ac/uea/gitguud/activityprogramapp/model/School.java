package uk.ac.uea.gitguud.activityprogramapp.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

import uk.ac.uea.gitguud.framework.convenience.ErrorChecking;
import uk.ac.uea.gitguud.framework.convenience.SupportFuncs;
import uk.ac.uea.gitguud.framework.interfaces.ViewTextProvider;

/**
 * School.java
 *
 * Models a school with open days.
 * Created by Pavel Solodilov on 04/11/16.
 */

public class School implements ViewTextProvider {
    private String name;

    private final ArrayList<SchoolOpenDay> OPEN_DAYS;

    /**
     * Constructor for a school object.
     *
     * @param name Name of the school.
     * @throws Exception if any of the provided values do not conform with the requirements
     *                   in appropriate setters.
     */
    public School(String name) {
        this.setName(name);
        this.OPEN_DAYS = new ArrayList<>();
    }


    /**
     * Accessor for the name of the school.
     *
     * @return Name of the school.
     */
    public String getName() {
        return this.name;
    }

    /**
     * Accessor for all of the open days of the school.
     *
     * @return Array of all open days of the school.
     */
    public SchoolOpenDay[] getOpenDays() {
        SchoolOpenDay[] opendaysArr = new SchoolOpenDay[this.OPEN_DAYS.size()];
        opendaysArr = this.OPEN_DAYS.toArray(opendaysArr);
        return opendaysArr;
    }

    /**
     * Accessor for all of the open days of the school. No changes to the returned array list will
     * affect the the open day's inner ordering.
     *
     * @return ArrayList of all open days of the school.
     */
    public ArrayList<SchoolOpenDay> getOpenDaysAsArrayList(){
        return new ArrayList<>(this.OPEN_DAYS);
    }


    /**
     * Simply returns capitalised name of the school.
     *
     * @return String representing the school.
     */
    public String getText() {
        return this.getName().toUpperCase();
    }

    /**
     * Returns a school event with matching UID.
     *
     * @param uid UID to search for.
     * @return School event with the provided UID, or null if not found.
     */
    public SchoolEvent findEventByUid(String uid) {
        SchoolEvent event = null;

        for (SchoolOpenDay openDay : this.OPEN_DAYS) {
            event = openDay.findEventByUid(uid);

            // If found, return the event.
            if (event != null) return event;
        }

        return event;
    }

    /**
     * Returns an open day which happens at the same day as given date.
     *
     * @param date Date of the open day.
     * @return Matching open day or null if not found.
     */
    public SchoolOpenDay findOpenDayByDate(Date date) {

        for (SchoolOpenDay openDay : this.OPEN_DAYS) {
            if (SupportFuncs.sameDay(date, openDay.getDate())) {
                // Found if the days match.
                return openDay;
            }
        }

        return null;
    }


    /**
     * Sets the name of the school.
     *
     * @param name New name of the school.
     * @return This school object for chaining.
     * @throws NullPointerException if the provided name is null.
     * @throws IllegalArgumentException if the provided name is an empty string.
     */
    public School setName(String name) throws  NullPointerException, IllegalArgumentException {
        ErrorChecking.getInstance()
                .throwOnNull(name, "School's name cannot be null.")
                .throwOnEmptyString(name, "School's name cannot be an empty string.");

        this.name = name;
        return this;
    }

    /**
     * Adds an open day to the school.
     *
     * @param openDay Open day to add. It will not be added if it is null.
     * @return True if the open day has been added, and false otherwise.
     */
    public boolean addOpenDay(SchoolOpenDay openDay) {

        if (openDay != null && this.OPEN_DAYS.add(openDay)) {
            // If the open day is not null, and could successfully be added, sort the school open
            // days by date and return true.
            Collections.sort(this.OPEN_DAYS, new SchoolOpenDay.CompareByDate());
            return true;
        }

        return false;
    }

    /**
     * Adds all of the open days into school.
     *
     * @param openDays Open days to add. Assumed not to be null.
     * @return This object for chaining.
     */
    public School addOpenDays(Iterable<SchoolOpenDay> openDays) {
        for (SchoolOpenDay openDay : openDays) {
            this.OPEN_DAYS.add(openDay);
        }

        // Compare the open days by date, but do it only once by adding open days directly into the
        // open days list instead of using the other setter.
        Collections.sort(this.OPEN_DAYS, new SchoolOpenDay.CompareByDate());
        return this;
    }

    /**
     * Removes all of the open days from the school.
     *
     * @return This object for chaining.
     */
    public School clearOpenDays() {
        this.OPEN_DAYS.clear();
        return this;
    }

    /**
     * Updates the contents (open days) of this school with another school's ones.
     * If this school has an open day that the other one does not, it gets removed from this school.
     * If the other school has an open day that this one does not, it gets added to this school.
     * If a matching open day exists in both schools, the open days gets merged in the same way as
     * this school with another one.
     *
     * @param school School to merge this one with.
     * @return This object for chaining.
     */
    public School mergeSchools(School school) {

        int index;
        for (SchoolOpenDay newOpenDay : school.OPEN_DAYS) {

            // The index will be -1 if not found.
            index = this.OPEN_DAYS.indexOf(newOpenDay);

            if (index >= 0) {
                // If match was found, merge the open day data.
                this.OPEN_DAYS.get(index).mergeOpenDays(newOpenDay);
            } else {
                // Otherwise simply add it to the school.
                this.OPEN_DAYS.add(newOpenDay);
            }

        } // end for each


        // Remove all of the open days that are not in the new school.
        this.OPEN_DAYS.retainAll(school.OPEN_DAYS);

        return this;
    }


    /**
     * Compares two schools by their names. Note that due to no real type checking, make sure that
     * the object you pass to this method is another school. You will get an exception otherwise.
     *
     * @param other Other school to compare to.
     * @return True if names of schools are the same.
     */
    @Override
    public boolean equals(Object other) {
        // Cast to school. Throws exception on failure.
        School otherSchool = (School) other;

        return otherSchool.getName().equals(this.getName());
    }


    /**
     * Inner static comparator class. Used to compare two schools by name.
     */
    public static class CompareByName implements Comparator<School> {
        public int compare(School s1, School s2) {
            return s1.getName().compareTo(s2.getName());
        }
    }
}

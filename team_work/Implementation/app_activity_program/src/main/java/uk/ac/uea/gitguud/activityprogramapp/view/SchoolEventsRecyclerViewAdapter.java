package uk.ac.uea.gitguud.activityprogramapp.view;

import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v4.util.Pair;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.koushikdutta.async.future.FutureCallback;

import java.util.ArrayList;
import java.util.List;

import uk.ac.uea.gitguud.activityprogramapp.R;
import uk.ac.uea.gitguud.activityprogramapp.controller.EventDetailsActivity;
import uk.ac.uea.gitguud.activityprogramapp.controller.NotificationSpawner;
import uk.ac.uea.gitguud.activityprogramapp.controller.SupportController;
import uk.ac.uea.gitguud.activityprogramapp.model.SchoolEvent;
import uk.ac.uea.gitguud.activityprogramapp.model.SchoolList;
import uk.ac.uea.gitguud.framework.convenience.SupportFuncs;

import static uk.ac.uea.gitguud.framework.convenience.AndroidSpecific.shortToast;

/**
 * SchoolEventsRecyclerViewAdapter.java
 *
 * Adapter for a recycler view containing the open day event list.
 * Created by Pavel Solodilov on 26/11/16.
 */
public class SchoolEventsRecyclerViewAdapter
        extends RecyclerView.Adapter<SchoolEventsRecyclerViewAdapter.RowViewHolder> {
    private List<SchoolEvent> events;
    private int viewHolderLayout;

    /**
     * Constructor taking the events to be displayed.
     *
     * @param events List of events.
     * @param viewHolderLayout ID of the view holder layout to inflate a single row of the recycler
     *                         view.
     */
    public SchoolEventsRecyclerViewAdapter(List<SchoolEvent> events, int viewHolderLayout) {
        this.setEvents(events).viewHolderLayout = viewHolderLayout;
    }


    /**
     * Sets the events for this adapter to hold.
     *
     * @param events List of events. Will be set to an empty list if null.
     * @return This adapter for chaining.
     */
    public SchoolEventsRecyclerViewAdapter setEvents(List<SchoolEvent> events) {

        // Initialise events to an empty array list if null.
        if (events == null) {
            events = new ArrayList<>();
        }

        this.events = events;
        return this;
    }


    /**
     * Creates a single recycler view row with event data.
     *
     * @param parent The parent view that will hold the row.
     * @param viewType The view type of the new View; ignored in this case.
     * @return A single row (view) of event data.
     */
    @Override
    public RowViewHolder onCreateViewHolder(ViewGroup parent,
                                            int viewType) {
        // Inflate a single view.
        View v = LayoutInflater.from(parent.getContext())
                .inflate(this.viewHolderLayout, parent, false);

        // Wrap the view into the view holder and return it.
        return new RowViewHolder(v);
    }


    /**
     * Connects the event (model) data to the required view row.
     *
     * @param holder Row of the recycler view that needs the event data.
     * @param position Position of the row needing the event data.
     */
    @Override
    public void onBindViewHolder(RowViewHolder holder, int position) {
        holder.bindModel(this.events.get(position), position);
    }


    /**
     * Returns the total amount of events in the adapter.
     *
     * @return Amount of events in the adapter.
     */
    @Override
    public int getItemCount() {
        return this.events.size();
    }


    // ----------------------------------- INNER CLASS ------------------------------------------ //

    static class RowViewHolder extends RecyclerView.ViewHolder {
        private Context context;
        private View rowView;
        private TextView timeColumn;
        private TextView titleColumn;

        /**
         * The image column contains the notification icon.
         */
        private ImageView imageColumn;

        private ContainerOnClickListener<SchoolEvent> onTextClickListener;
        private ContainerOnClickListener<SchoolEvent> onImageClickListener;


        /**
         * Constructor.
         *
         * @param v Single row containing three columns: two text views and an image view.
         */
        public RowViewHolder(View v) {
            super(v);
            this.context = v.getContext().getApplicationContext();
            this.rowView = v;

            // Find the columns and then cast them to appropriate views.
            this.timeColumn = (TextView)  (v.findViewById(R.id.list_row_text_col_1));
            this.titleColumn = (TextView)  (v.findViewById(R.id.list_row_text_col_2));
            this.imageColumn = (ImageView) (v.findViewById(R.id.list_row_img_col_3));

            // Create the listeners.
            this.onTextClickListener = new OnTextClickListener();
            this.onImageClickListener = new OnImageClickListener();

            // Set the listeners: the text click listener applies to both of the text columns.
            this.timeColumn.setOnClickListener(this.onTextClickListener);
            this.titleColumn.setOnClickListener(this.onTextClickListener);
            this.imageColumn.setOnClickListener(this.onImageClickListener);
        }

        /**
         * Binds a school event data to the view holder.
         *
         * @param event Event to bind.
         * @param position Position of the view holder in the recycler view list.
         * @return This row view holder for chaining.
         */
        public RowViewHolder bindModel(SchoolEvent event, int position) {

            // Calls methods on self and returns self.
            this.setTimeColumn(SupportFuncs.timespanToTime(event.getTimeSpan()))
                    .setTitleColumn(event.getTitle())
                    .setImageColumn(event.isSetToNotify())
                    .altColour(position)
                    .bindListenerEvent(event);
            return this;
        }

        /**
         * Sets the text in the first (time) column.
         *
         * @param text Text to set.
         * @return This row view holder for chaining.
         */
        private RowViewHolder setTimeColumn(String text) {
            this.timeColumn.setText(text);
            return this;
        }

        /**
         * Sets the text in the second (title) column.
         *
         * @param text Text to set.
         * @return This row view holder for chaining.
         */
        private RowViewHolder setTitleColumn(String text) {
            this.titleColumn.setText(text);
            return this;
        }

        /**
         * Sets the image in the third (notification) column.
         *
         * @param active True if notification has been set, and false if not.
         * @return This row view holder for chaining.
         */
        private RowViewHolder setImageColumn(boolean active) {

            // Get the image.
            Drawable img =
                    SupportController.getNotificationImageAndText(this.context, active).first;

            // Set the image.
            this.imageColumn.setImageDrawable(img);
            return this;
        }


        /**
         * Sets the alternating colour to the row depending on its position.
         *
         * @param pos Position of the row.
         * @return This row view holder for chaining.
         */
        private RowViewHolder altColour(int pos) {

            // Odd rows have to have the main list colour, and event ones the alternative one.
            // This gets the ID of the colour to get from resources.
            int colourId = (pos % 2 != 0) ? R.color.transparent : R.color.colorAltList;

            // Set the background colour by retrieving it from the resources.
            this.rowView.setBackgroundColor(this.context.getResources().getColor(colourId));

            return this;
        }


        /**
         * Binds an event object to all of the listeners.
         *
         * @param evt Event to bind.
         * @return This row view holder for chaining.
         */
        private RowViewHolder bindListenerEvent(SchoolEvent evt) {
            this.onTextClickListener.bindModel(evt);
            this.onImageClickListener.bindModel(evt);
            return this;
        }


        /**
         * Inner class. Handles clicks on the text views.
         */
        private static class OnTextClickListener implements ContainerOnClickListener<SchoolEvent> {
            private SchoolEvent event = null;

            /**
             * The on click of the text takes you to the event details activity.
             *
             * @param view View that was clicked.
             */
            @Override
            public void onClick(View view) {
                Context context = view.getContext();

                // Set the path to contain the clicked event for the details activity to
                // reference the same event object.
                SchoolList.getInstance().path().setEvent(this.event);

                // Create a new intent to show the details screen, and start the event details
                // activity.
                Intent showEventDetailsIntent = new Intent(context, EventDetailsActivity.class);
                context.startActivity(showEventDetailsIntent);
            }

            /**
             * Binds a school event model to this listener in order to set the appropriate event
             * in path whenever it has been clicked. {@link OnTextClickListener#onClick(View)}
             *
             * @param event SchoolEvent Event to use with this listener.
             * @return This listener for chaining.
             */
            @Override
            public ContainerOnClickListener<SchoolEvent> bindModel(SchoolEvent event) {
                this.event = event;
                return this;
            }
        } // end class OnTextClickListener


        /**
         * Inner class. Handles clicks on the image view.
         */
        private static class OnImageClickListener implements ContainerOnClickListener<SchoolEvent> {
            private SchoolEvent event = null;

            /**
             * The on click of the image toggles the event's notification status.
             *
             * @param view View that has been clicked: a row in the recycler view.
             * @throws ClassCastException if this listener has been attached to anything but an
             *                            ImageView.
             */
            @Override
            public void onClick(View view) {

                // Get the context for easy access.
                Context context = view.getContext().getApplicationContext();

                // We know that the clicked view is the image view.
                ImageView imgView = (ImageView) view;

                // Get the image and toast string.
                Pair<Drawable, String> viewData = SupportController.getNotificationImageAndText(
                        context,
                        this.event.toggleNotify().isSetToNotify()
                );

                if (NotificationSpawner.instance != null) {
                    if (this.event.isSetToNotify()) {
                        NotificationSpawner.instance.setNotification(this.event.getUid());
                    } else {
                        NotificationSpawner.instance.unsetNotification(this.event.getUid());
                    }
                }

                // Set the picture.
                imgView.setImageDrawable(viewData.first);

                // Show a toast with confirming data.
                shortToast(context, viewData.second);
            }

            /**
             * Binds a school event model to this listener in order to detect a correct status of
             * the notification: has it been set or not. {@link OnImageClickListener#onClick(View)}
             *
             * @param event SchoolEvent Event to use with this listener.
             * @return This listener for chaining.
             */
            @Override
            public ContainerOnClickListener<SchoolEvent> bindModel(SchoolEvent event) {
                this.event = event;
                return this;
            }
        } // end OnImageClickListener

    }
    // ------------------------------------ END INNER CLASS ------------------------------------- //
}

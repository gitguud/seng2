package uk.ac.uea.gitguud.activityprogramapp.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

import uk.ac.uea.gitguud.framework.convenience.ErrorChecking;
import uk.ac.uea.gitguud.framework.convenience.SupportFuncs;
import uk.ac.uea.gitguud.framework.interfaces.ViewTextProvider;

/**
 * SchoolOpenDay.java
 *
 * Models an open day of a school.
 * Created by Pavel Solodilov on 05/11/16.
 */
public class SchoolOpenDay implements ViewTextProvider {
    private ArrayList<SchoolEvent> SCHOOL_EVENTS;
    private Date date;

    /**
     * Constructor for an open day object.
     *
     * @param date Date of the school open day.
     * @throws Exception if any of the provided values do not conform with the requirements
     *                   in appropriate setters.
     */
    public SchoolOpenDay(Date date) {
        this.SCHOOL_EVENTS = new ArrayList<>();
        this.setDate(date);
    }


    /**
     * Accessor for all of the events of the open day. No changes to the returned array list will
     * affect the the open day's inner ordering.
     *
     * @return ArrayList of all events of the open day.
     */
    public ArrayList<SchoolEvent> getEventsAsArrayList(){
        return new ArrayList<>(this.SCHOOL_EVENTS);
    }

    /**
     * Accessor for all of the events of the open day.
     *
     * @return Array of all events of the open day.
     */
    public SchoolEvent[] getSchoolEvents() {
        SchoolEvent[] events = new SchoolEvent[this.SCHOOL_EVENTS.size()];
        events = this.SCHOOL_EVENTS.toArray(events);
        return events;
    }

    /**
     * Formats a date to string in format: 26 November 2016.
     *
     * @return Text in appropriate format.
     */
    public String getText() {
        return SupportFuncs.dateToString(this.getDate());
    }

    /**
     * Accessor for the date of the open day.
     * <p>
     * Returns a copy of the corresponding object, and therefore any changes to it will not affect
     * the container.
     *
     * @return Date of the open day.
     */
    public Date getDate() {
        return new Date(this.date.getTime());
    }

    /**
     * Returns a school event with matching UID.
     *
     * @param uid UID to search for.
     * @return School event with the provided UID, or null if not found.
     */
    public SchoolEvent findEventByUid(String uid) {

        for (SchoolEvent event : this.SCHOOL_EVENTS) {
            if (event.getUid().equals(uid)) {
                return event;
            }
        } // end for each

        return null;
    }


    /**
     * Sets the date of the open day.
     * <p>
     * The provided date object will be copied, and therefore any modifications to the school open day
     * from outside of the object are impossible.
     *
     * @param date Date of the open day.
     * @return This open day object for chaining.
     * @throws NullPointerException if any part of the provided date is null. This check is here to
     *                              provide a more meaningful in-app debug error message to the
     *                              developers than if it was thrown by some other call within this
     *                              same method.
     */
    public SchoolOpenDay setDate(Date date) throws NullPointerException{
        ErrorChecking.getInstance().throwOnNull(date, "The date cannot be null");

        this.date = new Date(date.getTime());
        return this;
    }

    /**
     * Add a school event to the open day. Note that there are no checks whether this event is
     * within the bounds of the open day.
     *
     * @param schoolEvent School event to add to the open day. It will not be added if it is null.
     * @return True if the event has been added, and false otherwise.
     */
    public boolean addSchoolEvent(SchoolEvent schoolEvent) {

        if (schoolEvent != null && this.SCHOOL_EVENTS.add(schoolEvent)) {
            // If the school event is not null, and could successfully be added, sort the school
            // events by start date (start time) and return true.
            Collections.sort(this.SCHOOL_EVENTS, new SchoolEvent.CompareByStartDate());
            return true;
        }

        return false;
    }

    /**
     * Updates the contents (events) of this open day with another open day's ones. Current open day
     * will be referred to as THIS, and the other one as OTHER.
     * 1) If THIS has an event which is not in OTHER, it gets removed from THIS.
     * 2) If OTHER has an event which is not in this, it gets added to THIS.
     * 3) If THIS and OTHER have a same event with the same UID, data from the OTHER is going to be
     *    copied over into the event of THIS.
     *
     * @param openDay Open day to merge this one with.
     * @return This object for chaining.
     */
    public SchoolOpenDay mergeOpenDays(SchoolOpenDay openDay) {

        // This array list contains the events that will be left in this open day.
        ArrayList<SchoolEvent> matching = new ArrayList<>();

        boolean found = false;
        // Go through each event in the new events.
        for (SchoolEvent newEvent : openDay.SCHOOL_EVENTS) {

            // Try to match the new event in current events.
            for (SchoolEvent ownEvent : this.SCHOOL_EVENTS) {

                // If the UIDs match, copy the data from the new event into own event, and then add
                // the own event into the matching array list.
                if (newEvent.equals(ownEvent)) {
                    matching.add(ownEvent.copyFrom(newEvent));
                    found = true;
                    // Break out of the inner loop: a match has been found.
                    break;
                }
            }

            // If the event with same UID was not found in this open day, but it is in the new one,
            // add it as a new event.
            if (!found) {
                matching.add(newEvent);
            }

            // Reset the flag.
            found = false;

        } // end for each

        // Replace all of the school event references.
        this.SCHOOL_EVENTS.clear();
        this.SCHOOL_EVENTS.addAll(matching);
        return this;
    }


    /**
     * Checks whether this date is equal to the other one. Note that due to no real type checking,
     * you must make sure that the object you pass to this method is another SchoolOpenDay.
     * You will get an exception otherwise.
     *
     * @param other Other SchoolOpenDay to check.
     * @return True if two school open days happen on the same day (does not take exact time into
     *         consideration)
     */
    @Override
    public boolean equals(Object other) {
        SchoolOpenDay otherDay = (SchoolOpenDay) other;
        return SupportFuncs.sameDay(otherDay.date, this.date);
    }


    /**
     * Inner static comparator class. Used to compare two schools by date.
     */
    public static class CompareByDate implements Comparator<SchoolOpenDay> {
        public int compare(SchoolOpenDay d1, SchoolOpenDay d2) {
            return d1.getDate().compareTo(d2.getDate());
        }
    }
}

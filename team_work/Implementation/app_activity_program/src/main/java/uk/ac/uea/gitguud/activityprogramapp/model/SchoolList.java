package uk.ac.uea.gitguud.activityprogramapp.model;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;

import uk.ac.uea.gitguud.framework.AndroidFileIO;
import uk.ac.uea.gitguud.framework.SynchronisedObject;
import uk.ac.uea.gitguud.framework.Synchroniser;
import uk.ac.uea.gitguud.framework.convenience.SupportFuncs;
import uk.ac.uea.gitguud.framework.convenience.Tuple;
import uk.ac.uea.gitguud.framework.interfaces.ParseStrategy;
import uk.ac.uea.gitguud.framework.interfaces.SyncCallback;

/**
 * SchoolList.java
 *
 * Models a list of schools.
 * Created by Pavel Solodilov on 05/11/16.
 */
public class SchoolList extends SynchronisedObject<School> implements Serializable {
    private final ArrayList<School> SCHOOLS;
    private School defaultSchool;
    private final Path PATH;

    private final int DEFAULT_NOTIFICATION_OFFSET = SupportFuncs.TimeValue.TEN_MINUTES.SECONDS;
    private final int DEFAULT_SYNC_INTERVAL       = SupportFuncs.TimeValue.TEN_MINUTES.SECONDS;

    private Thread syncThread;

    private static SchoolList instance;

    /**
     * Default constructor for a list of schools.
     */
    private SchoolList() {
        this.setDefaultSchool(null);
        this.SCHOOLS = new ArrayList<>();
        this.PATH = new Path();
    }


    /**
     * Accessor for a school list instance; singleton pattern.
     *
     * @return Instance of a school list.
     */
    public static SchoolList getInstance() {
        if (SchoolList.instance == null) {
            instance = new SchoolList();
        }

        return SchoolList.instance;
    }


    /**
     *
     *
     * @param context
     * @param start
     * @return
     */
    public synchronized SchoolList autoSync(final Context context, boolean start) {

        if (!start) {
            // If the request was to stop.
            if (this.syncThread != null && this.syncThread.isAlive()) {
                // If the thread was not null and was alive, interrupt it.
                this.syncThread.interrupt();
            }
            // Otherwise just nullify it and return, since there is nothing to stop.
            this.syncThread = null;
            return this;
        }


        if (this.syncThread != null) {
            // If the request was to start the thread and it is already there, ignore this request.
            return this;
        }


        // If the request was to start a new thread and there isn't one, construct the thread's
        // runnable insides.
        Runnable runnable = new Runnable() {
            @Override
            public void run() {

                int interval;
                while (true) {
                    SchoolList facade = SchoolList.getInstance();
                    facade.synchroniseSchools(context, null, true);

                    // The interval is in seconds, but we need milliseconds.
                    interval = facade.getSyncInterval(context) * 1000;

                    // Interval of 0 means do not synchronise, so just break out of here.
                    if (interval == 0) break;

                    // Sleep for the amount specified by interval.
                    // If interrupted, break out of the loop and die.
                    try {
                        Thread.sleep(interval);
                    } catch (InterruptedException e) {
                        break;
                    }
                } // end while
            } // end run
        };


        // Create the thread and run it.
        this.syncThread = new Thread(runnable);
        this.syncThread.start();

        return this;
    }

    /**
     * Accessor for the offset of event notification.
     *
     * @param context Context of the app.
     * @return Offset in seconds. 0 means notify at the start of the event. Will return a default
     *         offset if it was not stored in shared preferences or the context was not provided.
     */
    public int getNotificationOffset(Context context) {

        if (context == null) return this.DEFAULT_NOTIFICATION_OFFSET;

        // Get the offset from shared preferences.
        int offset = AndroidFileIO.getInstance().getSharedPref(context)
                .getInt(ReadonlyResources.getInstance().KEY_NOTIF_OFFSET, -1);

        // Return the offset if found; otherwise return default.
        if (offset >= 0) {
            return offset;
        } else {
            return this.DEFAULT_NOTIFICATION_OFFSET;
        }
    }


    /**
     * Accessor for the synchronisation interval.
     *
     * @param context Context of the app.
     * @return Sync interval in seconds. 0 means do not synchronise. Will return a default
     *         interval if it was not stored in shared preferences or the context was not provided.
     */
    public int getSyncInterval(Context context) {
        if (context == null) return this.DEFAULT_SYNC_INTERVAL;

        int interval = AndroidFileIO.getInstance().getSharedPref(context)
                .getInt(ReadonlyResources.getInstance().KEY_SYNC_INTERVAL, -1);

        if (interval >= 0) {
            return interval;
        } else {
            return this.DEFAULT_SYNC_INTERVAL;
        }
    }


    /**
     * Mutator of the notification offset.
     *
     * @param context Context of the app.
     * @param offset Offset in seconds. 0 means notify at the start of the event.
     * @return This object for chaining.
     * @throws IllegalArgumentException if offset is negative.
     */
    public SchoolList setNotificationOffset(Context context, int offset) {
        if (offset < 0) {
            throw new IllegalArgumentException("The offset cannot be negative.");
        }

        // Use commit() over apply() to avoid race conditions.
        AndroidFileIO.getInstance().getSharedPref(context).edit()
                .putInt(ReadonlyResources.getInstance().KEY_NOTIF_OFFSET, offset).commit();

        return this;
    }


    /**
     * Mutator of the synchronisation interval. Will also try to restart the auto sync thread.
     *
     * @param context Context of the app.
     * @param interval Interval in seconds. 0 means do not synchronise.
     * @return This object for chaining.
     * @throws IllegalArgumentException if the interval is negative.
     */
    public SchoolList setSyncInterval(Context context, int interval) {
        if (interval < 0) {
            throw new IllegalArgumentException("The interval cannot be negative.");
        }

        // Use commit() over apply() to avoid race conditions.
        AndroidFileIO.getInstance().getSharedPref(context).edit()
                .putInt(ReadonlyResources.getInstance().KEY_SYNC_INTERVAL, interval).commit();

        return this.autoSync(context, true);
    }

    /**
     * Setter for a default school. Adds the passed object to the school list if it is not there
     * yet.
     *
     * @param defaultSchool Default school to set. Might be null for no default.
     * @return This object.
     */
    public SchoolList setDefaultSchool(School defaultSchool) {

        if (defaultSchool != null && !this.SCHOOLS.contains(defaultSchool)) {
            this.SCHOOLS.add(defaultSchool);
        }

        this.defaultSchool = defaultSchool;
        return this;
    }

    /**
     * Accessor for the path the app is in.
     *
     * @return The path that the app is in.
     */
    public Path path() {
        return this.PATH;
    }

    /**
     * Accessor for a default school.
     *
     * @return Default school. Might be null.
     */
    public School getDefaultSchool() {
        return this.defaultSchool;
    }

    /**
     * Adds a new school to the school list.
     *
     * @param school New school to add. Will not be added if it is null.
     * @return True if the school was added, and false otherwise.
     */
    public boolean addSchool(School school) {

        if (school != null) {
            return this.SCHOOLS.add(school);
        }

        return false;
    }


    /**
     * Accessor for all of the schools in the school list.
     *
     * @return Array of all schools in the list.
     */
    public School[] getSchools() {
        School[] schoolArr = new School[this.SCHOOLS.size()];
        schoolArr = this.SCHOOLS.toArray(schoolArr);

        return schoolArr;
    }


    /**
     * Accessor for all of the schools in the school list.
     *
     * @return ArrayList of all schools in the list.
     */
    public ArrayList<School> getSchoolsAsArrayList() {
        return new ArrayList<>(this.SCHOOLS);
    }


    /**
     * Deletes all of the schools of this school list.
     *
     * @return This (SchoolList) object.
     */
    public SchoolList clearSchools() {
        this.SCHOOLS.clear();
        return this;
    }


    /**
     * Returns a school event with matching UID.
     *
     * @param uid UID to search for.
     * @return School event with the provided UID, or null if not found.
     */
    public SchoolEvent findEventByUid(String uid) {
        SchoolEvent event = null;

        for (School school : this.SCHOOLS) {
            event = school.findEventByUid(uid);

            if (event != null) {
                return event;
            }
        }

        return event;
    }


    /**
     * This method downloads updates for all of the school open day data from online or offline.
     *
     * @param context Context of the app to use.
     * @param callback Object to callback with status.
     * @param network True to download from offline, false to try loading from offline.
     */
    public void synchroniseSchools(Context context, SyncCallback callback, boolean network) {

        // Go through all of the schools and download all of their data.
        for (Tuple.Three<String, String, ParseStrategy<School>> triple :
                ReadonlyResources.getInstance().getSchoolURIs()) {
            Synchroniser.getInstance().synchronise(
                    network ? triple.item1() : null,  // URI to download from, but only if asked to.
                    context,         // Context of this activity.
                    triple.item2(),  // Offline storage path.
                    triple.item3(),  // Parser.
                    this,            // Update the school list with new data.
                    callback         // Notify when done.
            );
        } // end for each
    }


    /**
     * Adds the given school objects to the list: replaces their information if they were already
     * present, or simply adds them if they were not.
     *
     * @param newSchools New set of school information to add.
     */
    @Override
    public synchronized void updateEntries(ArrayList<School> newSchools) {

        if (newSchools != null) {
            // Note that a school will never get deleted or completely emptied of events without prior
            // notification, so there is no need to handle the case when the school is present in the
            // list, but not present in the new schools array list.

            int index;
            for (School newSchool : newSchools) {
                // Get the index of the school if it is contained within the school array list.
                // Since indexOf uses the equals() method, make sure that it is comparing by name
                // (same schools should have the same names).
                index = this.SCHOOLS.indexOf(newSchool);

                // Merge schools if found, and add a new one if not.
                if (index >= 0) {
                    this.SCHOOLS.get(index).mergeSchools(newSchool);
                } else {
                    this.SCHOOLS.add(newSchool);
                }
            }

            // Remove redundant open days.
            this.clearRedunantOpenDays();

            // Sort the schools by name.
            Collections.sort(this.SCHOOLS, new School.CompareByName());
        }

        // Notify the attached observers of an update.
        this.changeNotify();
    }

    /**
     * Removes all of the open days that are in the past except for one (for reference).
     * For example, there is a number of open days present (today is 12th January):
     * 15th January - Future open day, so stays as is.
     * 12th January - This is today, so stays as is.
     * 10th January - This one is in the past, but keep this one for reference.
     *  8th January - This one gets removed, since it is in the past, but only a single day is kept
     *                as reference.
     *
     * @return This object for chaining.
     */
    private SchoolList clearRedunantOpenDays() {
        Date now = Calendar.getInstance().getTime();
        Date openDayDate;
        SchoolOpenDay referenceDay = null;
        ArrayList<SchoolOpenDay> keptDays = new ArrayList<>();

        // Loop through each of the schools' open days.
        for (School school : this.SCHOOLS) {
            for (SchoolOpenDay openDay : school.getOpenDays()) {

                openDayDate = openDay.getDate();

                // If the date is in the past, but not today, check whether to keep.
                /// If present or today, keep.
                if (openDayDate.before(now) && !SupportFuncs.sameDay(openDayDate, now)) {

                    // If the reference day is null or the reference day is before the currently
                    // checked date, set it as the reference day.
                    if (referenceDay == null || referenceDay.getDate().before(openDayDate)) {
                        referenceDay = openDay;
                    }
                } else {
                    keptDays.add(openDay);
                }
            } // end for

            // Replace the open days, and then clear the kept days list for the next iteration.
            school.clearOpenDays();

            if (referenceDay != null) {
                school.addOpenDay(referenceDay);
                referenceDay = null;
            }
            school.addOpenDays(keptDays);
            keptDays.clear();
        } // end for

        return this;
    }

    public class Path {
        private School school = null;
        private SchoolOpenDay openDay = null;
        private SchoolEvent event = null;

        /**
         * Default constructor
         */
        public Path() {
        }

        /**
         * Accessor method to get the school in the current path.
         *
         * @return School in the path of the user.
         */
        public School getSchool() {
            return this.school;
        }

        /**
         * Accessor method to get the open day in the current path.
         *
         * @return Open day in the path of the user.
         */
        public SchoolOpenDay getOpenDay() {
            return this.openDay;
        }

        /**
         * Accessor method to get the school event in the current path.
         *
         * @return School event that in the path of the user.
         */
        public SchoolEvent getEvent() {
            return this.event;
        }

        /**
         * Mutator method to set the school in the current path.
         *
         * @param school School in the path. May be null, but then event and open day must also be
         *               null.
         * @return This object for chaining.
         */
        public Path setSchool(School school) {
            this.school = school;
            return this;
        }

        /**
         * Mutator method to set the open day in the current path.
         * @param openDay Open day in the path. May be null, but then the event must also be null.
         *
         * @return This object for chaining.
         */

        public Path setOpenDay(SchoolOpenDay openDay) {
            this.openDay = openDay;
            return this;
        }

        /**
         * Mutator method to set the open day in the current path.
         * @param event School event in the path. May be null.
         *
         * @return This object for chaining.
         */
        public Path setEvent(SchoolEvent event) {
            this.event = event;
            return this;
        }
    } // end inner static class Path

}

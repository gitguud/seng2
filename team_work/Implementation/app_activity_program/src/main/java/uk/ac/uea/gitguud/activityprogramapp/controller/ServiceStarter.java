package uk.ac.uea.gitguud.activityprogramapp.controller;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;

/**
 * ServiceStarter.java
 * Starts the notification service on boot.
 *
 * Created by Pavel Solodilov on 22/01/17.
 */
public class ServiceStarter extends BroadcastReceiver {

    private static final String BOOT_COMPLETED = "android.intent.action.BOOT_COMPLETED";

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction() != null && intent.getAction().equals(BOOT_COMPLETED) &&
                NotificationSpawner.instance == null) {
            Intent serviceIntent = new Intent(context, NotificationSpawner.class);
            context.startService(serviceIntent);
        }
    }
}

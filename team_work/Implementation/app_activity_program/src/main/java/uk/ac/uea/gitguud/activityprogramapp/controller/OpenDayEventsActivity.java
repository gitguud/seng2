package uk.ac.uea.gitguud.activityprogramapp.controller;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;

import uk.ac.uea.gitguud.activityprogramapp.R;
import uk.ac.uea.gitguud.activityprogramapp.model.ReadonlyResources;
import uk.ac.uea.gitguud.activityprogramapp.model.SchoolEvent;
import uk.ac.uea.gitguud.activityprogramapp.model.SchoolList;
import uk.ac.uea.gitguud.activityprogramapp.model.SchoolOpenDay;
import uk.ac.uea.gitguud.activityprogramapp.view.SchoolEventsRecyclerViewAdapter;
import uk.ac.uea.gitguud.framework.convenience.SupportFuncs;
import uk.ac.uea.gitguud.framework.interfaces.StateObserver;
import uk.ac.uea.gitguud.framework.view.ViewFunctions;

import static uk.ac.uea.gitguud.framework.convenience.AndroidSpecific.shortToast;

/**
 * OpenDayEventsActivity.java
 *
 * Created by mircea on 15/11/16.
 */
public class OpenDayEventsActivity extends AppCompatActivity implements StateObserver {
    private RecyclerView recyclerView;
    private SchoolEventsRecyclerViewAdapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private SchoolOpenDay openDay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_open_day_events);

        // Get the recycler view, and set a layout manager for it.
        this.recyclerView = (RecyclerView) findViewById(R.id.guud_recycler_view);
        this.layoutManager = new LinearLayoutManager(this);
        this.recyclerView.setLayoutManager(this.layoutManager);

        SchoolList.Path path = SchoolList.getInstance().path();

        // Get the events from the open day contained in the path.
        // Afterwards, attach the adapter to the recycler view.
        this.openDay = path.getOpenDay();
        ArrayList<SchoolEvent> eventList = this.openDay.getEventsAsArrayList();

        // Create and set the toolbar.
        String toolbarText = SupportFuncs.dateToString(this.openDay.getDate()) + ", " +
                path.getSchool().getText();
        SupportController.createToolbar(this, getResources().getColor(R.color.white), toolbarText);

        this.adapter =
                new SchoolEventsRecyclerViewAdapter(eventList, R.layout.guud_text_text_pic_widget);
        this.recyclerView.setAdapter(adapter);

        // Set the background image
        Drawable img;
        if (SettingsActivity.backgroundImage == null) {
            ReadonlyResources r = ReadonlyResources.getInstance();
            img = ViewFunctions.loadBackgroundImage(
                    r.STORAGE_BACKGROUND_IMAGE_DIRECTORY,
                    r.STORAGE_BACKGROUND_IMAGE_FILE,
                    this
            );
            SettingsActivity.backgroundImage = img;
        } else {
            img = SettingsActivity.backgroundImage;
        }
        if (img != null) {
            ViewFunctions.setWhitenedImage(findViewById(R.id.guud_recycler_view), img);
        }

        // Attach this activity as a listener to the school list.
        SchoolList.getInstance().attach(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (!SupportController.onOptionsItemSelected(item, this, null)) {
            return super.onOptionsItemSelected(item);
        } else {
            return true;
        }
    }

    /**
     * Refreshes the model data in the view.
     */
    public synchronized void updateNotify() {

        SchoolList list = SchoolList.getInstance();

        // Search the school for the open day since two same dates may exist in different schools.
        // In addition, schools never get deleted, so this is safe unlike searching for an event by
        // UID from within an open day from the path.
        this.openDay = list.path().getSchool().findOpenDayByDate(this.openDay.getDate());

        if (this.openDay == null) {
            // If the open day has been deleted, remove it from path, notify the user, and go to
            // previous activity.
            list.path().setOpenDay(null);
            shortToast(this, getResources().getString(R.string.open_day_deleted));
            this.onBackPressed();
        } else {
            // Otherwise update the view data.
            this.adapter.setEvents(this.openDay.getEventsAsArrayList());

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    adapter.notifyDataSetChanged();
                }
            });
        }
    }

    /**
     * Override method that redraws all of the data if it has changed when the app is reopened.
     */
    @Override
    protected void onResume() {
        super.onResume();
        this.updateNotify();
    }

}

package uk.ac.uea.gitguud.activityprogramapp.controller;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.Spinner;

import java.io.FileNotFoundException;
import java.io.InputStream;

import uk.ac.uea.gitguud.activityprogramapp.R;
import uk.ac.uea.gitguud.activityprogramapp.model.ReadonlyResources;
import uk.ac.uea.gitguud.activityprogramapp.model.SchoolList;
import uk.ac.uea.gitguud.framework.AndroidFileIO;
import uk.ac.uea.gitguud.framework.convenience.AndroidSpecific;
import uk.ac.uea.gitguud.framework.convenience.SupportFuncs;
import uk.ac.uea.gitguud.framework.view.ViewFunctions;

import static uk.ac.uea.gitguud.framework.convenience.AndroidSpecific.shortToast;

public class SettingsActivity extends AppCompatActivity {

    private Spinner syncIntervalSpinner;
    private Spinner notificationOffsetSpinner;
    private ImageView backgroundImageView;
    private ScrollView background;

    public static Drawable backgroundImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.parseColor("#FFFFFFFF"));
        toolbar.setTitle(getResources().getString(R.string.action_settings));
        setSupportActionBar(toolbar);

        // Get references to views.
        this.backgroundImageView = (ImageView) findViewById(R.id.settings_background_image_image);
        this.syncIntervalSpinner = (Spinner)   findViewById(R.id.settings_sync_interval_spinner);
        this.notificationOffsetSpinner =
                (Spinner) findViewById(R.id.settings_notif_interval_spinner);
        this.background = (ScrollView) findViewById(R.id.activity_settings);

        // Create an adapter that will host time values for spinners.
        ArrayAdapter<SupportFuncs.TimeValue> adapter = new ArrayAdapter<>(
                this,                                           // Context.
                android.R.layout.simple_spinner_dropdown_item,  // Just a string for an item.
                SupportFuncs.TimeValue.values()                 // All values of the enum.
        );
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);


        // Use same values (adapters) for each of the spinners.
        this.notificationOffsetSpinner.setAdapter(adapter);
        this.syncIntervalSpinner.setAdapter(adapter);

        // Get the notification offset and the sync interval.
        SchoolList facade = SchoolList.getInstance();
        int offset = facade.getNotificationOffset(this);
        int interval = facade.getSyncInterval(this);

        // Set the notification offset and sync interval spinner values according to stored ones.
        this.notificationOffsetSpinner.setSelection(SupportFuncs.TimeValue.indexByValue(offset));
        this.syncIntervalSpinner.setSelection(SupportFuncs.TimeValue.indexByValue(interval));

        // Attach the same listener to different spinners (the types of listeners are too similar
        // to use two different ones.)
        SettingsOnItemSelectedListener listener = new SettingsOnItemSelectedListener();
        this.notificationOffsetSpinner.setOnItemSelectedListener(listener);
        this.syncIntervalSpinner.setOnItemSelectedListener(listener);

        this.loadBackgroundImage();
        this.backgroundImageView.setOnClickListener(new onImageClickListener());
    }


    /**
     * Loads and sets the custom background image if available.
     */
    private void loadBackgroundImage() {

        final Drawable bgImage;
        if (SettingsActivity.backgroundImage == null) {
            ReadonlyResources r = ReadonlyResources.getInstance();
            final Bitmap image = AndroidFileIO.getInstance().loadImageFromStorage(
                    r.STORAGE_BACKGROUND_IMAGE_DIRECTORY,
                    r.STORAGE_BACKGROUND_IMAGE_FILE,
                    this);
            bgImage = new BitmapDrawable(getResources(), image);
            SettingsActivity.backgroundImage = bgImage;
        } else {
            bgImage = SettingsActivity.backgroundImage;
        }

        if (bgImage != null) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    backgroundImageView.setImageDrawable(bgImage);
                    ViewFunctions.setWhitenedImage(background, bgImage);
                }
            });
        }
    }


    /**
     * This class is a listener for events of the settings' time-based spinners.
     */
    public class SettingsOnItemSelectedListener implements AdapterView.OnItemSelectedListener {

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
            // Get the time value.
            SupportFuncs.TimeValue time = (SupportFuncs.TimeValue) parent.getItemAtPosition(pos);

            // Get the spinner and detect which spinner was used by reference.
            Spinner spinner = (Spinner) parent;
            SchoolList facade = SchoolList.getInstance();

            if (view == null) return;
            Context context = view.getContext();

            if (spinner == notificationOffsetSpinner) {
                // Set the notification offset.
                facade.setNotificationOffset(context, time.SECONDS);
            } else if (spinner == syncIntervalSpinner) {
                // Set the synchronisation interval.
                facade.setSyncInterval(context, time.SECONDS);
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
            // Do nothing.
        }
    } // end class SettingsOnItemSelectedListener


    /**
     * Image selector dialog on click.
     */
    public class onImageClickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.setType("image/*");
            startActivityForResult(intent,
                    ReadonlyResources.getInstance().ACTION_SET_BACKGROUND_IMAGE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == ReadonlyResources.getInstance().ACTION_SET_BACKGROUND_IMAGE &&
                resultCode == Activity.RESULT_OK) {
            if (data == null) {
                shortToast(this, "Error: No data");
                return;
            }

            try {
                InputStream inputStream = this.getContentResolver().openInputStream(data.getData());
                Bitmap bitmap = BitmapFactory.decodeStream(inputStream);

                this.backgroundImageView.setImageBitmap(bitmap);

                ReadonlyResources r = ReadonlyResources.getInstance();
                AndroidFileIO.getInstance().saveImageToStorageAsync(
                        r.STORAGE_BACKGROUND_IMAGE_DIRECTORY,
                        r.STORAGE_BACKGROUND_IMAGE_FILE,
                        bitmap,
                        getApplicationContext()
                );

                Drawable newBackground = new BitmapDrawable(getResources(), bitmap);
                newBackground.setColorFilter(Color.parseColor("#A0FFFFFF"), PorterDuff.Mode.ADD);
                background.setBackground(newBackground);
                SettingsActivity.backgroundImage = newBackground;

            } catch (FileNotFoundException e) {
                e.printStackTrace();
                Log.e(AndroidSpecific.getLogTag(this), e.getMessage());
                shortToast(this, "Error: file not found???");
            }
        } // end if
    }

}

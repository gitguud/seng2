package uk.ac.uea.gitguud.activityprogramapp.model;

import android.location.Location;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.Comparator;
import java.util.Date;
import uk.ac.uea.gitguud.framework.convenience.ErrorChecking;
import uk.ac.uea.gitguud.framework.convenience.TimeSpan;

/**
 * SchoolEvent.java
 *
 * Models a school activity/event.
 * This class guarantees that none of its parameters are ever going to be null.
 *
 * Note on thread safety if we'll ever need it:
 * - Use the 'volatile' keyword if the object is going to be reassigned (e.g. has a setter).
 * - Synchronize on the object when using or modifying it in order not to allow changes from other
 *   methods to affect it. The synchronisation part does not apply to immutable objects such as
 *   strings. Note that any of these modifiable objects shall return their copies in their
 *   accessors, and not the objects themselves. Also, the setters would need to lock (synchronise)
 *   on their lock objects, since the object that is going to be set might be null.
 *
 * Created by Pavel Solodilov on 04/11/16.
 */
public class SchoolEvent {
    private Location location;

    private String uid;
    private String locationString;
    private String title;
    private String description;

    private TimeSpan time;
    private boolean notify = false;


    /**
     * Constructor for a school event.
     *
     * @param title Title.
     * @param location Location.
     * @param locationString Location as a string for users.
     * @param time Object describing the start and end times of the event.
     * @param description Description.
     * @throws Exception if any of the provided values do not conform with the requirements
     *                   in appropriate setters.
     */
    public SchoolEvent(String title, Location location, String locationString,
                       TimeSpan time, String description) {

        // Note the use of fluent programming style with this kind of chaining.
        this.setLocation(location, locationString)
                .setTime(time)
                .setTitle(title)
                .setDescription(description);
    }


    // ------------------------------------ ACCESSORS ------------------------------------------- //

    /**
     * Accessor for the UID of the event. Might be null.
     *
     * @return Unique identifier of this event.
     */
    public String getUid() {
        return this.uid;
    }

    /**
     * Accessor for the notification flag.
     *
     * @return True if set to notify about this event, and false otherwise.
     */
    public boolean isSetToNotify() {
        return this.notify;
    }

    /**
     * Accessor for a time span of the event.
     *
     * @return Time span of this event.
     */
    public TimeSpan getTimeSpan() {
        return this.time;
    }

    /**
     * Accessor for the location of the school event.
     * <p>
     * Returns a copy of the corresponding object, and therefore any changes to it will not affect
     * the container.
     *
     * @return Location.
     */
    public Location getLocation() {
        return new Location(this.location);
    }

    /**
     * Accessor for the human-readable location of the school event.
     *
     * @return Location string.
     */
    public String getLocationString() {
        return this.locationString;
    }

    /**
     * Accessor for the start time of the school event.
     * <p>
     * Returns a copy of the corresponding object, and therefore any changes to it will not affect
     * the container.
     *
     * @return Start time.
     */
    public Date getStartTime() {
        return this.time.getStartTime();
    }

    /**
     * Accessor for the end time of the school event.
     * <p>
     * Returns a copy of the corresponding object, and therefore any changes to it will not affect
     * the container.
     *
     * @return End time.
     */
    public Date getEndTime() {
        return this.time.getEndTime();
    }

    /**
     * Accessor for the title of the school event.
     *
     * @return Title.
     */
    public String getTitle() {
        return this.title;
    }

    /**
     * Accessor for the description of the school event.
     *
     * @return Description.
     */
    public String getDescription() {
        return this.description;
    }

    // ---------------------------------- END ACCESSORS ----------------------------------------- //


    // ------------------------------------ MUTATORS -------------------------------------------- //

    /**
     * Sets a unique identifier for this event.
     *
     * @param uid New UID.
     * @return This school event object for chaining.
     */
    public SchoolEvent setUid(String uid) {
        this.uid = uid;
        return this;
    }

    /**
     * Toggles the notification flag.
     *
     * @return This school event object for chaining.
     */
    public SchoolEvent toggleNotify() {
        this.notify = !this.notify;
        return this;
    }

    /**
     * Sets a new location of the school event.
     * <p>
     * The provided Location object will be copied, and therefore any modifications to the school
     * activity from outside of the object are impossible.
     *
     * @param location New location.
     * @param locationString Name of the new location.
     * @return This school event object for chaining.
     * @throws NullPointerException if any of the provided objects are null. This check is here to
     *                              provide a more meaningful in-app debug error message to the
     *                              developers than if it was thrown by some other call within this
     *                              same method.
     * @throws IllegalArgumentException if the location string is an empty string.
     */
    public SchoolEvent setLocation(Location location, String locationString)
            throws NullPointerException {

        ErrorChecking.getInstance()
                .throwOnNull(location, "Location cannot be null")
                .throwOnNull(locationString, "Location string cannot be null")
                .throwOnEmptyString(locationString, "Location string cannot be empty");

        this.location = new Location(location);
        this.locationString = locationString;

        return this;
    }

    /**
     * Sets when the school event is planned to start and end.
     * <p>
     *
     * @param time Time span of the event.
     * @return This school event object for chaining.
     * @throws NullPointerException if the provided object is null.
     */
    public SchoolEvent setTime(TimeSpan time) throws NullPointerException {
        ErrorChecking.getInstance().throwOnNull(time, "The time cannot be null.");

        this.time = time;
        return this;
    }

    /**
     * Sets the title of the school event.
     *
     * @param title New title.
     * @return This school event object for chaining.
     * @throws NullPointerException if the provided object is a null reference.
     * @throws IllegalArgumentException if the provided object is an empty string.
     */
    public SchoolEvent setTitle(String title) throws IllegalArgumentException {
        ErrorChecking.getInstance()
                .throwOnNull(title, "Event title cannot be null.")
                .throwOnEmptyString(title, "Event title cannot be an empty string.");

        this.title = title;
        return this;
    }

    /**
     * Sets the description of the school event.
     *
     * @param description Description to set.
     * @return This school event object for chaining.
     * @throws NullPointerException if the provided description is null.
     */
    public SchoolEvent setDescription(String description) throws NullPointerException {
        ErrorChecking.getInstance().throwOnNull(description, "Description cannot be null.");

        this.description = description;
        return this;
    }

    /**
     * Copies all of the entries from the given school event into this school event, except for the
     * UID and notification toggle.
     *
     * @param event Event to copy from. If null, does nothing.
     * @return This school event object for chaining.
     */
    public SchoolEvent copyFrom(SchoolEvent event) {

        if (event != null) {
            this.setTitle(event.getTitle())
                    .setTime(event.getTimeSpan())
                    .setLocation(event.getLocation(), event.getLocationString())
                    .setDescription(event.getTitle());
        }

        return this;
    }

    // ---------------------------------- END MUTATORS ------------------------------------------ //


    /**
     * Calculates the duration of the event.
     *
     * @return Duration in milliseconds.
     */
    public long getDurationInMs() {
        return (this.time.getEndTime().getTime() - this.time.getStartTime().getTime());
    }

    /**
     * Calculates the duration of the event.
     *
     * @return Duration in minutes.
     */
    public long getDurationInMinutes() {
        // Millisecond is 1/1000 of a second, and a minute has 60 seconds,
        // and therefore 1000*60 = 60000.
        return this.getDurationInMs() / 60000;
    }


    /**
     * Two school events are equal if they share the same UID.
     *
     * @param other Other school event to compare to.
     * @return True if the passed school event is equal to this one, and false otherwise.
     */
    @Override
    public boolean equals(Object other) {
        SchoolEvent otherEvent = (SchoolEvent) other;

        return this.getUid().equals(otherEvent.getUid());
    }


    /**
     * Static inner comparator class. Compares two school events by start date.
     */
    public static class CompareByStartDate implements Comparator<SchoolEvent> {
        public int compare(SchoolEvent e1, SchoolEvent e2) {
            return e1.getStartTime().compareTo(e2.getStartTime());
        }
    }

    /**
     * Static inner comparators class. Compares two school events by their UID.
     */
    public static class CompareByUid implements Comparator<SchoolEvent> {
        public int compare(SchoolEvent e1, SchoolEvent e2) {
            return e1.getUid().compareTo(e2.getUid());
        }
    }
}

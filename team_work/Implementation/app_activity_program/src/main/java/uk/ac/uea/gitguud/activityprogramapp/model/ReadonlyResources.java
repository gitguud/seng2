package uk.ac.uea.gitguud.activityprogramapp.model;

import java.util.ArrayList;

import uk.ac.uea.gitguud.framework.convenience.Tuple;
import uk.ac.uea.gitguud.framework.interfaces.ParseStrategy;

/**
 * ReadonlyResources.java
 *
 * Constains static resources like defaults and storage keys.
 * Created by Pavel Solodilov on 07/11/16.
 */
public final class ReadonlyResources {

    private static ReadonlyResources instance;

    /**
     * Accessor for an instance of the resources; singleton pattern.
     *
     * @return Instance of the resources.
     */
    public static ReadonlyResources getInstance() {
        if (ReadonlyResources.instance == null) {
            ReadonlyResources.instance = new ReadonlyResources();
        }

        return ReadonlyResources.instance;
    }

    // Shared preferences' keys.
    public final String KEY_SYNC_INTERVAL = "key_synchronisation_interval_stored";
    public final String KEY_NOTIF_OFFSET  = "key_notification_offset_stored";
    public final String KEY_SHARED_PREF_NOTIFICATION_IDS = "key_notification_ids";

    // Image storage strings.
    public final String STORAGE_BACKGROUND_IMAGE_FILE = "storage_background_image";
    public final String STORAGE_BACKGROUND_IMAGE_DIRECTORY = "images";

    // Activity action number; arbitrary.
    public final int ACTION_SET_BACKGROUND_IMAGE = 1000;




    // Contains the school calendar URI, file name to store the data in, and the parser for the data
    // fetched from the URI location. TODO add configurable school names.
    private ArrayList<Tuple.Three<String, String, ParseStrategy<School>>> schoolURIs;

    private ReadonlyResources() {
        this.schoolURIs = new ArrayList<>();

        // PPL
        this.schoolURIs.add(
            new Tuple.Three<String, String, ParseStrategy<School>>(
                "https://calendar.google.com/calendar/ical/eqnkahp5vv0h4k5r4lil3vt0n8%40group.calendar.google.com/public/basic.ics",
                "PPL_School",
                new IcalToSchoolParser()
            )
        );

        // CMP
        this.schoolURIs.add(
            new Tuple.Three<String, String, ParseStrategy<School>>(
                "https://calendar.google.com/calendar/ical/tkvnpvj95utul59dtjke1d0p4g%40group.calendar.google.com/public/basic.ics",
                "CMP_School",
                new IcalToSchoolParser()
            )
        );

        // Generic
        this.schoolURIs.add(
            new Tuple.Three<String, String, ParseStrategy<School>>(
                "https://calendar.google.com/calendar/ical/s3n13ja5a02ndv5pste927vo68%40group.calendar.google.com/public/basic.ics",
                "UEA_Generic",
                new IcalToSchoolParser()
            )
        );
    }

    /**
     * Accessor for all of the data needed to fetch, save and parse school open day data. Each entry
     * corresponds to a single school.
     *
     * @return Array list of triples containing the URI to download school data from, followed by an
     *         offline location to store the data at, and an independent parse strategy to parse the
     *         data.
     */
    public ArrayList<Tuple.Three<String, String, ParseStrategy<School>>> getSchoolURIs() {
        return this.schoolURIs;
    }
}

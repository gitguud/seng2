package uk.ac.uea.gitguud.activityprogramapp.controller;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

import uk.ac.uea.gitguud.activityprogramapp.R;
import uk.ac.uea.gitguud.activityprogramapp.model.ReadonlyResources;
import uk.ac.uea.gitguud.activityprogramapp.model.School;
import uk.ac.uea.gitguud.activityprogramapp.model.SchoolEvent;
import uk.ac.uea.gitguud.activityprogramapp.model.SchoolList;
import uk.ac.uea.gitguud.framework.AndroidFileIO;
import uk.ac.uea.gitguud.framework.NotificationFacade;
import uk.ac.uea.gitguud.framework.convenience.AndroidSpecific;

import static uk.ac.uea.gitguud.framework.convenience.AndroidSpecific.shortToast;

/**
 * NotificationSpawner.java
 * Creates notifications, and loads and stores data about upcoming event to be notified about.
 *
 * Created by Pavel Solodilov on 22/01/17.
 */
public class NotificationSpawner extends Service {
    private final Object EVENT_UIDS_LOCK = new Object();
    private ArrayList<String> eventUIDs;
    private SharedPreferences prefs;

    private boolean keepRunning = true;
    public static NotificationSpawner instance;

    private ArrayList<String> eventUIDsToRemove = new ArrayList<>();

    /**
     * Sets up the service.
     *
     * @param intent {@link Service#onStartCommand(Intent, int, int)}
     * @param flags {@link Service#onStartCommand(Intent, int, int)}
     * @param startId {@link Service#onStartCommand(Intent, int, int)}
     * @return {@link Service#onStartCommand(Intent, int, int)}
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        this.prefs = AndroidFileIO.getInstance().getSharedPref(getApplicationContext());

        // http://stackoverflow.com/a/22985657
        Gson gson = new Gson();
        String json = prefs.getString(
                ReadonlyResources.getInstance().KEY_SHARED_PREF_NOTIFICATION_IDS,
                null
        );

        if (json != null) {
            Type type = new TypeToken<ArrayList<String>>() {}.getType();
            this.eventUIDs = gson.fromJson(json, type);
        }

        if (this.eventUIDs == null) {
            this.eventUIDs = new ArrayList<>();
        }

        NotificationSpawner.instance = this;
        this.runLoop();

        return Service.START_STICKY;
    }

    /**
     * Checks whether to fire a notification every 30 seconds.
     */
    private void runLoop() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (keepRunning) {
                    long offset = prefs.getInt(
                            ReadonlyResources.getInstance().KEY_NOTIF_OFFSET, 600) * 1000;

                    synchronized (EVENT_UIDS_LOCK) {
                        for (String uid : eventUIDs) {
                            for (School school : SchoolList.getInstance().getSchools()) {
                                if (foundUID(school, offset, uid)) break;
                            }
                        } // end for
                    }

                    SchoolEvent event;
                    for (String uid : eventUIDsToRemove) {
                        unsetNotification(uid);
                        for (School school : SchoolList.getInstance().getSchools()) {
                            event = school.findEventByUid(uid);
                            if (event != null) {
                                event.toggleNotify();
                                SchoolList.getInstance().updateEntries(null);
                                break;
                            }
                        }
                    }

                    try {
                        Thread.sleep(30000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            } // end run()
        }).start();
    }

    /**
     * Fires up a notification if the given UID was found, and it is time to run the notification.
     *
     * @param school School that contains the needed event.
     * @param offset Amount of milliseconds before firing the notification.
     * @param uid UID of the event to check.
     * @return True if the notification was run, and false if not.
     */
    private boolean foundUID(School school, long offset, String uid) {
        SchoolEvent event = school.findEventByUid(uid);
        if (event != null) {
            if (event.getStartTime().getTime() - System.currentTimeMillis() <= offset) {
                Intent intent = new Intent();
                NotificationCompat.Builder builder =
                        new NotificationCompat.Builder(getApplicationContext());

                // TODO change icon to something more appropriate.
                builder.setSmallIcon(R.drawable.ic_tv_dark)
                        .setContentTitle("Event Reminder")
                        .setContentText("Event " + event.getTitle() + " starts in " +
                                offset/1000/60 + " minutes at " + event.getLocationString());

                NotificationFacade.getInstance().displayNotification(intent, builder,
                        getApplicationContext(), SchoolListActivity.class);

                this.eventUIDsToRemove.add(uid);
            }
            return true;
        }

        return false;
    }

    /**
     * Returns null: unused.
     *
     * @param intent
     * @return
     */
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    /**
     * Sets up event to be notified.
     *
     * @param uid UID of the event.
     * @return This object for chaining.
     */
    public NotificationSpawner setNotification(String uid) {
        synchronized (this.EVENT_UIDS_LOCK) {
            this.eventUIDs.add(uid);
            this.storeIds();
        }
        return this;
    }


    /**
     * Set not to notify about the event.
     *
     * @param uid UID of the event not to notify.
     * @return This object for chaining.
     */
    public NotificationSpawner unsetNotification(String uid) {
        synchronized (this.EVENT_UIDS_LOCK) {
            this.eventUIDs.remove(uid);
            this.storeIds();
        }
        return this;
    }

    /**
     * Stores the UIDs to be notified about synchronously.
     */
    private void storeIds() {
        // http://stackoverflow.com/a/22985657
        SharedPreferences.Editor editor = prefs.edit();

        Gson gson = new Gson();

        String json = gson.toJson(this.eventUIDs);

        editor.putString(ReadonlyResources.getInstance().KEY_SHARED_PREF_NOTIFICATION_IDS, json);
        editor.commit();
    }

    /**
     * Checks whether notification is set for the given UID.
     *
     * @param uid Event UID to check.
     * @return True if the UID is set, and false otherwise.
     */
    public boolean isSetNotification(String uid) {
        synchronized (this.EVENT_UIDS_LOCK) {
            return this.eventUIDs.contains(uid);
        }
    }
}
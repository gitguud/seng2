package uk.ac.uea.gitguud.activityprogramapp.model;
import android.location.Location;
import android.support.v4.util.Pair;

import java.util.ArrayList;
import java.util.Hashtable;

import uk.ac.uea.gitguud.framework.convenience.AndroidSpecific;
import uk.ac.uea.gitguud.framework.convenience.Tuple;

/**
 * CampusLocations.java
 *
 * Contains campus locations with their acronyms, full names, and coordinate locations.
 * Created by Anastassia Segeda on 12/8/2016.
 */
public class CampusLocations {
    private static CampusLocations instance;

    /**
     * Accessor for an instance of locations; singleton pattern.
     *
     * @return instance of location data.
     */
    public static CampusLocations getInstance() {
        if (instance == null) {
            instance = new CampusLocations();
        }

        return instance;
    }

    /*Locations with missing acronyms/coords
    Location					Acronym	Latitude	Longitude
    School of Engineering
    School of Natural Sciences
     */

    private ArrayList<Tuple.Four<String, String, Double, Double>> locationData = new ArrayList<>();
    private Hashtable<String, Pair<String, Location>> locations;

    /**
     * Constructor for campus locations.
     */
    private CampusLocations() {
        this.locations = new Hashtable<>();

        // Location code, location string, latitude, longitude.
        this.locationData.add(new Tuple.Four<>("ZICER", "Zuckerman Institue for Connective Environmental Research", 52.62170696, 1.23765707));
        this.locationData.add(new Tuple.Four<>("CRU", "Climatic Research Unit", 52.621784, 1.238556));
        this.locationData.add(new Tuple.Four<>("INTO", "INTO UEA", 52.623531, 1.24535));
        this.locationData.add(new Tuple.Four<>("NBS", "Norwich Business School", 52.62204, 1.2362));
        this.locationData.add(new Tuple.Four<>("MED", "Norwich Medical School", 52.62188445, 1.234918535));
        this.locationData.add(new Tuple.Four<>("NRP", "Norwich Research Park", 52.624611, 1.224031));
        this.locationData.add(new Tuple.Four<>("AMA", "Art, Media and American Studies", 52.62205, 1.24054));
        this.locationData.add(new Tuple.Four<>("BIO", "School of Biological Sciences", 52.62073, 1.23655));
        this.locationData.add(new Tuple.Four<>("CHE", "School of Chemistry", 52.621536, 1.239213));
        this.locationData.add(new Tuple.Four<>("CMP", "School of Computing Sciences", 52.620902, 1.237376));
        this.locationData.add(new Tuple.Four<>("ECO", "School of Economics", 52.622316, 1.240763));
        this.locationData.add(new Tuple.Four<>("EDU", "School of Education and Lifelong Learning", 52.621639, 1.237255));
        this.locationData.add(new Tuple.Four<>("ENV", "School of Environmental Sciences", 52.621433, 1.23822));
        this.locationData.add(new Tuple.Four<>("HIS", "History", 52.62211, 1.24053));
        this.locationData.add(new Tuple.Four<>("DEV", "School of International Development", 52.62194, 1.24033));
        this.locationData.add(new Tuple.Four<>("PPL", "Politics, Philosophy, Language and Communications Studies", 52.62189, 1.24035));
        this.locationData.add(new Tuple.Four<>("LDC", "Literature, Drama and Creative Writing", 52.62224, 1.24083));
        this.locationData.add(new Tuple.Four<>("MTH", "School of Mathematics", 52.62101, 1.23769));
        this.locationData.add(new Tuple.Four<>("HSC", "School of Health Sciences", 52.61961, 1.22324));
        this.locationData.add(new Tuple.Four<>("PHA", "School of Pharmacy", 52.62154, 1.2395));
        this.locationData.add(new Tuple.Four<>("PSY", "School of Psychology", 52.62137, 1.23732));
        this.locationData.add(new Tuple.Four<>("SWK", "School of Social Work", 52.62156, 1.23557));
        this.locationData.add(new Tuple.Four<>("LAW", "Law School", 52.625523, 1.236653));
        this.locationData.add(new Tuple.Four<>("GS", "Graduate School", 52.621727, 1.222765));
        this.locationData.add(new Tuple.Four<>("IIH", "Interdisciplinary Institute for the Humanities", 52.620696, 1.241978));
        this.locationData.add(new Tuple.Four<>("LT", "Lecture Theatre", 52.621258, 1.240329));
        this.locationData.add(new Tuple.Four<>("LIB", "Library", 52.620899, 1.240648));
        this.locationData.add(new Tuple.Four<>("TEC", "The Enterprise Centre", 52.625375, 1.239803));
        this.locationData.add(new Tuple.Four<>("SP", "Sportspark", 52.624466, 1.240854));
        this.locationData.add(new Tuple.Four<>("CHALL", "Congregation Hall", 52.621994, 1.242635));
        this.locationData.add(new Tuple.Four<>("CCEN", "CareerCentral", 52.621298, 1.241192));
        this.locationData.add(new Tuple.Four<>("CHAP", "Multifaith Centre", 52.621799, 1.240608));
        this.locationData.add(new Tuple.Four<>("UH", "Union House", 52.621559, 1.241468));
        this.locationData.add(new Tuple.Four<>("SCVA", "Sainsbury Centre for Visual Arts", 52.620365, 1.234653));
        this.locationData.add(new Tuple.Four<>("ITCS", "ITCS Building", 52.62134, 1.23953));
        this.locationData.add(new Tuple.Four<>("JSC", "Julian Study Centre", 52.622151, 1.234079));
        this.locationData.add(new Tuple.Four<>("EFRY", "Elizabeth Fry Building", 52.62153925, 1.235387921));
        this.locationData.add(new Tuple.Four<>("LSB", "Lawrence Stenhouse Building", 52.62143503, 1.237353981));
        this.locationData.add(new Tuple.Four<>("DRA", "The Drama Studio", 52.621815, 1.242362));
        this.locationData.add(new Tuple.Four<>("JPLC", "James Platt Language Centre", 52.62186, 1.24027));
        this.locationData.add(new Tuple.Four<>("BDS", "Blackdale Building", 52.622505, 1.241659));
        this.locationData.add(new Tuple.Four<>("REG", "Registry Building", 52.622505, 1.241659));
        this.locationData.add(new Tuple.Four<>("QUEENS", "Queen's Building", 52.62120218, 1.236128211));
        this.locationData.add(new Tuple.Four<>("TPST", "Thomas Paine Study Centre", 52.621895, 1.236076));
        this.locationData.add(new Tuple.Four<>("EH", "Earlham Hall", 52.625499, 1.236646));
        this.locationData.add(new Tuple.Four<>("ECB", "Edith Cavell Building", 52.61972, 1.22333));
        this.locationData.add(new Tuple.Four<>("EST", "Estates Building", 52.62215148, 1.238343716));
        this.locationData.add(new Tuple.Four<>("BMRC", "Bio-Medical Research Centre", 52.62106866, 1.236739755));
        this.locationData.add(new Tuple.Four<>("JIC", "John Innes Centre", 52.621315, 1.222208));
        this.locationData.add(new Tuple.Four<>("BEC", "Biomass Energy Centre", 52.62364948, 1.244850755));
        this.locationData.add(new Tuple.Four<>("HLB", "Hubert Lamb Building", 52.62184374, 1.238560975));
        this.locationData.add(new Tuple.Four<>("INTEROF", "International Office", 52.62167, 1.23723));
        this.locationData.add(new Tuple.Four<>("STABR", "Study Abroad", 52.62189, 1.24033));
        this.locationData.add(new Tuple.Four<>("SQUARE", "The Square", 52.621742, 1.241021));
        this.locationData.add(new Tuple.Four<>("ARTS", "Arts Building", 52.621877, 1.240029));

        String key;
        String locationString;
        Location location;

        for (Tuple.Four<String, String, Double, Double> loc : this.locationData){

            // Gather the data.
            key = loc.item1();
            locationString = loc.item2();

            // Create and set the location.
            location = new Location(AndroidSpecific.getLogTag(this));
            location.setLatitude(loc.item3());
            location.setLongitude(loc.item4());

            // Add the locations to the hash table.
            this.locations.put(key, Pair.create(locationString, location));
        }
    }

    /**
     * Accessor method for location details.
     *
     * @param key key pointing to the location. Acronym, or in case the acronym is missing the full
     *            case-insensitive key.
     * @return Pair containing location data, where the first entry is the location string, and the
     *         second one is the actual location.
     */
    public Pair<String, Location> getLocationByKey(String key) {
        return this.locations.get(key);
    }

}

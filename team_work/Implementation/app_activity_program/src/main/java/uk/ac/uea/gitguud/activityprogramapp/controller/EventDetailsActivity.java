package uk.ac.uea.gitguud.activityprogramapp.controller;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;


import uk.ac.uea.gitguud.activityprogramapp.R;
import uk.ac.uea.gitguud.activityprogramapp.model.ReadonlyResources;
import uk.ac.uea.gitguud.activityprogramapp.model.SchoolEvent;
import uk.ac.uea.gitguud.activityprogramapp.model.SchoolList;
import uk.ac.uea.gitguud.framework.convenience.SupportFuncs;
import uk.ac.uea.gitguud.framework.interfaces.StateObserver;
import uk.ac.uea.gitguud.framework.view.ViewFunctions;

import static uk.ac.uea.gitguud.framework.convenience.AndroidSpecific.shortToast;

/**
 * EventDetailsActivity.java
 *
 * Created by mircea on 21/11/16.
 */
public class EventDetailsActivity extends AppCompatActivity implements OnMapReadyCallback,
        StateObserver {
    private SchoolEvent event;

    private TextView eventTitle;
    private TextView eventTime;
    private TextView eventLocation;
    private TextView eventDetails;

    private Button eventReminderButton;
    private ImageView eventReminderImg;

    private SupportMapFragment map;

    /**
     * Method used when instance is created to format event information
     * @param savedInstanceState all event information
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_details);

        SchoolList facade = SchoolList.getInstance();
        SchoolList.Path path = facade.path();

        this.map =
                (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.event_map);
        this.event = path.getEvent();

        // Set up the toolbar.
        String toolbarText = SupportFuncs.dateToString(path.getOpenDay().getDate()) + ", " +
                path.getSchool().getText();
        SupportController.createToolbar(this, getResources().getColor(R.color.white), toolbarText);

        // Get references to views.
        this.eventTitle          = (TextView)  findViewById(R.id.event_title);
        this.eventLocation       = (TextView)  findViewById(R.id.event_location_string);
        this.eventTime           = (TextView)  findViewById(R.id.event_time);
        this.eventReminderButton = (Button)    findViewById(R.id.event_reminder_set_button);
        this.eventReminderImg    = (ImageView) findViewById(R.id.event_reminder_image);
        this.eventDetails        = (TextView)  findViewById(R.id.event_details);


        // Attach a listener to the reminder button.
        this.eventReminderButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                event.toggleNotify();
                setNotificationButtonData();
                if (event.isSetToNotify()) {
                    NotificationSpawner.instance.setNotification(event.getUid());
                } else {
                    NotificationSpawner.instance.unsetNotification(event.getUid());
                }
            }
        });

        // Attach this activity as a listener to the school list.
        facade.attach(this);

        // Set the background image
        Drawable img;
        if (SettingsActivity.backgroundImage == null) {
            ReadonlyResources r = ReadonlyResources.getInstance();
            img = ViewFunctions.loadBackgroundImage(
                    r.STORAGE_BACKGROUND_IMAGE_DIRECTORY,
                    r.STORAGE_BACKGROUND_IMAGE_FILE,
                    this
            );
            SettingsActivity.backgroundImage = img;
        } else {
            img = SettingsActivity.backgroundImage;
        }

        if (img != null) {
            ViewFunctions.setWhitenedImage(findViewById(R.id.event_scroll_view), img);
        }

        // Set the view data, and get the map.
        this.setViewData().map.getMapAsync(this);
    }

    /**
     * Creates the buttons on the action bar.
     *
     * @param menu Action bar to attach buttons to.
     * @return True if succeeded.
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (!SupportController.onOptionsItemSelected(item, this, null)) {
            return super.onOptionsItemSelected(item);
        } else {
            return true;
        }
    }

    /**
     * Sets an appropriate size of the map.
     *
     * @return This object for chaining.
     */
    private EventDetailsActivity setMapSize() {

        // Get the map container and its parameters.
        View mapContainer = findViewById(R.id.event_map_container);
        ViewGroup.LayoutParams params = mapContainer.getLayoutParams();

        // Then get the display size.
        Point size = new Point();
        getWindowManager().getDefaultDisplay().getSize(size);

        // Calculate the height of the map as 70% of the screen height and set it.
        params.height = (int) (size.y * 0.7);
        mapContainer.setLayoutParams(params);


        // Now make sure that the map is usable inside a scroll view.
        // See http://stackoverflow.com/a/17317176
        final ScrollView SCROLL_VIEW = (ScrollView) findViewById(R.id.event_scroll_view);
        ImageView transparentImageView = (ImageView) findViewById(R.id.event_map_transparent_img);

        transparentImageView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        // Disallow ScrollView to intercept touch events.
                        SCROLL_VIEW.requestDisallowInterceptTouchEvent(true);

                        // Disable touch on transparent view
                        return false;
                    case MotionEvent.ACTION_UP:
                        // Allow ScrollView to intercept touch events.
                        SCROLL_VIEW.requestDisallowInterceptTouchEvent(false);

                        return true;
                    case MotionEvent.ACTION_MOVE:
                        SCROLL_VIEW.requestDisallowInterceptTouchEvent(true);

                        return false;
                    default:
                        return true;
                }
            }
        });

        return this;
    }

    /**
     * Sets the view data for all of the text views, image views, etc.
     *
     * @return This activity for chaining.
     */
    private EventDetailsActivity setViewData() {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                eventTitle.setText(event.getTitle());
                eventLocation.setText(getResources().getString(R.string.location_prefix) + " " +
                         event.getLocationString()); // TODO string placeholders instead of concatenation.

                setNotificationButtonData();

                eventTime.setText(SupportFuncs.timespanToTime(event.getTimeSpan()));
                eventDetails.setText(event.getDescription());
            }
        });

        return this;
    }

    /**
     * Sets the notification button's image and text depending on the state of the event.
     *
     * @return This activity for chaining.
     */
    private EventDetailsActivity setNotificationButtonData() {
        // Get the image and text.
        Pair<Drawable, String> viewData = SupportController.getNotificationImageAndText(
                getApplicationContext(),
                this.event.isSetToNotify(),
                R.string.reminder_unset_btn,
                R.string.reminder_set_btn
        );

        Drawable reminderImg = viewData.first;
        String buttonText = viewData.second;

        // Set the image and text.
        this.eventReminderImg.setImageDrawable(reminderImg);
        this.eventReminderButton.setText(buttonText);

        return this;
    }

    /**
     * Custom callback for whenever the map has been loaded and is ready to use.
     *
     * @param map GoogleMap Initialised map object.
     */
    @Override
    public void onMapReady(GoogleMap map) {

        // Set the map size.
        this.setMapSize();

        // Set the location and move camera to it.
        Location location = this.event.getLocation();
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());

        map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 18));

        // Add a marker to the map.
        map.addMarker(new MarkerOptions()
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_place_black_24dp))
                .position(latLng));

        map.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        // Check if location permissions are enabled. Turn on the location services if the necessary
        // permissions are granted, and otherwise just finish the method execution.
        boolean fineLocationPermission = ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;
        boolean coarseLocationPermission = ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED;

        if (fineLocationPermission || coarseLocationPermission) {
            map.setMyLocationEnabled(true);
        }
    }

    /**
     *
     */
    public synchronized void updateNotify() {

        SchoolList list = SchoolList.getInstance();

        if (list.findEventByUid(this.event.getUid()) == null) {
            // If the event has been deleted, remove it from path, notify the user, and go to
            // previous activity.
            list.path().setEvent(null);
            shortToast(this, getResources().getString(R.string.event_deleted));
            this.onBackPressed();
        } else {
            // Otherwise update the view data.
            this.setViewData().map.getMapAsync(this);
        }
    }
}

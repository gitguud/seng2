package uk.ac.uea.gitguud.activityprogramapp.controller;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ExpandableListView;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import uk.ac.uea.gitguud.activityprogramapp.R;
import uk.ac.uea.gitguud.activityprogramapp.model.ReadonlyResources;
import uk.ac.uea.gitguud.activityprogramapp.model.School;
import uk.ac.uea.gitguud.activityprogramapp.model.SchoolList;
import uk.ac.uea.gitguud.activityprogramapp.model.SchoolOpenDay;
import uk.ac.uea.gitguud.framework.convenience.AndroidSpecific;
import uk.ac.uea.gitguud.framework.interfaces.ExpandableListAdapterResourceProvider;
import uk.ac.uea.gitguud.framework.interfaces.StateObserver;
import uk.ac.uea.gitguud.framework.interfaces.SyncCallback;
import uk.ac.uea.gitguud.framework.view.GuudExpandableListAdapter;
import uk.ac.uea.gitguud.framework.view.ViewFunctions;

import static uk.ac.uea.gitguud.framework.convenience.AndroidSpecific.shortToast;

/**
 * SchoolListActivity.java
 *
 * Represents a list of schools and their open days.
 * Created by GitGuud.
 */
public class SchoolListActivity extends AppCompatActivity implements SyncCallback, StateObserver,
        ExpandableListAdapterResourceProvider {

    private GuudExpandableListAdapter<School, SchoolOpenDay> expandableListAdapter;
    private ExpandableListView expandableListView;

    // http://kevinpelgrims.com/blog/2012/06/25/android-development-tips-style-inheritance/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Set up the layout and the toolbar.
        setContentView(R.layout.activity_school_list);
        SupportController.createToolbar(this, -0.5, null);

        // Set an adapter for the expandable list view.
        this.expandableListView =
                (ExpandableListView) findViewById(R.id.guud_expandable_list);
        this.expandableListAdapter = new GuudExpandableListAdapter<>(this, null, null, this);
        this.expandableListView.setAdapter(this.expandableListAdapter);
        this.expandableListView.setOnChildClickListener(this.new OnOpenDayClickListener());

        // Attach this activity as a listener to the school list.
        SchoolList facade = SchoolList.getInstance();
        facade.attach(this);

        // Set the background image.
        Drawable img;
        if (SettingsActivity.backgroundImage == null) {
            ReadonlyResources r = ReadonlyResources.getInstance();
            img = ViewFunctions.loadBackgroundImage(
                    r.STORAGE_BACKGROUND_IMAGE_DIRECTORY,
                    r.STORAGE_BACKGROUND_IMAGE_FILE,
                    this
            );
            SettingsActivity.backgroundImage = img;
        } else {
            img = SettingsActivity.backgroundImage;
        }
        if (img != null) {
            ViewFunctions.setWhitenedImage(findViewById(R.id.guud_expandable_list), img);
        }


        // Start the background notification service if not started yet.
        if (NotificationSpawner.instance == null) {
            Intent serviceIntent = new Intent(getApplicationContext(), NotificationSpawner.class);
            getApplicationContext().startService(serviceIntent);
        }

        // Synchronise the schools from online.
        facade.synchroniseSchools(this, this, true);
    }

    /**
     * Reacts to the toolbar's items being clicked.
     * If the sync button has been tapped, tries to synchronise. Else if the settings button has
     * been tapped, opens the settings activity.
     *
     * @param item Toolbar menu item that has been clicked.
     * @return True if succeeded.
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (!SupportController.onOptionsItemSelected(item, this, this)) {
            return super.onOptionsItemSelected(item);
        } else {
            return true;
        }
    }


    /**
     * Adds the toolbar into the view.
     *
     * @param menu Reference to the toolbar's menu.
     * @return True if succeeded. Arbitrary; always returns true.
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


    /**
     * Handles a click on an open day.
     * A click leads to starting an activity containing the events of the open day.
     */
    public class OnOpenDayClickListener implements ExpandableListView.OnChildClickListener {
        @Override
        public boolean onChildClick(ExpandableListView parent, View v, int groupPos, int childPos,
                                    long id) {
            // Get the school open day and school from the parameters using the adapter.
            School school     = expandableListAdapter.getGroupConcrete(groupPos);
            SchoolOpenDay day = expandableListAdapter.getChildConcrete(groupPos, childPos);

            // Construct an intent to open the school events activity.
            Context context = v.getContext();
            Intent showEventsIntent = new Intent(context, OpenDayEventsActivity.class);

            // Add the school and open day into path, and start the screen.
            SchoolList.getInstance().path().setSchool(school).setOpenDay(day);
            context.startActivity(showEventsIntent);
            return true;
        }
    }

    /**
     * Reacts to synchroniser's callbacks.
     *
     * @param status Status of the synchroniser's action.
     */
    public void onSync(SyncCallback.Status status) {

        Log.d(AndroidSpecific.getLogTag(this), "GOT STATUS: " + status.toString());

        switch(status) {
            case FAIL_ONLINE_FETCH:
                // If online fetch failed, try to fetch from offline.
                shortToast(this, R.string.err_online_fetch);
                SchoolList.getInstance().synchroniseSchools(this, this, false);
                break;

            case FAIL_OFFLINE_FETCH:
                // If offline fetch failed, let the user know.
                shortToast(this, R.string.err_offline_fetch);
                break;

            case FAIL_PARSE:
                // If parsing failed, let the user know.
                shortToast(this, R.string.err_offline_fetch);
                break;

            case SUCCESS:
            case NO_CHANGE:
            default:
                break;
        }
    }


    /**
     * Listen to changes in the model class, and update own views' information as needed.
     */
    public synchronized void updateNotify() {

        // Get all of the schools and add all of their open days to a hash table.
        ArrayList<School> schools = SchoolList.getInstance().getSchoolsAsArrayList();
        Hashtable<String, List<SchoolOpenDay>> openDays = new Hashtable<>();
        for (School s : schools) {
            openDays.put(s.getText(), s.getOpenDaysAsArrayList());
        }

        // Set adapter data and refresh the list view.
        this.expandableListAdapter.setData(schools, openDays);

        // This method can be called by any thread, but the UI can only be set using the UI thread.
        // The notify method of the adapter leads to setting of the data, and thus has to be run on
        // the UI thread.
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                expandableListAdapter.notifyDataSetChanged();
            }
        });
    }


    /**
     * {@link ExpandableListAdapterResourceProvider#childLayoutId()}
     */
    public int childLayoutId() {
        return R.layout.guud_expandable_list_child;
    }

    /**
     * {@link ExpandableListAdapterResourceProvider#groupLayoutId()}
     */
    public int groupLayoutId() {
        return R.layout.guud_expandable_list_group;
    }

    /**
     * {@link ExpandableListAdapterResourceProvider#childTextViewId()}
     */
    public int childTextViewId() {
        return R.id.guud_expandable_list_child_text;
    }

    /**
     * {@link ExpandableListAdapterResourceProvider#groupIndicatorImageViewId()}
     */
    public int groupIndicatorImageViewId() {
        return R.id.guud_expandable_list_group_indicator;
    }

    /**
     * {@link ExpandableListAdapterResourceProvider#groupTextViewId()}
     */
    public int groupTextViewId() {
        return R.id.guud_expandable_list_group_text;
    }

    /**
     * {@link ExpandableListAdapterResourceProvider#groupBgColour()}
     */
    public int groupBgColour() {
        return this.getResources().getColor(R.color.transparent);
    }

    /**
     * {@link ExpandableListAdapterResourceProvider#altBgColour()}
     */
    public int altBgColour() {
        return this.getResources().getColor(R.color.colorAltList);
    }

    /**
     * {@link ExpandableListAdapterResourceProvider#indicatorColour()}
     */
    public int indicatorColour() {
        return this.getResources().getColor(R.color.colorPrimary);
    }

    /**
     * {@link ExpandableListAdapterResourceProvider#expandedGroupIndicator()}
     */
    public Drawable expandedGroupIndicator() {
        return this.getResources().getDrawable(R.drawable.ic_expand_more_black_24dp);
    }

    /**
     * {@link ExpandableListAdapterResourceProvider#collapsedGroupIndicator()}
     */
    public Drawable collapsedGroupIndicator() {
        return this.getResources().getDrawable(R.drawable.ic_chevron_right_black_24dp);
    }

    /**
     * {@link ExpandableListAdapterResourceProvider#emptyGroupIndicator()}
     */
    public Drawable emptyGroupIndicator() {
        return new ColorDrawable(Color.TRANSPARENT);
    }
}

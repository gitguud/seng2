package uk.ac.uea.locationfinderapp.controller;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.Collections;

import uk.ac.uea.locationfinderapp.R;
import uk.ac.uea.gitguud.framework.model.CategoryList;
import uk.ac.uea.gitguud.framework.model.PointOfInterest;

import static uk.ac.uea.locationfinderapp.controller.POIBrowsingScreen.locationWanted;

/**
 * Created by Katrina on 13/11/2016.
 */

public class LocationDetailsScreen extends AppCompatActivity implements OnMapReadyCallback{
    private TextView locationTitle;
    private TextView locationDescription;
    private TextView gridCoords;
    private SupportMapFragment locationMap;
    private PointOfInterest poi;
    private Location location;
    private Switch mySwitch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.location_details_screen);

        String ourLocation = this.getIntent().getStringExtra("name");

        ArrayList<String> categoryList =
                Collections.list(CategoryList.getInstance().getCategories());

        String POIcategory = null;
        for(String categoryString: categoryList){
            for(PointOfInterest pointOfInterest: CategoryList.
                    getInstance().getPointsOfInterestAsArrayList(categoryString)){
                if(pointOfInterest.getName().equals(ourLocation)) {
                    this.poi = pointOfInterest;
                    POIcategory = categoryString;
                }
            }
        }
        mySwitch = (Switch) findViewById(R.id.locationSwitch);
        mySwitch.setChecked(locationWanted);
        mySwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {
                if(isChecked){
                    locationWanted = true;
                    askForLocationPermission();
                    locationMap.getMapAsync(returnThis());
                }else{
                    locationWanted = false;
                    recreate();
                }
            }
        });

        this.locationTitle = (TextView) findViewById(R.id.locationTitle);
        this.locationDescription = (TextView) findViewById(R.id.locationDescription);
        this.locationDescription.setMovementMethod(new ScrollingMovementMethod());//allows the description to scroll so map is always displayed over at least half of the screen
        this.gridCoords = (TextView) findViewById(R.id.gridCoords);
        this.locationMap =
                (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.location_map);
        this.locationTitle.setText(this.poi.getName());
        this.locationDescription.setText(this.poi.getDescription());
        if(this.poi.getCoordinateString()!=null && !this.poi.getCoordinateString().equals(""))
            this.gridCoords.setText("Grid coordinates - " + this.poi.getCoordinateString());
        this.location = this.poi.getLocation();

        Toolbar toolbar = (Toolbar) findViewById(R.id.detailsToolbar);
        toolbar.setTitle(POIcategory);
        setSupportActionBar(toolbar);

        this.locationMap.getMapAsync(this);

        if(!(ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)
           && locationWanted)
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        0);
    }

    public LocationDetailsScreen returnThis(){
        return this;
    }

    public void askForLocationPermission(){
        if(!(ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED))
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    0);
    }

    public void openMapScreen(View view){
        Context context = view.getContext();
        Intent showBigMapScreen = new Intent(context, LocationBigMapScreen.class);
        showBigMapScreen.putExtra("latitude",this.poi.getLocation().getLatitude());
        showBigMapScreen.putExtra("longitude",this.poi.getLocation().getLongitude());
        showBigMapScreen.putExtra("name",this.poi.getName());
        context.startActivity(showBigMapScreen);
    }


    @Override
    public void onMapReady(GoogleMap map) {

        // Set the location and move camera to it.

        LatLng latLng = new LatLng(this.location.getLatitude(), this.location.getLongitude());

        map.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
        // Add a marker to the map.
        map.addMarker(new MarkerOptions()
                .icon(BitmapDescriptorFactory.fromResource(
                        R.drawable.ic_place_black_24dp))
                .position(latLng));
        // Check if location permissions are enabled. Turn on the location services if the necessary
        // permissions are granted, and otherwise just finish the method execution.

        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
            && locationWanted) {
            map.setMyLocationEnabled(true);
        }
    }
}

package uk.ac.uea.locationfinderapp.controller;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import uk.ac.uea.locationfinderapp.R;

import static uk.ac.uea.locationfinderapp.controller.POIBrowsingScreen.locationWanted;

/**
 * Created by mircea on 03/01/17.
 */

public class LocationBigMapScreen extends AppCompatActivity implements OnMapReadyCallback{

    SupportMapFragment locationMap;
    double ourLatitude;
    double ourLongitude;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.big_map_screen);
        this.ourLatitude = this.getIntent().getDoubleExtra("latitude", 0);
        this.ourLongitude = this.getIntent().getDoubleExtra("longitude", 0);

        this.locationMap =(SupportMapFragment) getSupportFragmentManager().
                findFragmentById(R.id.location_map_big);

        Toolbar toolbar = (Toolbar) findViewById(R.id.bigMapToolbar);
        toolbar.setTitle(this.getIntent().getStringExtra("name"));
        setSupportActionBar(toolbar);
        this.locationMap.getMapAsync(this);
        if(!(ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)
           && locationWanted)
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},0);
    }

    @Override
    public void onMapReady(GoogleMap map) {

        LatLng latLng = new LatLng(ourLatitude,ourLongitude);

        map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 17));
        // Add a marker to the map.
        map.addMarker(new MarkerOptions()
                .icon(BitmapDescriptorFactory.fromResource(
                        R.drawable.ic_place_black_24dp))
                .position(latLng));
        // Check if location permissions are enabled. Turn on the location services if the necessary
        // permissions are granted, and otherwise just finish the method execution

        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
            && locationWanted) {
            map.setMyLocationEnabled(true);
        }
    }
}

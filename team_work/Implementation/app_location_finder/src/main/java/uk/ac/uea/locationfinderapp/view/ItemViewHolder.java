package uk.ac.uea.locationfinderapp.view;

/**
 * Created by mircea on 13/12/16.
 */

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import uk.ac.uea.locationfinderapp.R;
import uk.ac.uea.locationfinderapp.controller.LocationDetailsScreen;


public class ItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView name_TextView;


    public ItemViewHolder(View itemView) {
        super(itemView);
        name_TextView = (TextView) itemView.findViewById(R.id.item_name);
        itemView.setOnClickListener(this);

    }

    public void bind(String model) {
        name_TextView.setText(model);
    }

    @Override
    public void onClick(View view) {
        Context context = view.getContext();

        Intent showLocationDetailsScreen = new Intent(context, LocationDetailsScreen.class);
        showLocationDetailsScreen.putExtra("name",
                ((TextView)view.findViewById(R.id.item_name)).getText().toString());
        context.startActivity(showLocationDetailsScreen);
    }

}

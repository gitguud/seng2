package uk.ac.uea.locationfinderapp.view;

/**
 * Created by mircea on 06/12/16.
 */

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;


import java.util.ArrayList;

import uk.ac.uea.gitguud.framework.model.CategoryList;
import uk.ac.uea.gitguud.framework.model.PointOfInterest;


public class PagerAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;
    ArrayList<String> ourObjects = new ArrayList<>();

    public PagerAdapter(FragmentManager fm, int NumOfTabs, ArrayList<String> k) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
        ourObjects = k;
    }

    @Override
    public Fragment getItem(int position) {

        TabOneFragment tabFragment = new TabOneFragment();

        Bundle bundle = new Bundle();
        CategoryList.getInstance().getPointsOfInterestAsArrayList(ourObjects.get(position));
        ArrayList<String> POIstrings = new ArrayList<>();
        for(PointOfInterest POI: CategoryList.getInstance().
                getPointsOfInterestAsArrayList(ourObjects.get(position))){
            POIstrings.add(POI.getName());
        }
        bundle.putStringArrayList("pointsofinterest", POIstrings);
        bundle.putString("category", ourObjects.get(position));
        tabFragment.setArguments(bundle);
        return tabFragment;
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }

}

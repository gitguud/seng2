package uk.ac.uea.locationfinderapp.view;

/**
 * Created by mircea on 06/12/16.
 */

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Collections;

import uk.ac.uea.locationfinderapp.R;
import uk.ac.uea.locationfinderapp.controller.POIBrowsingScreen;
import uk.ac.uea.gitguud.framework.model.CategoryList;
import uk.ac.uea.gitguud.framework.model.PointOfInterest;


import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

public class TabOneFragment extends Fragment implements SearchView.OnQueryTextListener {

    private RecyclerView recyclerview;
    private ArrayList<String> mPointsOfInterest;
    private ArrayList<String> catObjects;
    private RVAdapter adapter;
    private boolean firstRun = true;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPointsOfInterest = new ArrayList<>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mPointsOfInterest = new ArrayList<>();
        View view = inflater.inflate(R.layout.tab_one_fragment, container, false);

        for (String category : Collections.list(CategoryList.getInstance().getCategories())) {
            for (PointOfInterest POI :
                    CategoryList.getInstance().getPointsOfInterestAsArrayList(category)) {
                mPointsOfInterest.add(POI.getName());
            }
        }
        recyclerview = (RecyclerView) view.findViewById(R.id.recyclerview);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerview.setLayoutManager(layoutManager);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mPointsOfInterest = new ArrayList<>();
        setHasOptionsMenu(true);
        Bundle k = this.getArguments();
        mPointsOfInterest = k.getStringArrayList("pointsofinterest");
        adapter = new RVAdapter(mPointsOfInterest);
        adapter.notifyDataSetChanged();
        recyclerview.setAdapter(adapter);
        firstRun=false;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser&&!firstRun) {
            mPointsOfInterest = new ArrayList<>();
            setHasOptionsMenu(true);
            Bundle k = this.getArguments();
            mPointsOfInterest = k.getStringArrayList("pointsofinterest");
            adapter = new RVAdapter(mPointsOfInterest);
            adapter.notifyDataSetChanged();
            recyclerview.setAdapter(adapter);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.options_menu, menu);

        final MenuItem item = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
        searchView.setOnQueryTextListener(this);

        MenuItemCompat.setOnActionExpandListener(item,
                new MenuItemCompat.OnActionExpandListener() {
                    @Override
                    public boolean onMenuItemActionCollapse(MenuItem item) {
                        adapter.setData(mPointsOfInterest);
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                adapter.notifyDataSetChanged();
                            }
                        });
                        return true; // Return true to collapse action view
                    }

                    @Override
                    public boolean onMenuItemActionExpand(MenuItem item) {
                        return true; // Return true to expand action view
                    }
                });
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        final ArrayList<String> filteredModelList = filter(mPointsOfInterest, newText);
        adapter.setData(filteredModelList);
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    private ArrayList<String> filter(ArrayList<String> models, String query) {

        query = query.toLowerCase();
        final ArrayList<String> filteredModelList = new ArrayList<>();
        if (query.equals(""))
            return filteredModelList;
        for (String model : models)
            if (model.toLowerCase().contains(query))
                filteredModelList.add(model);

        return filteredModelList;
    }
}

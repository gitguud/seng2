package uk.ac.uea.locationfinderapp.controller;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;

import uk.ac.uea.gitguud.framework.convenience.AndroidSpecific;
import uk.ac.uea.gitguud.framework.interfaces.StateObserver;
import uk.ac.uea.gitguud.framework.model.JSONToPOIParser;
import uk.ac.uea.locationfinderapp.R;
import uk.ac.uea.gitguud.framework.model.CategoryList;
import uk.ac.uea.gitguud.framework.model.XMLToPOIParser;
import uk.ac.uea.locationfinderapp.view.PagerAdapter;


public class POIBrowsingScreen extends AppCompatActivity implements StateObserver {

    private final int READ_REQUEST_CODE = 42;
    private PagerAdapter adapter;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    static boolean locationWanted;
    static boolean firstRun = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        loadInitialXML();

        locationWanted = false;

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        tabLayout = (TabLayout) findViewById(R.id.tab_layout);

        this.setViewData();

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                //ignored
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                //ignored
            }
        });
    }

    public void addSource(View view) {
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("*/*");
        startActivityForResult(intent, READ_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data == null)
            return;
        switch (requestCode) {
            case READ_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    Uri file = data.getData();
                    try {
                        int size = 0;
                        for (String cat : Collections.list(
                                CategoryList.getInstance().getCategories())) {
                            size += CategoryList.getInstance().
                                    getPointsOfInterestAsArrayList(cat).size();
                        }
                        System.out.println(size +
                                " NUMBER OF POIs BEFORE ADDING THE EXTERNAL XML @@@@@@@@@@@@@@");
                        String textRead = readTextFromUri(file);
                        String toastText = "Successfully loaded the file!";
                        if (textRead.charAt(0) == '<')
                            CategoryList.getInstance().updateEntries(new XMLToPOIParser()
                                    .parse(textRead));
                         else if (textRead.charAt(0) == '{')
                            CategoryList.getInstance().updateEntries(new JSONToPOIParser().
                                    parse(textRead));
                         else
                             toastText = "Failed to load the file!";


                        Toast toast = Toast.makeText(getApplicationContext(),
                                toastText, Toast.LENGTH_SHORT);
                        toast.show();
                        
                        viewPager.setCurrentItem(0);
                        this.updateNotify();

                        size = 0;
                        for (String cat : Collections.list(
                                CategoryList.getInstance().getCategories())) {
                            size += CategoryList.getInstance().
                                    getPointsOfInterestAsArrayList(cat).size();
                        }
                        System.out.println(size +
                                " NUMBER OF POIs AFTER ADDING EXTERNAL XML @@@@@@@@@@@@@@@@@@@");

                        recreate();
                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.e(AndroidSpecific.getLogTag(this),
                                "Failed to load the example XML file.");
                    }
                }
        }
    }

    public void loadInitialXML() {
        if (firstRun)
            try {
                String rawXml = AndroidSpecific.stringFromRawResource(this, R.raw.example_full);
                CategoryList.getInstance().loadCategories(new XMLToPOIParser().parse(rawXml), this);
                firstRun = false;
            } catch (Exception e) {
                e.printStackTrace();
                Log.e(AndroidSpecific.getLogTag(this), "Failed to load the example XML file.");
            }
    }

    private String readTextFromUri(Uri uri) throws IOException {
        InputStream inputStream = getContentResolver().openInputStream(uri);
        BufferedReader reader = new BufferedReader(new InputStreamReader(
                inputStream));
        StringBuilder stringBuilder = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
            stringBuilder.append(line);
        }
        inputStream.close();
        return stringBuilder.toString();
    }


    public POIBrowsingScreen setViewData() {
        ArrayList<String> categoryList = Collections.list(
                CategoryList.getInstance().getCategories());

        tabLayout.removeAllTabs();
        for (String categoryString : categoryList) {
            tabLayout.addTab(tabLayout.newTab().setText(categoryString));
        }

        tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        adapter = new PagerAdapter
                (getSupportFragmentManager(), tabLayout.getTabCount(),
                        Collections.list(CategoryList.getInstance().getCategories()));
        viewPager = (ViewPager) findViewById(R.id.pager);
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        return this;
    }


    @Override
    public void updateNotify() {
        this.setViewData();
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                adapter.notifyDataSetChanged();
            }
        });
    }
}

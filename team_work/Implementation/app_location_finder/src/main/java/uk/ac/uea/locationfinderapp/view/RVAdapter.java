package uk.ac.uea.locationfinderapp.view;


import uk.ac.uea.locationfinderapp.R;
import uk.ac.uea.locationfinderapp.controller.LocationDetailsScreen;

/**
 * Created by mircea on 11/12/16.
 */

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

public class RVAdapter extends RecyclerView.Adapter<ItemViewHolder>{

    private ArrayList<String> mDataSet;
    public RVAdapter(ArrayList<String> mItems) {
        this.mDataSet = mItems;
    }


    @Override
    public void onBindViewHolder(ItemViewHolder itemViewHolder, int i) {
        final String model = mDataSet.get(i);
        itemViewHolder.bind(model);
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_row, viewGroup, false);
        return new ItemViewHolder(view);
    }

    @Override
    public int getItemCount() {
        return mDataSet.size();
    }

    public void setData(ArrayList<String> models) {
        mDataSet = new ArrayList<>();
        mDataSet.addAll(models);
        notifyDataSetChanged();
    }
}
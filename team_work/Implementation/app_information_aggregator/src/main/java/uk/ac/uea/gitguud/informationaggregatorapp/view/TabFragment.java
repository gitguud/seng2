package uk.ac.uea.gitguud.informationaggregatorapp.view;

/**
 * Created by mircea on 06/12/16.
 */

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import uk.ac.uea.gitguud.framework.model.CategoryList;
import uk.ac.uea.gitguud.informationaggregatorapp.R;


public class TabFragment extends Fragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View k = inflater.inflate(R.layout.tab_fragment, container, false);
        RecyclerView rv = (RecyclerView) k.findViewById(R.id.my_recycler_view);

        Bundle j = this.getArguments();
        //TODO get the actual object here (probably will Parcel it)
        //or get its contents as an ArrayList, not sure
        String categoryName = j.getString("categoryName");
        CardAdapter ca = new CardAdapter(CategoryList.getInstance().getPointsOfInterestAsArrayList(categoryName));
        rv.setAdapter(ca);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        rv.setLayoutManager(llm);

        return k;
    }


}

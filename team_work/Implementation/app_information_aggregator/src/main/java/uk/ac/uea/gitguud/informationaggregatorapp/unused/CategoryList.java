package uk.ac.uea.gitguud.informationaggregatorapp.unused;

import android.support.annotation.Nullable;
import android.support.v4.util.Pair;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;

import uk.ac.uea.gitguud.framework.SynchronisedObject;
import uk.ac.uea.gitguud.framework.convenience.ErrorChecking;

/**
 * CategoryList.java
 *
 * Models a list of aggregated entry categories.
 * Created by Katrina on 13/11/2016.
 */
public class CategoryList extends SynchronisedObject<Pair<String, ArrayList<AggregatedEntry>>> {
    private static CategoryList instance = null;

    private Hashtable<String, Hashtable<AggregatedEntry, AggregatedEntry>> categories;


    /**
     * Accessor for a single instance of the category list.
     *
     * @return Category list with all of the categories of aggregated information.
     */
    public static CategoryList getInstance() {
        if (CategoryList.instance == null) {
            CategoryList.instance = new CategoryList();
        }

        return CategoryList.instance;
    }

    /**
     * Private default constructor for the singleton pattern.
     */
    private CategoryList() {
        this.categories = new Hashtable<>();
    }


    /**
     * Accessor for all of the categories <b>except for custom</b>.
     *
     * @return Enumerable object of categories, except for custom, in a random order.
     */
    public Enumeration<String> getCategories() {
        return this.categories.keys();
    }

    /**
     * Adds or updates a category.
     *
     * @param category Category to add. If does not exist, simply adds it. If exists, merges its
     *                 the existing entries with new ones, updating their insides. Does not remove
     *                 entries that were not present in the update.
     *                 Note that the names of categories are case-insensitive.
     * @param entries Entries of the category. May be null or empty: in this case nothing will be
     *                added into the category.
     * @return This object for chaining.
     * @throws NullPointerException If either of the provided category name was null.
     * @throws IllegalArgumentException If the provided category name was an empty string.
     */
    public CategoryList addOrUpdateCategory(String category, ArrayList<AggregatedEntry> entries) {
        ErrorChecking.getInstance()
                .throwOnNull(category, "The category cannot be null.")
                .throwOnEmptyString(category, "the category cannot be empty.");

        // It is meaningless to do anything else if the entries list is empty.
        if (entries == null || entries.isEmpty()) {
            return this;
        }

        // Ignore case.
        category = category.toUpperCase();

        // Get the current category entries if present.
        Hashtable<AggregatedEntry, AggregatedEntry> entryHashtable = this.categories.get(category);

        // If nothing was found, simply add the new category into the category list.
        if (entryHashtable == null) {
            entryHashtable = new Hashtable<>();

            // Add all of the entries into a new hash table.
            for (AggregatedEntry entry : entries) {
                entryHashtable.put(entry, entry);
            }

            // Add the new hash table of entries into categories.
            this.categories.put(category, entryHashtable);
            return this;
        }


        // If the category is present, try adding or updating each of the entries.
        for (AggregatedEntry entry : entries) {
            this.addOrUpdateEntry(category, entry);
        } // end for each

        return this;
    }

    /**
     * Adds or updates an entry into the specified category.
     *
     * @param category Category to add the entry into. Case-insensitive.
     * @param entry Entry to add.
     * @throws NullPointerException if either of the provided objects are null pointers.
     * @return True if the category was found and the object added or updated, and false if not.
     */
    public boolean addOrUpdateEntry(String category, AggregatedEntry entry) {

        ErrorChecking.getInstance()
                .throwOnNull(category, "The category name cannot be null.")
                .throwOnNull(entry, "The entry to add or update cannot be null.");

        // Find the category ignoring the case.
        category = category.toUpperCase();
        Hashtable<AggregatedEntry, AggregatedEntry> entries = this.categories.get(category);

        // If not found, return false.
        if (entries == null) {
            return false;
        }

        // Try getting the existing entry.
        AggregatedEntry existingEntry = entries.get(entry);
        if (existingEntry != null) {
            // If exists, copy the new values.
            existingEntry.copyValues(entry);
        } else {
            // Otherwise simply add the entry.
            entries.put(entry, entry);
        }

        return true;
    }

    /**
     * Removes an entry from the category.
     *
     * @param category Category to remove the entry from.
     * @param entry Entry to remove.
     * @return True if the entry was removed, and false if not (due to the category or the entry not
     *         existing within the category).
     */
    public boolean removeEntry(String category, AggregatedEntry entry) {

        ErrorChecking.getInstance()
                .throwOnNull(category, "The category to remove from cannot be null.")
                .throwOnNull(entry, "Entry to remove cannot be null");

        // Find the category ignoring the case.
        category = category.toUpperCase();
        Hashtable<AggregatedEntry, AggregatedEntry> entries = this.categories.get(category);

        // If the category was not found, return false.
        if (entries == null) {
            return false;
        }

        // If failed to remove an object, return false. Otherwise true.
        return entries.remove(entry) != null;
    }

    /**
     * Removes a category from the list.
     *
     * @param category Name of the category to remove (case-insensitive).
     * @return True if the category was removed, and false if it was not.
     */
    public boolean removeCategory(String category) {
        return this.categories.remove(category.toUpperCase()) != null;
    }

    /**
     * Accessor for entries within a category.
     *
     * @param category Category to retrieve entries from; case-insensitive.
     * @return Entries within the category, if the category was found, and null if not.
     */
    @Nullable
    public ArrayList<AggregatedEntry> getEntriesOf(String category) {

        // Try getting the entries in internal representation.
        Hashtable<AggregatedEntry, AggregatedEntry> entriesInner =
                this.categories.get(category.toUpperCase());

        // If not found, return null.
        if (entriesInner == null) {
            return null;
        }

        // Otherwise create a new array list with values from within the internal representation.
        return new ArrayList<>(entriesInner.values());
    }


    /**
     * 
     * @param newCategories
     */
    @Override
    public void updateEntries(ArrayList<Pair<String, ArrayList<AggregatedEntry>>> newCategories) {
        String categoryName;
        ArrayList<AggregatedEntry> newEntries;
        Hashtable<AggregatedEntry, AggregatedEntry> retainedEntries = new Hashtable<>();

        Hashtable<AggregatedEntry, AggregatedEntry> existingEntries;
        AggregatedEntry existingEntry;

        // Go through each of the new categories.
        for (Pair<String, ArrayList<AggregatedEntry>> category : newCategories) {

            categoryName = category.first.toUpperCase();
            newEntries = category.second;

            // Check if the category exists.
            existingEntries = this.categories.get(categoryName);
            if (existingEntries != null) {
                // If it does, merge old entries and add the new ones.
                for (AggregatedEntry newEntry : newEntries) {

                    // Try getting the existing entry.
                    existingEntry = existingEntries.get(newEntry);
                    if (existingEntry != null) {
                        // If exists, update.
                        existingEntry.copyValues(newEntry);
                        retainedEntries.put(existingEntry, existingEntry);
                    } else {
                        // If does not exist, add as new.
                        retainedEntries.put(newEntry, newEntry);
                    }
                } // end for each

                // Clear out existing entries and replace them with new ones.
                existingEntries.clear();
                existingEntries.putAll(retainedEntries);

            } else {
                // If the category does not exist, add all of the entries into the hash table and
                // add it as a new category.
                for (AggregatedEntry newEntry : newEntries) {
                    retainedEntries.put(newEntry, newEntry);
                }

                this.categories.put(categoryName, retainedEntries);
            } // end if
        } // end for each
    }

}

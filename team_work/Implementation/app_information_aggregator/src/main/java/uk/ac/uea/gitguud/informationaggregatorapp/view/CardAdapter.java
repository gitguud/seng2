package uk.ac.uea.gitguud.informationaggregatorapp.view;

/**
 * Created by mircea on 09/12/16.
 */

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import uk.ac.uea.gitguud.framework.model.PointOfInterest;
import uk.ac.uea.gitguud.informationaggregatorapp.R;

public class CardAdapter extends RecyclerView.Adapter<CardAdapter.MyViewHolder> {

    private ArrayList<PointOfInterest> mDataset;

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        public CardView mCardView;
        public TextView placeNameTextView;
        public TextView placeDescriptionTextView;

        public MyViewHolder(View v) {
            super(v);
            mCardView = (CardView) v.findViewById(R.id.card_view);
            placeNameTextView = (TextView) v.findViewById(R.id.placeName);
            placeDescriptionTextView = (TextView) v.findViewById(R.id.placeDescription);

        }
    }

    public CardAdapter(ArrayList<PointOfInterest> myDataset) {
        mDataset = myDataset;
    }

    @Override
    public CardAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent,
                                                     int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_layout, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }


    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.placeNameTextView.setText(mDataset.get(position).getName());
        if(mDataset.get(position).getDescription()!=null&&!mDataset.get(position).getDescription().isEmpty())
            holder.placeDescriptionTextView.setText(mDataset.get(position).getDescription());
        else
            holder.placeDescriptionTextView.setHeight(1);
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}
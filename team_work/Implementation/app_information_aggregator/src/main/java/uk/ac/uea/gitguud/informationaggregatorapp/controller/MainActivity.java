package uk.ac.uea.gitguud.informationaggregatorapp.controller;

import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collections;


import uk.ac.uea.gitguud.framework.convenience.AndroidSpecific;
import uk.ac.uea.gitguud.framework.interfaces.StateObserver;
import uk.ac.uea.gitguud.framework.model.CategoryList;
import uk.ac.uea.gitguud.framework.model.JSONToPOIParser;
import uk.ac.uea.gitguud.framework.model.XMLToPOIParser;
import uk.ac.uea.gitguud.informationaggregatorapp.R;
import uk.ac.uea.gitguud.informationaggregatorapp.view.PagerAdapter;


public class MainActivity extends AppCompatActivity implements StateObserver {

    private FloatingActionButton addSourceButton;
    private class readXMLFromInternet extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String[] params) {
            URL url = null;
            try {
                url = new URL(params[0]);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            if (url != null && !url.equals("")) {
                StringBuilder strBld = new StringBuilder();
                try {
                    BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
                    String str;
                    while ((str = in.readLine()) != null) {
                        strBld.append(str);
                    }
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                System.out.println(strBld.toString());
                return strBld.toString();
            } else {
                return null;
            }
        }

        @Override
        protected void onPostExecute(String message) {
            String text;
            System.out.println(message);
            if (message != null && !message.equals("")) {
                if(message.charAt(0)=='<')
                    CategoryList.getInstance().updateEntries(new XMLToPOIParser().parse(message));
                else
                    CategoryList.getInstance().updateEntries(new JSONToPOIParser().parse(message));
                updateNotify();
                text = "Successfully loaded the file!";
            } else
                text = "Failed to load the file!";

            Toast toast = Toast.makeText(getApplicationContext(),
                    text, Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    private Context c = this;
    private ViewPager viewPager;
    private PagerAdapter adapter;
    private TabLayout tabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        try {
//            String rawJson = AndroidSpecific.stringFromRawResource(this, R.raw.testjson);
//            CategoryList.getInstance().loadCategories(new JSONToPOIParser().parse(rawJson), this);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
        try {
            String rawXml = AndroidSpecific.stringFromRawResource(this, R.raw.example_ig_values);
            CategoryList.getInstance().loadCategories(new XMLToPOIParser().parse(rawXml), this);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(AndroidSpecific.getLogTag(this), "Failed to load the example XML file.");
        }
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        this.addSourceButton = (FloatingActionButton) findViewById(R.id.add_source_button);
        this.addSourceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LayoutInflater layoutInflaterAndroid = LayoutInflater.from(c);
                View mView = layoutInflaterAndroid.inflate(R.layout.user_input_dialog_box, null);
                AlertDialog.Builder alertDialogBuilderUserInput = new AlertDialog.Builder(c);
                alertDialogBuilderUserInput.setView(mView);

                final EditText userInputSourceAddress = (EditText) mView.
                        findViewById(R.id.sourceAddress);
                alertDialogBuilderUserInput
                        .setCancelable(false)
                        .setPositiveButton("Add", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogBox, int id) {

                                int size = 0;
                                for (String cat : Collections.list(
                                        CategoryList.getInstance().getCategories())) {
                                    size += CategoryList.getInstance().
                                            getPointsOfInterestAsArrayList(cat).size();
                                }
                                System.out.println(size +
                                        " NUMBER OF POIs BEFORE ADDING THE EXTERNAL XML @@@@@@@@@@@@@@");

                                new readXMLFromInternet().execute(
                                        userInputSourceAddress.getText().toString());
                                viewPager.setCurrentItem(0);

                                size = 0;
                                for (String cat : Collections.list(
                                        CategoryList.getInstance().getCategories())) {
                                    size += CategoryList.getInstance().
                                            getPointsOfInterestAsArrayList(cat).size();
                                }
                                System.out.println(size +
                                        " NUMBER OF POIs AFTER ADDING EXTERNAL XML @@@@@@@@@@@@@@@@@@@");
                            }
                        })
                        .setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialogBox, int id) {
                                        dialogBox.cancel();
                                    }
                                });

                AlertDialog alertDialogAndroid = alertDialogBuilderUserInput.create();
                alertDialogAndroid.show();
            }
        });

        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        this.setViewData();

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                //ignored
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                //ignored
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return true;
    }

    public MainActivity setViewData() {
        tabLayout.removeAllTabs();
        for (String category : Collections.list(CategoryList.getInstance().getCategories()))
            tabLayout.addTab(tabLayout.newTab().setText(category));
        viewPager = (ViewPager) findViewById(R.id.pager);
        adapter = new PagerAdapter
                (getSupportFragmentManager(), tabLayout.getTabCount(),
                        Collections.list(CategoryList.getInstance().getCategories()));
        tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        return this;
    }

    @Override
    public void updateNotify() {
        this.setViewData();
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                adapter.notifyDataSetChanged();
            }
        });
    }
}

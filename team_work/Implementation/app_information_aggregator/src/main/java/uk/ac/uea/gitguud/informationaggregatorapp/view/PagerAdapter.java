package uk.ac.uea.gitguud.informationaggregatorapp.view;

/**
 * Created by mircea on 06/12/16.
 */

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;


public class PagerAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;
    ArrayList<String> ourCategories = new ArrayList<>();

    public PagerAdapter(FragmentManager fm, int NumOfTabs, ArrayList<String> k) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
        ourCategories = k;
    }

    @Override
    public Fragment getItem(int position) {

        //TODO pass the Category object here
        //or pass its contents as an ArrayList, not sure
        TabFragment tabFragment = new TabFragment();

        Bundle bundle = new Bundle();
        bundle.putString("categoryName", ourCategories.get(position));

        tabFragment.setArguments(bundle);
        return tabFragment;
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}

package uk.ac.uea.gitguud.informationaggregatorapp.unused;

import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.util.Pair;

import uk.ac.uea.gitguud.framework.convenience.ErrorChecking;

/**
 * AggregatedEntry.java
 *
 * Models a single aggregated entry with a short message and an optional image.
 * Created by Pavel Solodilov on 27/12/16.
 */
public class AggregatedEntry {
    private Pair<String, String> source;
    private String title;
    private String message;
    private Drawable image;


    /**
     * Constructor for an entry with only a message and a title, but no source or image.
     *
     * @param title Title of the entry. See also {@link AggregatedEntry#setTitle(String)}.
     * @param message Message of the entry. See also {@link AggregatedEntry#setMessage(String)}.
     */
    public AggregatedEntry(String title, String message) {
        // Define in terms of the full constructor.
        this(title, message, null, null);
    }

    /**
     * Constructor for an entry with a title, message and an image.
     *
     * @param title Title of the entry. See also {@link AggregatedEntry#setTitle(String)}.
     * @param message Message of the entry.See also {@link AggregatedEntry#setMessage(String)}.
     * @param image Image of the entry. See also {@link AggregatedEntry#setImage(Drawable)}.
     */
    public AggregatedEntry(String title, String message, Drawable image) {
        // Define in terms of the full constructor.
        this(title, message, null, image);
    }

    /**
     * Constructor for an imageless entry.
     *
     * @param title Title of the entry. See also {@link AggregatedEntry#setTitle(String)}.
     * @param message Message of the entry. See also {@link AggregatedEntry#setMessage(String)}.
     * @param source Source of the entry. See also {@link AggregatedEntry#setSource(Pair)}.
     */
    public AggregatedEntry(String title, String message, Pair<String, String> source) {
        // Define in terms of the full constructor.
        this(title, message, source, null);
    }

    /**
     * Full constructor for an entry.
     *
     * @param title Title of the entry. See also {@link AggregatedEntry#setTitle(String)}.
     * @param message Message of the entry. See also {@link AggregatedEntry#setMessage(String)}.
     * @param source Source of the entry. See also {@link AggregatedEntry#setSource(Pair)}.
     * @param image Image of the entry. See also {@link AggregatedEntry#setImage(Drawable)}.
     */
    public AggregatedEntry(String title, String message, Pair<String, String> source,
                           Drawable image) {
        this.setTitle(title).setMessage(message).setSource(source).setImage(image);
    }


    /**
     * Accessor for the title of the entry.
     *
     * @return Title of the entry.
     */
    @NonNull
    public String getTitle() {
        return this.title;
    }

    /**
     * Accessor for the message of the entry.
     *
     * @return Message of the entry.
     */
    @NonNull
    public String getMessage() {
        return this.message;
    }

    /**
     * Accessor for the source of the entry.
     *
     * @return Source of the entry. Might be null.
     */
    @Nullable
    public Pair<String, String> getSource() {
        return this.source;
    }

    /**
     * Accessor for the image of the entry.
     *
     * @return Image of the entry if present.
     */
    @Nullable
    public Drawable getImage() {
        return this.image;
    }


    /**
     * Setter for the title of the entry.
     *
     * @param title New title.
     * @return This object for chaining.
     * @throws NullPointerException if the title provided was null.
     * @throws IllegalArgumentException if the title provided was an empty string.
     */
    public AggregatedEntry setTitle(String title) {
        ErrorChecking.getInstance()
                .throwOnNull(title, "The title of an aggregated entry cannot be null")
                .throwOnEmptyString(title, "The title of an aggregated entry cannot be empty.");

        this.title = title;
        return this;
    }

    /**
     * Setter for the source of the entry.
     *
     * @param source Source of the entry. The first element of the pair is the name of the source,
     *               and the second one is the URL or some other type of source string
     *               (e.g. storage path). Might be null for no path.
     * @return This object for chaining.
     */
    public AggregatedEntry setSource(Pair<String, String> source) {
        this.source = source;
        return this;
    }

    /**
     * Setter for the message of the entry.
     *
     * @param message Short message to be displayed.
     * @return This object for chaining.
     * @throws NullPointerException if the provided message is null.
     * @throws IllegalArgumentException if the provided message is an empty string.
     */
    public AggregatedEntry setMessage(String message) {
        ErrorChecking.getInstance()
                .throwOnNull(message, "The aggregated entry's message cannot be null")
                .throwOnEmptyString(message, "The aggregated entry's message cannot be empty.");

        this.message = message;
        return this;
    }

    /**
     * Setter for the image of the entry.
     *
     * @param image Image to associate with the entry. Leave as null for no image.
     * @return This object for chaining.
     */
    public AggregatedEntry setImage(Drawable image) {
        this.image = image;
        return this;
    }

    /**
     * Copies the non-hashed values (image, source name, not the URL, and the message) from the
     * provided entry into this one.
     *
     * @param other Other entry to copy the value from. If null, does nothing.
     * @return This object for chaining.
     */
    public AggregatedEntry copyValues(AggregatedEntry other) {
        if (other == null) {
            return this;
        }

        // If both of the sources are present, set the name (first) from the other source, but keep
        // the URL as-is.
        if (this.getSource() != null && other.getSource() != null) {
            this.setSource(new Pair<>(other.getSource().first, this.getSource().second));
        }

        // Set the image and the message, and use the chaining to return this object.
        return this.setImage(other.getImage()).setMessage(other.getMessage());
    }

    /**
     * If the source is empty, the hash code is simply calculated as hash of the title of the entry.
     * If the source is present, the hash code is a hash of concatenation of the source title and
     * the source's URL (second element of the pair).
     *
     * @return Hash of this entry.
     */
    @Override
    public int hashCode() {
        if (this.source == null) {
            return this.title.hashCode();
        } else {
            return (this.title + this.source.second).hashCode();
        }
    }
}

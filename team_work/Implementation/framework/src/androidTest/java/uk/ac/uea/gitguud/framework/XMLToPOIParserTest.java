package uk.ac.uea.gitguud.framework;

import android.location.Location;
import android.util.Pair;

import org.junit.Test;

import java.util.ArrayList;

import uk.ac.uea.gitguud.framework.model.PointOfInterest;
import uk.ac.uea.gitguud.framework.model.XMLToPOIParser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;


/**
 * Created by Anastassia Segeda on 1/10/2017.
 */

public class XMLToPOIParserTest {

    private Location createLocation(String provider, double latitude, double longitude) {
        Location location = new Location(provider);
        location.setLatitude(latitude);
        location.setLongitude(longitude);
        return location;
    }

    @Test
    public void parse(){

        XMLToPOIParser parser = new XMLToPOIParser();

        ArrayList<Pair<String, ArrayList<PointOfInterest>>> cats = parser.parse(xml1);
        assertNotNull("The parsed XML was correct, so the result should not be null.", cats);
        if (cats.size() == 0) fail ("There should be a number of categories.");

        ArrayList<Pair<String, ArrayList<PointOfInterest>>> expCats = new ArrayList<>();

        Location loc = createLocation("UNKNOWN", 52.621483, 1.24073);

        ArrayList<PointOfInterest>points1 = new ArrayList<>();
        PointOfInterest poi1 = new PointOfInterest("Campus Kitchen- Zest", loc, "UEA Campus",
                "A friendly open place to eat in the centre of campus.");
        points1.add(poi1);
        Pair<String,ArrayList<PointOfInterest>> cat1 = Pair.create("restaurants", points1);

        ArrayList<PointOfInterest>points2 = new ArrayList<>();
        PointOfInterest poi2 = new PointOfInterest("Waterstones", loc, "UEA Campus",
                "A book shop.");
        points2.add(poi2);
        Pair<String,ArrayList<PointOfInterest>> cat2 = Pair.create("shops", points2);

        expCats.add(cat1); expCats.add(cat2);
        assertEquals(expCats, cats);
    }

    String xml1 = "<?xml version = \"1.0\" encoding= \"UTF-8\" standalone = \"no\"?>"+
            "<data>"+
            "<category name = \"restaurants\">"+
            "<poi name = \"Campus Kitchen- Zest\">"+
            "<location name = \"UEA Campus\">"+
            "<latitude>52.621483</latitude>"+
            "<longitude>1.24073</longitude>"+
            "</location>"+
            "<description>"+
            "A friendly open place to eat in the centre of campus."+
            "</description>"+
            "</poi>"+
            "</category>"+

            "<category name = \"shops\">"+
            "<poi name = \"Waterstones\">"+
            "<location name = \"UEA Campus\">"+
            "<latitude>52.621483</latitude>"+
            "<longitude>1.24073</longitude>"+
            "</location>"+
            "<description>"+
            "A book shop."+
            "</description>"+
            "</poi>"+
            "</category>"+
            "</data>";
}

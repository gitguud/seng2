package uk.ac.uea.gitguud.framework;

import org.junit.Test;

import android.graphics.drawable.Drawable;
import android.location.*;
import android.util.Pair;

import static org.junit.Assert.*;

import uk.ac.uea.gitguud.framework.model.PointOfInterest;

/**
 * Created by Katrina on 13/11/2016.
 */
public class PointOfInterestTest {
    private Location createLocation(String provider, double altitude, double longitude,
                                    double latitude) {
        Location location = new Location(provider);
        location.setAltitude(altitude);
        location.setLatitude(latitude);
        location.setLongitude(longitude);
        return location;
    }

    private boolean equalLocations(Location loc1, Location loc2) {
        if (loc1.getAltitude() != loc2.getAltitude()) {
            return false;
        } else if(loc1.getLatitude() != loc2.getLatitude()){
            return false;
        } else if (loc1.getLongitude() != loc2.getLongitude()){
            return false;
        }
        return true;
    }

    private Pair<String, String> source = Pair.create("sourceName","sourceURL");
    private Drawable image = null;
    private String name = "Lecture Theatre";
    private String locationString = "LT2";
    private Location location = createLocation(locationString, 10, 10, 10) ;
    private String description = "Description of the poi";

    public PointOfInterest buildInstance(){
        return new PointOfInterest(name, location, locationString, description, source, image);
    }


    @Test
    public void getSource(){
        PointOfInterest instance = buildInstance();
        assertEquals(source, instance.getSource());
    }

    @Test
    public void getImage(){
        PointOfInterest instance = buildInstance();
        assertEquals(image, instance.getImage());
    }

    @Test
    public void getName(){
        PointOfInterest instance = buildInstance();
        assertEquals(name, instance.getName());
    }

    @Test
    public void getLocation() {
        PointOfInterest instance = buildInstance();
        assertTrue(equalLocations(location, instance.getLocation()));
    }

    @Test
    public void getLocationString() {
        PointOfInterest instance =  buildInstance();
        assertEquals(locationString, instance.getLocationString());
    }

    @Test
    public void getDescription(){
        PointOfInterest instance =  buildInstance();
        assertEquals(description, instance.getDescription());
    }

    @Test
    public void setSource(){
        PointOfInterest instance = buildInstance();
        Pair<String, String> newSource = Pair.create("newSourceName","newSourceURL");
        instance.setSource(newSource);
        assertEquals(newSource, instance.getSource());
    }

    @Test
    public void setImage(){
        PointOfInterest instance = buildInstance();
        Drawable newImage = null;
        instance.setImage(newImage);
        assertEquals(newImage, instance.getImage());
    }


    @Test
    public void setName() throws Exception {
        PointOfInterest instance = buildInstance();
        String name2 = "Shops";
        instance.setName(name2);
        assertEquals(name2, instance.getName());
    }

    @Test(expected = NullPointerException.class)
    public void setNameToNull (){
        PointOfInterest instance = buildInstance();
        instance.setName(null);
    }
    @Test(expected = IllegalArgumentException.class)
    public void setNameToEmptyString(){
        PointOfInterest instance = buildInstance();
        instance.setName("");
    }

    @Test
    public void setLocation(){
        PointOfInterest instance = buildInstance();
        Location loc3 = createLocation("loc3", 12, 12, 12);
        instance.setLocation(loc3,"loc2");
        assertTrue(equalLocations(loc3, instance.getLocation()));
    }

    @Test(expected = NullPointerException.class)
    public void LocationIsNull(){
        PointOfInterest instance = buildInstance();
        instance.setLocation(null, locationString);
    }

    @Test
    public void setDescription(){
        PointOfInterest instance = buildInstance();
        String newDescription = "New description";
        instance.setDescription(newDescription);
        assertEquals(newDescription, instance.getDescription());
    }


    @Test
    public void updateWith(){
        PointOfInterest instance = buildInstance();

        PointOfInterest newPOI = buildInstance();
        Location newPoiLoc = createLocation("Location", 20, 20, 20);
        Pair<String, String> newSource = Pair.create("newSourceName","sourceURL should not change");
        newPOI.setDescription("Description")
                .setName("Should not change")
                .setLocation(newPoiLoc,"LocationString")
                .setSource(newSource);

        instance.updateWith(newPOI);

        //update newPOI name and source URL
        Pair<String, String> newSource2 = Pair.create(newSource.first,source.second);
        newPOI.setSource(newSource2);
        newPOI.setName(name);

        assertEquals(newPOI, instance);
    }

}
package uk.ac.uea.gitguud.framework;

import android.content.Context;
import android.support.test.InstrumentationRegistry;

import junit.framework.Assert;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Pavel Solodilov on 11/11/16.
 */
public class AndroidFileIOTest {
    private static final String FILE = "some_file";
    private static final String STORED_TEXT = "<p>This is the test string line 1.</p>\n" +
            "<p>This is line 2</p>";
    private static final Context CONTEXT = InstrumentationRegistry.getTargetContext();


    /**
     * Test reads the file.
     * Assumes that the read file is empty.
     */
    @Test
    public void testReadFile() {
        System.out.println("testReadFile()");

        //AndroidFileIO.getInstance().useExternal();

        if(AndroidFileIO.getInstance().readFile(null, CONTEXT)!=null)
            fail("Proceeded reading the file even though the filename was null.");
        if(AndroidFileIO.getInstance().readFile("", CONTEXT)!=null)
            fail("Proceeded reading the file even though the filename was empty.");
        if(AndroidFileIO.getInstance().readFile(FILE, null)!=null)
            fail("Proceeded reading the file even though the context was null.");

        //There should be nothing at first, so this should return a null.
        String loadedString = AndroidFileIO.getInstance().readFile(FILE, CONTEXT);
        Assert.assertNull("Retrieved data '" + loadedString + "' from " + FILE +
                " while it should have been null.", loadedString);

        //writes to a file and then reads it back to assert if it is equal or not
        AndroidFileIO.getInstance().writeFile(FILE, STORED_TEXT, CONTEXT);
        Assert.assertEquals(STORED_TEXT,AndroidFileIO.getInstance().readFile(FILE, CONTEXT));
        CONTEXT.deleteFile(FILE);

        //this test below is obsolete because writeFile
        //handles writing an empty string (does not allow it)
        //left it here for referencing purposes
        AndroidFileIO.getInstance().writeFile(FILE, "", CONTEXT);
        if(AndroidFileIO.getInstance().readFile(FILE,CONTEXT)!=null)
            fail("Proceeded returning an empty string when we should've returned null.");
        CONTEXT.deleteFile(FILE);
    }


    /**
     * Test writes the file.
     */
    @Test
    public void testWriteFile() {
        System.out.println("testWriteFile()");

        //AndroidFileIO.getInstance().useExternal();
        if(AndroidFileIO.getInstance().writeFile(null, STORED_TEXT, CONTEXT)){
            CONTEXT.deleteFile(FILE);
            fail("Writing proceeded even though filename was null.");
        }
        if(AndroidFileIO.getInstance().writeFile(FILE, null, CONTEXT)){
            CONTEXT.deleteFile(FILE);
            fail("Writing proceeded even though content was null.");
        }
        if(AndroidFileIO.getInstance().writeFile("", STORED_TEXT, CONTEXT)){
            CONTEXT.deleteFile(FILE);
            fail("Writing proceeded even though filename was empty.");
        }
        if(AndroidFileIO.getInstance().writeFile(FILE, "", CONTEXT)){
            CONTEXT.deleteFile(FILE);
            fail("Writing proceeded even though contents were empty.");
        }

        if(!AndroidFileIO.getInstance().writeFile(FILE, STORED_TEXT, CONTEXT))
            fail("Writing to file failed, most probably due to permissions.\n" +
                    "Comment line 174 in AndroidFileIOTest and run debug " +
                    "to see the path that's tried to be written to.");


        String loadedString = AndroidFileIO.getInstance().readFile(FILE, CONTEXT);
        String nullError = "Retrieved data from " + FILE + " was null, while should have been '" +
                STORED_TEXT + "'.";
        Assert.assertNotNull(nullError, loadedString);


        Assert.assertEquals("GOT '" + loadedString + "' WHEN IT SHOULD HAVE BEEN '" +
                STORED_TEXT + "'.", loadedString, STORED_TEXT);

        CONTEXT.deleteFile(FILE);
    }

}


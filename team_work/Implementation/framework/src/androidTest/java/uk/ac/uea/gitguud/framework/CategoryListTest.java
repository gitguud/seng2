
package uk.ac.uea.gitguud.framework;




import android.content.Context;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.support.test.InstrumentationRegistry;
import android.util.Pair;

import org.junit.Before;
import org.junit.Test;

import java.io.InputStream;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;


import uk.ac.uea.gitguud.framework.model.CategoryList;
import uk.ac.uea.gitguud.framework.model.PointOfInterest;
import uk.ac.uea.gitguud.framework.model.XMLToPOIParser;

import static org.junit.Assert.*;


/**Created by Katrina on 13/11/2016.**/


public class CategoryListTest {

    //Reset contents of singleton object
    //Source: http://blog.davidehringer.com/testing/test-driven-development/unit-testing-singletons/
    @Before
    public void resetSingleton() throws SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
        Field instance = CategoryList.class.getDeclaredField("instance");
        instance.setAccessible(true);
        instance.set(null, null);
    }

    private static final Context CONTEXT = InstrumentationRegistry.getTargetContext();

    private String readXml(int rawXmlResourceId) {

        InputStream stream = CONTEXT.getResources().openRawResource(rawXmlResourceId);

        try {
            byte[] buffer = new byte[stream.available()];
            stream.read(buffer);
            stream.close();
            return new String(buffer);
        } catch (Exception e) {
            throw new NullPointerException("The XML reading is ought to happen successfully.");
        }
    }

    private Location createLocation(String provider,double latitude, double longitude) {
        Location location = new Location(provider);
        location.setLatitude(latitude);
        location.setLongitude(longitude);
        return location;
    }

    private Pair<String, String> source = Pair.create("sourceName","sourceURL");
    private Drawable image = null;
    private String name = "Lecture Theatre";
    private String locationString = "LT2";
    private Location location = createLocation(locationString, 10, 10) ;
    private String description = "Description of the poi";

    public PointOfInterest buildPOIInstance(){
        return new PointOfInterest(name, location, locationString, description, source, image);
    }

    private PointOfInterest poi1 = buildPOIInstance();
    private ArrayList<PointOfInterest> points = new ArrayList(){{add(poi1);}};

    @Test
    public void addOrUpdateCategoryParser() throws NoSuchFieldException, IllegalAccessException {
        CategoryList instance = CategoryList.getInstance();
        // The XML is in the res/raw/example_full.xml file.
        String xml1 = readXml(R.raw.testxml);
        String xml2 = readXml(R.raw.testxmlupdate);
        XMLToPOIParser parser = new XMLToPOIParser();

        ArrayList<Pair<String, ArrayList<PointOfInterest>>> cats1 = parser.parse(xml1);
        assertNotNull("The parsed XML was correct, so the result should not be null.", cats1);
        if (cats1.size() == 0) fail ("There should be a number of categories.");
        instance.updateEntries(cats1);


        ArrayList<Pair<String, ArrayList<PointOfInterest>>> cats2 = parser.parse(xml2);
        assertNotNull("The parsed XML was correct, so the result should not be null.", cats2);
        if (cats2.size() == 0) fail ("There should be a number of categories.");
        instance.updateEntries(cats2);

        //create arrayList of expected data
        ArrayList<Pair<String, ArrayList<PointOfInterest>>> expCats = new ArrayList<>();


        //category 1 - restaurants
        ArrayList<PointOfInterest>points1 = new ArrayList<>();

        Location loc2 = createLocation("UNKNOWN", 52.62095956, 1.236450076);
        PointOfInterest poi2 = new PointOfInterest("Bio Cafe", loc2, "UEA Campus",
                "New description Bio Cafe");
        points1.add(poi2);

        Location loc1 = createLocation("UNKNOWN", 52.621483, 1.24073);
        PointOfInterest poi1 = new PointOfInterest("Campus Kitchen- Zest", loc1, "UEA Campus",
                "A friendly open place to eat in the centre of campus.");
        points1.add(poi1);

        Location loc3 = createLocation("UNKNOWN", 52.62165, 1.24073);
        PointOfInterest poi3 = new PointOfInterest("Vista", loc3, "UEA Campus",
                "Vista is an oasis of tranquility for staff and post graduates alike. Serving superb barista coffee, delicious lunches and tasty afternoon snacks.");
        points1.add(poi3);

        Pair<String,ArrayList<PointOfInterest>> cat1 = Pair.create("restaurants", points1);
        expCats.add(cat1);

        //category 2 - accommodation
        ArrayList<PointOfInterest>points2 = new ArrayList<>();

        Location loc4 = createLocation("UNKNOWN", 52.622128, 1.244792);
        PointOfInterest poi4 = new PointOfInterest("Britten House", loc4, "UEA Campus",
                "Ensuite Campus Accommodation");
        points2.add(poi4);

        Location loc5 = createLocation("UNKNOWN", 52.621407, 1.65453);
        PointOfInterest poi5 = new PointOfInterest("Constable Terrace", loc5, "UEA Campus",
                "Ensuite Campus Accommodation Constable Terrace");
        points2.add(poi5);

        Location loc6 = createLocation("UNKNOWN", 52.62938047, 1.235135794);
        PointOfInterest poi6 = new PointOfInterest("Pine House", loc6, "UEA University Village",
                "Accommodation in the University Village");
        points2.add(poi6);

        Location loc7 = createLocation("UNKNOWN", 52.62912325, 1.234969497);
        PointOfInterest poi7 = new PointOfInterest("Willow House", loc7, "New location name",
                "Accommodation in the University Village");
        points2.add(poi7);

        Pair<String,ArrayList<PointOfInterest>> cat2 = Pair.create("accommodation", points2);
        expCats.add(cat2);

        //category 3 - academic
        ArrayList<PointOfInterest>points3 = new ArrayList<>();

        Location loc10 = createLocation("UNKNOWN", 52.623531, 1.24535);
        PointOfInterest poi10 = new PointOfInterest("INTO University of East Anglia", loc10, "UEA Campus",
                "INTO University of East Anglia");
        points3.add(poi10);

        Location loc8 = createLocation("UNKNOWN", 52.624611, 1.224031);
        PointOfInterest poi8 = new PointOfInterest("Norwich Research Park", loc8, "UEA Campus",
                "New description");
        points3.add(poi8);

        Location loc9 = createLocation("UNKNOWN", 52.621052, 1.237673);
        PointOfInterest poi9 = new PointOfInterest("School of Computing Sciences", loc9, "UEA Campus", "");
        points3.add(poi9);



        Pair<String,ArrayList<PointOfInterest>> cat3 = Pair.create("academic", points3);
        expCats.add(cat3);

        //category 3 - study facilities
        ArrayList<PointOfInterest>points4 = new ArrayList<>();

        Location loc11 = createLocation("UNKNOWN", 52.621258, 1.240329);
        PointOfInterest poi11 = new PointOfInterest("Lecture Theatres", loc11, "UEA Campus",
                null);
        points4.add(poi11);

        Location loc12 = createLocation("UNKNOWN", 52.62, 1.3);
        PointOfInterest poi12 = new PointOfInterest("UEA Library", loc12, "UEA Campus",
                "The Library plays an important part in the life of every student. The Library contains over 800,000 books and journals, 1,200 study spaces, over 260 computers for student use and 24 hour open access to the IT facility.");
        points4.add(poi12);

        Pair<String,ArrayList<PointOfInterest>> cat4 = Pair.create("study facilities", points4);
        expCats.add(cat4);


        ArrayList<String>actCatNames = Collections.list(instance.getCategories());

        ArrayList<Pair<String, ArrayList<PointOfInterest>>> actCats = new ArrayList<>();
        for(String catName:actCatNames){
            ArrayList<PointOfInterest>actPoiList = new ArrayList<>();
            for(PointOfInterest p :instance.getPointsOfInterest(catName)){
                actPoiList.add(p);
            }
            Pair<String,ArrayList<PointOfInterest>> cat = Pair.create(catName.toLowerCase(), actPoiList);
            actCats.add(cat);
        }//end for


        Collections.sort(actCats, new Comparator<Pair<String, ArrayList<PointOfInterest>>>() {
            @Override
            public int compare(Pair<String, ArrayList<PointOfInterest>> o1, Pair<String, ArrayList<PointOfInterest>> o2) {
                return o1.first.toUpperCase().compareTo(o2.first.toUpperCase());
            }
        });

        Collections.sort(expCats, new Comparator<Pair<String, ArrayList<PointOfInterest>>>() {
            @Override
            public int compare(Pair<String, ArrayList<PointOfInterest>> o1, Pair<String, ArrayList<PointOfInterest>> o2) {
                return o1.first.toUpperCase().compareTo(o2.first.toUpperCase());
            }
        });

        if(expCats.size()!=actCats.size())
            fail("Number of categories must be the same. Expected: "+expCats.size()+"; actual: "+actCats.size());

        //check all categories
        for(int i = 0; i< expCats.size();i++){
            if(!(expCats.get(i).first.equals(actCats.get(i).first)))
                fail("Categories must be the same. Expected: "+expCats.get(i).first+"; actual: "+actCats.get(i).first);

            //check all points of interest
            for(int j = 0;j<expCats.get(i).second.size();j++){
                if(!(expCats.get(i).second.get(j).equals(actCats.get(i).second.get(j))))
                    fail("Pois must be the same. Expected: "+expCats.get(i).second.get(j)+
                          "; actual: "+actCats.get(i).second.get(j)+". Category: "+expCats.get(i).first+"poi number: "+j);
            }
        }

        resetSingleton();
    }

    @Test
    public void addOrUpdateCategory() throws SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
        CategoryList instance =  CategoryList.getInstance();

        //add first two categories
        ArrayList<PointOfInterest> cat1PointsCopy = new ArrayList<>(points);
        instance.addOrUpdateCategory("cat1", cat1PointsCopy);
        instance.addOrUpdateCategory("cat2", points);

        //update cat2
        ArrayList<PointOfInterest> cat2NewPOIs = new ArrayList<>();
        PointOfInterest poi2 = buildPOIInstance();
        PointOfInterest poi3 = buildPOIInstance();
        poi2.setName("This is new POI 1");
        poi3.setName("And this is new POI 2");
        cat2NewPOIs.add(poi1); cat2NewPOIs.add(poi2); cat2NewPOIs.add(poi3);
        instance.addOrUpdateCategory("cat2", cat2NewPOIs);

        //check if cats added/updated correctly
        ArrayList<String>cats = new ArrayList<>();
        cats.add("CAT1"); cats.add("CAT2");
        ArrayList<String>actCats = Collections.list(instance.getCategories());
        assertEquals(cats, actCats);

       // PointOfInterest[]expPoiList = {poi1, poi1, poi3, poi2};
        ArrayList<PointOfInterest>expPoiList = new ArrayList<>();
        expPoiList.add(poi1); expPoiList.add(poi3); expPoiList.add(poi1); expPoiList.add(poi2);

        ArrayList<PointOfInterest>actPoiList = new ArrayList<>();

        for(String catName:actCats){
            for(PointOfInterest p :instance.getPointsOfInterest(catName)){
                actPoiList.add(p);
            }
        }
        assertEquals(expPoiList, actPoiList);
        resetSingleton();
    }

    @Test(expected = NullPointerException.class)
    public void addOrUpdateCategoryNullCategoryName(){
        CategoryList instance = CategoryList.getInstance();
        instance.addOrUpdateCategory(null, points);
    }

    @Test(expected = IllegalArgumentException.class)
    public void addOrUpdateCategoryEmptyCategoryName(){
        CategoryList instance = CategoryList.getInstance();
        instance.addOrUpdateCategory("", points);
    }

    @Test
    public void loadCategories() throws SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
        CategoryList instance = CategoryList.getInstance();

        //add first two categories
        ArrayList<PointOfInterest> cat1PointsCopy = new ArrayList<>(points);
        instance.addOrUpdateCategory("cat1", cat1PointsCopy);
        instance.addOrUpdateCategory("cat2", points);

        //create collection of categories
        ArrayList<PointOfInterest> cat2NewPOIs= new ArrayList<>();
        PointOfInterest poi2 = buildPOIInstance();
        PointOfInterest poi3 = buildPOIInstance();
        poi2.setName("This is new POI 1");
        poi3.setName("And this is new POI 2");
        cat2NewPOIs.add(poi1); cat2NewPOIs.add(poi2); cat2NewPOIs.add(poi3);

        Pair<String,ArrayList<PointOfInterest>> c1 = Pair.create("cat3", cat1PointsCopy);
        Pair<String,ArrayList<PointOfInterest>> c2 = Pair.create("cat2", cat2NewPOIs);
        Collection <Pair<String,ArrayList<PointOfInterest>>> col = new ArrayList<>();
        col.add(c1);col.add(c2);

        Context context = InstrumentationRegistry.getTargetContext();;
        instance.loadCategories(col, context);

        //check if added/updated correctly
        ArrayList<String>expCats = new ArrayList<>();
        expCats.add("CAT1");expCats.add("CAT3");expCats.add("CAT2");
        ArrayList<String>actCats = Collections.list(instance.getCategories());
        assertEquals(expCats, actCats);

        ArrayList<PointOfInterest>expPoiList = new ArrayList<>();
        expPoiList.add(poi1); expPoiList.add(poi1); expPoiList.add(poi3); expPoiList.add(poi1); expPoiList.add(poi2);
        ArrayList<PointOfInterest>actPoiList = new ArrayList<>();
        for(String catName:actCats){
            for(PointOfInterest p :instance.getPointsOfInterest(catName)){
                actPoiList.add(p);
            }
        }
        assertEquals(expPoiList, actPoiList);

        resetSingleton();
    }

    @Test(expected = RuntimeException.class)
    public void loadCategoriesNullCategoryList(){
        CategoryList instance = CategoryList.getInstance();
        Collection col = null;
        Context context = InstrumentationRegistry.getTargetContext();;
        instance.loadCategories(col, context);
    }

    @Test
    public void removeCategory() throws SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
        CategoryList instance = CategoryList.getInstance();
        ArrayList<PointOfInterest> cat1PointsCopy = new ArrayList<>(points);
        instance.addOrUpdateCategory("CATA", cat1PointsCopy);
        instance.addOrUpdateCategory("CATB", cat1PointsCopy);
        instance.addOrUpdateCategory("CATC", cat1PointsCopy);
        instance.addOrUpdateCategory("CATD", cat1PointsCopy);
        instance.removeCategory("CATC");

        String[]expCats = {"CATA","CATB", "CATD"};
        ArrayList<String>actCats = Collections.list(instance.getCategories());
        Collections.sort(actCats);
        assertArrayEquals(expCats, actCats.toArray());

        resetSingleton();
    }

    @Test(expected = NullPointerException.class)
    public void removeCategoryNullCategoryName(){
        CategoryList instance = CategoryList.getInstance();
        instance.removeCategory(null);
    }

    @Test
    public void getCategories() throws SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
        CategoryList instance = CategoryList.getInstance();

        instance.addOrUpdateCategory("acat", points);
        instance.addOrUpdateCategory("bcat", points);
        instance.addOrUpdateCategory("ccat", points);
        instance.addOrUpdateCategory("dcat", points);

        String[]expCats = {"ACAT", "BCAT", "CCAT", "DCAT"};
        ArrayList<String>actCats = Collections.list(instance.getCategories());
        Collections.sort(actCats);
        assertArrayEquals(expCats, actCats.toArray());

        resetSingleton();
    }

    @Test
    public void getPointsOfInterest() throws SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
        CategoryList instance = CategoryList.getInstance();

        instance.addOrUpdateCategory("cat0", points);
        instance.addOrUpdateCategory("cat1", points);
        instance.addOrUpdateCategory("cat2", points);
        instance.addOrUpdateCategory("cat3", points);

        PointOfInterest[]expPoiList = {poi1, poi1, poi1, poi1};
        ArrayList<PointOfInterest>actPoiList = new ArrayList<>();

        ArrayList<String>actCats = Collections.list(instance.getCategories());
        for(String catName:actCats){
            for(PointOfInterest p :instance.getPointsOfInterest(catName)){
                actPoiList.add(p);
            }
        }
        assertArrayEquals(expPoiList, actPoiList.toArray());

        resetSingleton();
    }

    @Test
    public void getPointsOfInterestAsArrayList() throws SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
        CategoryList instance = CategoryList.getInstance();
        instance.addOrUpdateCategory("cat1", points);
        assertEquals(points, instance.getPointsOfInterestAsArrayList("cat1"));
        resetSingleton();
    }

    @Test(expected = NullPointerException.class)
    public void getPointsOfInterestAsArrayListNullCategoryName(){
        CategoryList instance = CategoryList.getInstance();
        instance.getPointsOfInterestAsArrayList(null);
    }

    @Test
    public void addOrUpdatePointOfInterest() throws SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException  {
        CategoryList instance = CategoryList.getInstance();
        instance.addOrUpdateCategory("cat1", points);
        //duplicate poi
        PointOfInterest poi2 = buildPOIInstance();
        instance.addOrUpdatePointOfInterest("cat1", poi2);

        //new poi
        PointOfInterest poi3 = buildPOIInstance();
        poi3.setName("New POI");
        instance.addOrUpdatePointOfInterest("cat1", poi3);

        //update poi
        PointOfInterest poi4 = buildPOIInstance();
        poi4.setDescription("new description");
        instance.addOrUpdatePointOfInterest("cat1", poi4);

        ArrayList<PointOfInterest> newPOIs = new ArrayList<>();
        newPOIs.add(poi4); newPOIs.add(poi3);

        assertEquals(newPOIs, instance.getPointsOfInterestAsArrayList("cat1"));

        resetSingleton();

    }

    @Test(expected = NullPointerException.class)
    public void addOrUpdatePointOfInterestNullCategoryName(){
        CategoryList instance = CategoryList.getInstance();
        instance.addOrUpdatePointOfInterest(null, poi1);
    }

    @Test(expected = NullPointerException.class)
    public void addOrUpdatePointOfInterestNullPOI() throws SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException  {
        CategoryList instance = CategoryList.getInstance();
        instance.addOrUpdateCategory("cat1", points);
        instance.addOrUpdatePointOfInterest("cat1", null);
        resetSingleton();
    }


    @Test
    public void removePointOfInterest() throws SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
        CategoryList instance = CategoryList.getInstance();
        instance.addOrUpdateCategory("cat1", points);

        //new poi
        PointOfInterest poi2 = buildPOIInstance();
        poi2.setName("New POI");
        instance.addOrUpdatePointOfInterest("cat1", poi2);

        PointOfInterest poi3 = buildPOIInstance();
        poi3.setName("Another new POI");
        instance.addOrUpdatePointOfInterest("cat1", poi3);

        instance.removePointOfInterest("cat1", poi2);

        ArrayList<PointOfInterest> newPOIs = new ArrayList<>();
        newPOIs.add(poi3); newPOIs.add(poi1);

        assertEquals(newPOIs, instance.getPointsOfInterestAsArrayList("cat1"));

        resetSingleton();
    }

    @Test(expected = NullPointerException.class)
    public void removePointOfInterestNullCategoryName() throws SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
        CategoryList instance = CategoryList.getInstance();
        instance.addOrUpdateCategory("cat1", points);
        instance.removePointOfInterest(null, poi1);
        resetSingleton();
    }

    @Test(expected = NullPointerException.class)
    public void removePointOfInterestNullPOI() throws SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
        CategoryList instance = CategoryList.getInstance();
        instance.addOrUpdateCategory("cat1", points);
        instance.removePointOfInterest("cat1", null);
        resetSingleton();
    }

    @Test
    public void findPOIByName() throws SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
        CategoryList instance = CategoryList.getInstance();
        instance.addOrUpdateCategory("cat1", points);
        assertEquals(poi1, instance.findPOIByName("cat1", name));
        resetSingleton();
    }

    @Test(expected = NullPointerException.class)
    public void findPOIByNameNullCategoryName() throws SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
        CategoryList instance = CategoryList.getInstance();
        instance.addOrUpdateCategory("cat1", points);
        instance.findPOIByName(null, "POI name");
        resetSingleton();
    }

    @Test(expected = NullPointerException.class)
    public void findPOIByNameNullPOIName() throws SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
        CategoryList instance = CategoryList.getInstance();
        instance.addOrUpdateCategory("cat1", points);
        instance.findPOIByName("cat1", null);
        resetSingleton();
    }

    @Test
    public void searchPOIByName() throws SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
        CategoryList instance = CategoryList.getInstance();
        ArrayList<PointOfInterest> cat1PointsCopy = new ArrayList<>(points);
        instance.addOrUpdateCategory("cat1", cat1PointsCopy);
        instance.addOrUpdateCategory("cat2", points);

        //add more POIs to cat1
        PointOfInterest poi2 = buildPOIInstance();
        poi2.setName("searchNewPOI1");
        instance.addOrUpdatePointOfInterest("cat1", poi2);
        PointOfInterest poi3 = buildPOIInstance();
        poi3.setName("newsearchPOI2");
        instance.addOrUpdatePointOfInterest("cat1", poi3);

        //add more POIs to cat2
        PointOfInterest poi4 = buildPOIInstance();
        poi4.setName("newPOI3sear");
        instance.addOrUpdatePointOfInterest("cat2", poi4);
        instance.addOrUpdatePointOfInterest("cat2", poi3);

        ArrayList<Pair<String, PointOfInterest>> exp = new ArrayList<>();
        exp.add(Pair.create("CAT1", poi3));
        exp.add(Pair.create("CAT1", poi2));
        exp.add(Pair.create("CAT2", poi4));
        exp.add(Pair.create("CAT2", poi3));

        ArrayList<Pair<String, PointOfInterest>> actCats = instance.searchPOIByName("sea", -1);
        Collections.sort(actCats, new Comparator<Pair<String, PointOfInterest>>() {
            @Override
            public int compare(Pair<String, PointOfInterest> o1, Pair<String, PointOfInterest> o2) {
                return o1.first.toUpperCase().compareTo(o2.first.toUpperCase());
            }
        });

        assertEquals(exp, actCats);

        resetSingleton();
    }

    @Test(expected = NullPointerException.class)
    public void searchPOIByNameNullSearchSeq(){
        CategoryList instance = CategoryList.getInstance();
        instance.searchPOIByName(null, 3);
    }

    @Test
    public void updateEntries() throws SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
        CategoryList instance = CategoryList.getInstance();
        instance.addOrUpdateCategory("cat1", points);

        //new pois for cat 1 - existing category
        PointOfInterest poi2 = buildPOIInstance();
        poi2.setName("New POI");
        PointOfInterest poi3 = buildPOIInstance();
        poi3.setName("Another new POI");
        //new pois for cat 2 - new category
        PointOfInterest poi4 = buildPOIInstance();
        poi4.setName("Another new POI2");
        PointOfInterest poi5 = buildPOIInstance();
        poi5.setDescription("New description");

        ArrayList<Pair<String, ArrayList<PointOfInterest>>> newEntries = new ArrayList<>();

        ArrayList<PointOfInterest> cat1POIs = new ArrayList<>();
        cat1POIs.add(poi2);
        cat1POIs.add(poi3);
        newEntries.add(Pair.create("cat1", cat1POIs));

        ArrayList<PointOfInterest> cat2POIs = new ArrayList<>();
        cat2POIs.add(poi4);
        cat2POIs.add(poi5);
        newEntries.add(Pair.create("cat2", cat2POIs));

        instance.updateEntries(newEntries);

        //check if cat1 updated correctly
        ArrayList<PointOfInterest>cat1NewPOIs = new ArrayList<>();
        cat1NewPOIs.add(poi3); cat1NewPOIs.add(poi1); cat1NewPOIs.add(poi2);
        assertEquals(cat1NewPOIs, instance.getPointsOfInterestAsArrayList("cat1"));

        //check if cat2 updated correctly
        assertEquals(cat2POIs, instance.getPointsOfInterestAsArrayList("cat2"));

        resetSingleton();
    }

}


package uk.ac.uea.gitguud.framework;

import android.location.Location;
import android.util.Log;
import android.util.Pair;

import org.junit.Test;

import java.util.ArrayList;

import uk.ac.uea.gitguud.framework.model.JSONToPOIParser;
import uk.ac.uea.gitguud.framework.model.PointOfInterest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

/**
 * Created by Pavel Solodilov on 24/01/2017.
 */
public class JSONToPOIParserTest {

    private Location createLocation(String provider, double latitude, double longitude) {
        Location location = new Location(provider);
        location.setLatitude(latitude);
        location.setLongitude(longitude);
        return location;
    }

    private final String LOG_TAG = "JSONTest";

    private String json = "{\n" +
            "  \"src\": \"http\",\n" +
            "  \"name\": \"source name\",\n" +
            "  \"categories\": {\n" +
            "    \"restaurants\": {\n" +
            "      \"Campus Kitchen-Zest\": {\n" +
            "        \"location\": {\n" +
            "          \"name\": \"UEA Campus\",\n" +
            "          \"latitude\": 52.621483,\n" +
            "          \"longitude\": 1.24073\n" +
            "        },\n" +
            "        \"description\": \"A friendly open place to eat in the centre of campus.\",\n" +
            "        \"coordinates\": \"1QTEST\"\n" +
            "      },\n" +
            "      \"Bio Cafe\": {\n" +
            "        \"location\": {\n" +
            "          \"name\": \"UEA Campus\",\n" +
            "          \"latitude\": 52.62095956,\n" +
            "          \"longitude\": 1.236450076\n" +
            "        },\n" +
            "        \"description\": \"ssasasassas\"\n" +
            "      }\n" +
            "    },\n" +
            "    \"accommodation\": {\n" +
            "      \"Britten House\": {\n" +
            "        \"location\": {\n" +
            "          \"name\": \"UEA Campus\",\n" +
            "          \"latitude\": 52.622128,\n" +
            "          \"longitude\": 1.244792\n" +
            "        },\n" +
            "        \"description\": \"Ensuite Campus Accommodation.\"\n" +
            "      },\n" +
            "      \"Constable Terrace\": {\n" +
            "        \"location\": {\n" +
            "          \"name\": \"UEA Campus\",\n" +
            "          \"latitude\": 52.621407,\n" +
            "          \"longitude\": 1.234353\n" +
            "        },\n" +
            "        \"description\": \"Ensuite Campus Accommodation.\"\n" +
            "      }\n" +
            "    }\n" +
            "  }\n" +
            "}";

    @Test
    public void parse(){
        JSONToPOIParser parser = new JSONToPOIParser();
        ArrayList<Pair<String, ArrayList<PointOfInterest>>> categories = parser.parse(json);
        assertNotNull("Categories should not have been null", categories);

        ArrayList<Pair<String, ArrayList<PointOfInterest>>> expCats = new ArrayList<>();

        Pair<String, String> source = Pair.create("source name", "http");

        ArrayList<PointOfInterest>points2 = new ArrayList<>();

        Location loc3 = createLocation("uk.ac.uea.gitguud.framework.model.JSONToPOIParser", 52.622128, 1.244792);
        PointOfInterest poi3 = new PointOfInterest("Britten House", loc3, "UEA Campus",
                "Ensuite Campus Accommodation.");
        poi3.setSource(source);
        points2.add(poi3);

        Location loc4 = createLocation("uk.ac.uea.gitguud.framework.model.JSONToPOIParser", 52.621407, 1.234353);
        PointOfInterest poi4 = new PointOfInterest("Constable Terrace", loc4, "UEA Campus",
                "Ensuite Campus Accommodation.");
        poi4.setSource(source);
        points2.add(poi4);

        Pair<String,ArrayList<PointOfInterest>> cat2 = Pair.create("accommodation", points2);
        expCats.add(cat2);

        ArrayList<PointOfInterest>points1 = new ArrayList<>();

        Location loc2 = createLocation("uk.ac.uea.gitguud.framework.model.JSONToPOIParser", 52.62095956, 1.236450076);
        PointOfInterest poi2 = new PointOfInterest("Bio Cafe", loc2, "UEA Campus",
                "ssasasassas");
        poi2.setSource(source);
        points1.add(poi2);

        Location loc1 = createLocation("uk.ac.uea.gitguud.framework.model.JSONToPOIParser", 52.621483, 1.24073);
        PointOfInterest poi1 = new PointOfInterest("Campus Kitchen-Zest", loc1, "UEA Campus",
                "A friendly open place to eat in the centre of campus.");
        poi1.setCoordinateString("1QTEST");
        poi1.setSource(source);
        points1.add(poi1);

        Pair<String,ArrayList<PointOfInterest>> cat1 = Pair.create("restaurants", points1);
        expCats.add(cat1);

        //check all categories
        for(int i = 0; i< expCats.size();i++){
            if(!(expCats.get(i).first.equals(categories.get(i).first)))
                fail("Categories must be the same. Expected: "+expCats.get(i).first+"; actual: "+categories.get(i).first);

            //check all points of interest
            for(int j = 0;j<expCats.get(i).second.size();j++){
                if(!(expCats.get(i).second.get(j).equals(categories.get(i).second.get(j))))
                    fail("Pois must be the same. Expected: "+expCats.get(i).second.get(j)+
                            "; actual: "+categories.get(i).second.get(j)+". Category: "+expCats.get(i).first+"; poi number: "+j);
            }
        }

        assertEquals(expCats, categories);
    }
}

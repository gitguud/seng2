package uk.ac.uea.gitguud.framework.interfaces;

/**
 * StateObserver.java
 *
 * Observer of the observer design pattern.
 * Created by Pavel Solodilov on 04/11/16.
 */
public interface StateObserver {

    /**
     * Notifies the observer of an update in the subject.
     */
    void updateNotify();
}
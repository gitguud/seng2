package uk.ac.uea.gitguud.framework.convenience;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

/**
 * TimeSpan.java
 *
 * Used to model a timespan (e.g. opening and closing times of a shop).
 * Created by Pavel Solodilov on 15/11/16.
 */
public class TimeSpan implements Parcelable {
    private final Date START_TIME;
    private final Date END_TIME;

    /**
     * Constructor. The time span is unmodifiable, and therefore safe to pass to different objects.
     * <p>
     * The provided objects will be copied, and therefore any modifications to the school activity
     * from outside of the object are impossible.
     *
     *
     * @param startTime Start time of the time span.
     * @param endTime End time of the time span.
     * @throws NullPointerException if any of the provided objects are null.
     * @throws IllegalArgumentException if the provided start time is after or equal to the
     *                                  end time. This check is here to provide a more meaningful
     *                                  in-app debug error message to the developers than if it
     *                                  was thrown by some other call within this same method.
     */
    public TimeSpan(Date startTime, Date endTime)
            throws IllegalArgumentException, NullPointerException {

        if (startTime == null) {
            throw new NullPointerException("Start time cannot be null");
        } else if (endTime == null) {
            throw new NullPointerException("End time cannot be null");
        } else if (!ErrorChecking.startDateBeforeEndDate(startTime, endTime, false)) {
            throw new IllegalArgumentException
                    ("Start date cannot be after or during the end date.");
        }

        this.START_TIME = new Date(startTime.getTime());
        this.END_TIME = new Date(endTime.getTime());
    }

    /**
     * Accessor for the start time of the time span.
     * <p>
     * Returns a copy of the corresponding object, and therefore any changes to it will not affect
     * the container.
     *
     * @return Start time of the time span.
     */
    public Date getStartTime() {
        return new Date(this.START_TIME.getTime());
    }

    /**
     * Accessor for the end time of the school time span.
     * <p>
     * Returns a copy of the corresponding object, and therefore any changes to it will not affect
     * the container.
     *
     * @return End time of the time span.
     */
    public Date getEndTime() {
        return new Date(this.END_TIME.getTime());
    }


    /**
     * Two time spans are equal if their start and end times match.
     *
     * @param other Another time span to compare with.
     * @return True if the time spans match, and false if they do not.
     */
    @Override
    public boolean equals(Object other) {
        TimeSpan ts = (TimeSpan) other;

        if (ts.getStartTime().equals(this.getStartTime()) &&
                ts.getEndTime().equals(this.getEndTime())) {
            return true;
        }

        return false;
    }

    // ------------------------------------ PARCELABLE ------------------------------------------ //

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        // Write the start time first, and end time second.
        dest.writeLong(this.getStartTime().getTime());
        dest.writeLong(this.getEndTime().getTime());
    }

    protected TimeSpan(Parcel in) {
        // Read the start time first, and end time second.
        this.START_TIME = new Date(in.readLong());
        this.END_TIME = new Date(in.readLong());
    }

    public static final Creator<TimeSpan> CREATOR = new Creator<TimeSpan>() {
        @Override
        public TimeSpan createFromParcel(Parcel in) {
            return new TimeSpan(in);
        }

        @Override
        public TimeSpan[] newArray(int size) {
            return new TimeSpan[size];
        }
    };
}

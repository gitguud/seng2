package uk.ac.uea.gitguud.framework.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.view.View;

import uk.ac.uea.gitguud.framework.AndroidFileIO;

/**
 * ViewFunctions.java
 * Provides commonly used view functions.
 *
 * Created by Pavel Solodilov on 21/01/17.
 */
public class ViewFunctions {

    /**
     * Loads the background image from the given directory with the provided filename.
     *
     * @param bgImageDirectory Directory to load from.
     * @param bgImageFileName File to load.
     * @param context Context of the app.
     * @return Background image.
     */
    @Nullable
    public static Drawable loadBackgroundImage(String bgImageDirectory, String bgImageFileName,
                                               Context context) {

        final Bitmap bitmap = AndroidFileIO.getInstance().loadImageFromStorage(
                bgImageDirectory, bgImageFileName, context);

        return new BitmapDrawable(context.getResources(), bitmap);
    }

    /**
     * Sets an image as background of the view, having applied a whitening filter to it beforehand.
     *
     * @param view View to set the background of.
     * @param image Image to set as the background.
     */
    public static void setWhitenedImage(View view, Drawable image) {
        image.setColorFilter(Color.parseColor("#A0FFFFFF"), PorterDuff.Mode.ADD);
        view.setBackground(image);
    }
}

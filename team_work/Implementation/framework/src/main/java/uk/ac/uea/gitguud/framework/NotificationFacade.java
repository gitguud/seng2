package uk.ac.uea.gitguud.framework;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;


/**
 * NotificationFacade.java
 *
 * Simplifies interactions with notifications.
 * Created by Pavel Solodilov on 06/12/16.
 */
public class NotificationFacade {

    private static NotificationFacade instance;

    /**
     * Accessor for the notification facade.
     *
     * @return Notification facade.
     */
    public static NotificationFacade getInstance() {

        // Singleton pattern implementation.
        if (NotificationFacade.instance == null) {
            NotificationFacade.instance = new NotificationFacade();
        }

        return NotificationFacade.instance;
    }

    /**
     * Constructor for a notification facade. Private due to singleton pattern implementation.
     */
    private NotificationFacade() {
    }


    /**
     * Displays a notification without a specified ID (sets it to 0 by default).
     *
     * {@link NotificationFacade#displayNotification(Intent, NotificationCompat.Builder, Context, Class, int)}
     */
    public void displayNotification(Intent resultIntent, NotificationCompat.Builder builder,
                                    Context context, Class resultActivity) {
        this.displayNotification(resultIntent, builder, context, resultActivity, 0);
    }

    /**
     * Method to display an already build notification using a notification builder.
     *
     * @param resultIntent Intent that will use the current app's context and call the resulting
     *                     activity. This should also contain all of the data you want to send to
     *                     the resulting activity.
     * @param builder Builder with a pre-built notification.
     * @param context Context of the application.
     * @param resultActivity Class of the activity to run when the notification is interacted with.
     *                       Make sure to declare it in the manifest for this not to crash the app.
     * @param id ID of the notification.
     */
    public void displayNotification(Intent resultIntent, NotificationCompat.Builder builder,
                                    Context context, Class resultActivity, int id) {
        // The stack builder object will contain an artificial back stack for the started Activity.
        // This ensures that navigating backward from the Activity leads out of your application to
        // the Home screen.
        // https://developer.android.com/guide/topics/ui/notifiers/notifications.html
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);

        // Adds the back stack for the Intent (but not the Intent itself), and then
        // adds the Intent that starts the Activity to the top of the stack.
        stackBuilder.addParentStack(resultActivity).addNextIntent(resultIntent);

        // The pending intent is used to display the notification.
        PendingIntent notificationPendingIntent =
                stackBuilder.getPendingIntent(0, PendingIntent.FLAG_CANCEL_CURRENT);

        // Attach a pending intent to the builder in order to actually show it, and then get a
        // reference to the notification manager.
        builder.setContentIntent(notificationPendingIntent);
        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        // Display the notification.
        notificationManager.notify(id, builder.build());
    }
}

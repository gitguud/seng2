package uk.ac.uea.gitguud.framework.model;

import android.location.Location;
import android.support.annotation.NonNull;
import android.util.Log;
import android.util.Pair;
import android.util.Xml;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;

import uk.ac.uea.gitguud.framework.convenience.AndroidSpecific;
import uk.ac.uea.gitguud.framework.convenience.ErrorChecking;
import uk.ac.uea.gitguud.framework.interfaces.ParseStrategy;

/**
 * XMLToPOIParser.java
 *
 * Provides a facility to parse an XML string into pairs of a string (category name), and a point of
 * interest. Namespaces are not supported.
 *
 * Please note that the category and POI names are case-insensitive.
 * As of writing, 19/11/2016, a typical schema looks like this (XML headers omitted):
 * <data src="Source location" name="Name of the source">
 *     <!-- Tag names are case-sensitive. The 'src' and 'name' attributes are optional. -->
 *     <!-- Please also note that if the source is present and the name is not, the source name is changed to 'personal'. -->
 *     <category name="restaurant">
 *         <poi name="Campus Kitchen">    <!-- Mandatory -->
 *             <location name="UEA Campus">    <!-- The location name (address) is optional. -->
 *                 <latitude>52.621543</latitude>    <!-- The locations are mandatory. -->
 *                 <longitude>1.240704</longitude>
 *             </location>
 *             <description>        <!-- The description is optional -->
 *                 Some descriptive text.
 *             </description>
 *             <coordinates>
 *                 The coordinates for the location using the UEA campus map. Just a string.
 *             </coordinates>
 *             <image type="url/id"></image> <!-- The image tag is optional.
 *                                                   The image is URL or the ID of the stored in-app
 *                                                   image. The 'type' attribute is mandatory.
 *                                                   TODO implement; DO NOT TEST THAT -->
 *             </poi>
 *         <poi>
 *             ...
 *         </poi>
 *         ...
 *     </category>
 *     <category>
 *         ...
 *     </category>
 * </data>
 *
 * Based on https://developer.android.com/training/basics/network-ops/xml.html
 *
 * Created by Katrina on 13/11/2016.
 */
public class XMLToPOIParser implements ParseStrategy<Pair<String, ArrayList<PointOfInterest>>> {

    private final String LOG_TAG = AndroidSpecific.getLogTag(this);

    private final static String NS = null; // No namespace.

    private final static String TAG_DATA        = "data";
    private final static String TAG_CATEGORY    = "category";
    private final static String TAG_POI         = "poi";
    private final static String TAG_LOCATION    = "location";
    private final static String TAG_LATITUDE    = "latitude";
    private final static String TAG_LONGITUDE   = "longitude";
    private final static String TAG_DESCRIPTION = "description";
    private final static String TAG_COORDINATES = "coordinates";

    // TODO USE!
    private final static String ATTR_IMAGE_TYPE = "type";
    private final static String ATTR_DATA_SRC = "src";
    private final static String ATTR_DATA_SRC_NAME = "name";

    private final static String TAG_IMAGE = "image"; // Ignore for now
    private final static String VAL_ATTR_IMAGE_TYPE_ID = "id";
    private final static String VAL_ATTR_IMAGE_TYPE_URL = "url";

    private final static String ATTR_CATEGORY_NAME = "name";
    private final static String ATTR_POI_NAME      = "name";
    private final static String ATTR_LOCATION_NAME = "name";

    private String sourceName;
    private String sourceUri;

    /**
     * Constructs an array list of pairs, which contains the name of the category and a POI object.
     *
     * @param xmlString XML string to parse.
     * @return An empty array list if the given string was empty, and null if it was null or
     *         malformed. Returns an array list with pair entries on successful parse.
     */
    public ArrayList<Pair<String, ArrayList<PointOfInterest>>> parse(String xmlString) {

        ArrayList<Pair<String, ArrayList<PointOfInterest>>> result = new ArrayList<>();
        if (xmlString != null && xmlString.isEmpty()) return result;

        // Parse the XML.
        try {
            XmlPullParser parser = Xml.newPullParser();
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(new StringReader(xmlString));

            // Skip the XML declaration tag.
            parser.nextTag();

            // Read all of the categories from within the assumed "data" tag.
            result.addAll(this.readCategories(parser));
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(LOG_TAG, e.getMessage());
            return null;
        }

        return result;
    }

    /**
     * Reads all of the categories contained within the "data" tags.
     *
     * @param parser Parser pointing a the category tag to start reading.
     * @return All of the categories in this bit of XML until the next "data" tag is hit.
     * @throws XmlPullParserException if the expected data did not match the provided data.
     * @throws IOException if the XML was malformed.
     */
    @NonNull
    private ArrayList<Pair<String, ArrayList<PointOfInterest>>> readCategories(
            XmlPullParser parser) throws XmlPullParserException, IOException {

        // Make sure that the opening tag is "data".
        parser.require(XmlPullParser.START_TAG, NS, TAG_DATA);

        // Set the object-wide source and source name values.
        sourceName = parser.getAttributeValue(NS, ATTR_DATA_SRC_NAME);
        sourceUri = parser.getAttributeValue(NS, ATTR_DATA_SRC);

        ArrayList<Pair<String, ArrayList<PointOfInterest>>> categories = new ArrayList<>();
        String tagName;

        // Until encountered the end tag (supposedly the closing "data" tag).
        while (parser.next() != XmlPullParser.END_TAG) {

            // Skip all of the non-starting tags.
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }

            // Get the name of the current tag.
            tagName = parser.getName();

            // If encountered a category, read and add a category.
            /// Otherwise, skip the whole irrelevant tag.
            if (tagName.equals(TAG_CATEGORY)) {
                categories.add(this.readCategory(parser));
            } else {
                this.skip(parser);
            }
        }

        // Finally, return the parsed categories.
        return categories;
    }

    /**
     * Reads a single "category" tag with all of its points of interest. It is assumed that the
     * parser will always point to a "category" tag.
     *
     * @param parser XML parser to use read from. It is assumed that it points at a "category" tag.
     * @return A pair containing the name of the category, and an array list of points of interest.
     * @throws XmlPullParserException if the expected data did not match the provided data.
     * @throws IOException if the XML was malformed.
     */
    private Pair<String, ArrayList<PointOfInterest>> readCategory(XmlPullParser parser)
            throws XmlPullParserException, IOException {

        parser.require(XmlPullParser.START_TAG, NS, TAG_CATEGORY);
        String categoryName = parser.getAttributeValue(NS, ATTR_CATEGORY_NAME);

        ArrayList<PointOfInterest> pointsOfInterest = new ArrayList<>();

        // Keep at it until reached the closing tag (supposedly the closing "category" tag).
        while (parser.next() != XmlPullParser.END_TAG) {

            // Skip all of the non-starting tags.
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }

            // If encountered an opening point of interest tag, add the POI.
            /// Otherwise, skip it.
            if (parser.getName().equals(TAG_POI)) {
                pointsOfInterest.add(this.readPoi(parser));
            } else {
                skip(parser);
            }
        } // end while



        // Construct and return the resulting pair.
        return new Pair<>(categoryName, pointsOfInterest);
    }


    /**
     * Reads a single point of interest into an object. The ordering of the tags does not matter.
     * Superfluous tags will be ignored. The required tag is "location" with "latitude" and
     * "longitude" as numbers inside.
     *
     * @param parser Parser to read from. Note that it is assumed that it points at a "poi" opening
     *               tag.
     * @return A point of interest made of the data inside the "poi" element.
     * @throws XmlPullParserException if the expected data did not match the provided data.
     * @throws IOException if the XML was malformed.
     * @throws NullPointerException If some data values were empty when providing them to the
     *                              PointOfInterest constructor.
     * @throws IllegalArgumentException Same as NullPointerException.
     */
    private PointOfInterest readPoi(XmlPullParser parser) throws XmlPullParserException,
            IOException, NullPointerException, IllegalArgumentException {

        parser.require(XmlPullParser.START_TAG, NS, TAG_POI);
        String poiName = parser.getAttributeValue(NS, ATTR_POI_NAME);

        // Set up the POI fields.
        String   locationString   = null;
        String   description      = null;
        String   coordinateString = null;
        Location location         = null;

        // Go until encountered a closing tag.
        while (parser.nextTag() != XmlPullParser.END_TAG) {
            switch(parser.getName()) {
                case TAG_DESCRIPTION:
                    description = this.readDescription(parser).trim();
                    break;
                case TAG_LOCATION:
                    try {
                        locationString = parser.getAttributeValue(NS, ATTR_LOCATION_NAME).trim();
                    } catch (Exception e) {
                         // Ignore
                    }

                    location = this.readLocation(parser);
                    break;
                case TAG_COORDINATES:
                    coordinateString = this.readCoordinates(parser).trim();
                    break;
            }
        }

        // Create the new POI, set and set its coordinate strings.
        PointOfInterest poi = new PointOfInterest(poiName, location, locationString, description);
        poi.setCoordinateString(coordinateString);

        // If source is present, but the source name is not, set source name to 'personal'.
        if (sourceUri != null && sourceName == null) {
            sourceName = "PERSONAL";
        }

        // If both the source URI and the name (set above) are present, set the source string pair.
        if (sourceUri != null) {
            poi.setSource(new Pair<>(sourceName, sourceUri));
        }

        return poi;
    }


    /**
     * Reads in a "location" tag which should contain the "latitude" and "longitude" tags.
     *
     * @param parser Parser that points at a "location" tag.
     * @return A location read from the "location tag".
     * @throws XmlPullParserException if the expected data did not match the provided data.
     * @throws IOException if the XML was malformed.
     * @throws NullPointerException if either of the "latitude" or "longitude" tags is missing.
     * @throws NumberFormatException if either the latitude or longitude were not numbers.
     */
    private Location readLocation(XmlPullParser parser) throws XmlPullParserException, IOException {
        parser.require(XmlPullParser.START_TAG, NS, TAG_LOCATION);
        String latitude = null;
        String longitude = null;

        while (parser.nextTag() != XmlPullParser.END_TAG) {

            switch (parser.getName()) {
                case TAG_LATITUDE:
                    latitude = this.readText(parser);
                    break;
                case TAG_LONGITUDE:
                    longitude = this.readText(parser);
                    break;
            }
        }

        // This does all of the exception throwing on errors.
        Double lat = Double.parseDouble(latitude);
        Double lng = Double.parseDouble(longitude);

        // Location with provider of this class name.
        Location location = new Location(this.getClass().getName());
        location.setLatitude(lat);
        location.setLongitude(lng);

        return location;
    }


    /**
     * Reads the coordinates (text insides of the tag).
     *
     * @param parser Parser pointing at the "coordinates" tag.
     * @return Coordinates' text.
     * @throws XmlPullParserException if the expected data did not match the provided data.
     * @throws IOException if the XML was malformed.
     */
    private String readCoordinates(XmlPullParser parser) throws XmlPullParserException,
            IOException {
        parser.require(XmlPullParser.START_TAG, NS, TAG_COORDINATES);
        String coordinates = this.readText(parser);
        parser.require(XmlPullParser.END_TAG, NS, TAG_COORDINATES);
        return coordinates;
    }

    /**
     * Reads the description (text insides of the tag).
     *
     * @param parser Parser pointing at the "description" tag.
     * @return Description text.
     * @throws XmlPullParserException if the expected data did not match the provided data.
     * @throws IOException if the XML was malformed.
     */
    private String readDescription(XmlPullParser parser) throws XmlPullParserException,
            IOException {
        parser.require(XmlPullParser.START_TAG, NS, TAG_DESCRIPTION);
        String description = this.readText(parser); // TODO newline characters are a bit awkward.
        parser.require(XmlPullParser.END_TAG, NS, TAG_DESCRIPTION);
        return description;
    }


    /**
     * Reads a text string from within the tag. Implies that the parser already points at the text.
     *
     * @param parser Parser to read from.
     * @return Text contained within the tag.
     * @throws XmlPullParserException if the expected data did not match the provided data.
     * @throws IOException if the XML was malformed.
     */
    private String readText(XmlPullParser parser) throws XmlPullParserException, IOException {
        String result = "";
        if (parser.next() == XmlPullParser.TEXT) {
            result = parser.getText();
            parser.nextTag();
        }
        return result;
    }


    /**
     * Skips a whole tag and its contents.
     *
     * @param parser
     * @throws XmlPullParserException if the expected data did not match the provided data.
     * @throws IOException if the XML was malformed.
     * @throws IllegalStateException if the first tag was not an opening tag.
     */
    private void skip(XmlPullParser parser) throws XmlPullParserException, IOException {

        // If this does not point at a start tag, throw an exception.
        if (parser.getEventType() != XmlPullParser.START_TAG) {
            throw new IllegalStateException();
        }

        // Skips the tag by descending tags inside, and then ascending from them, thus the depth.
        int depth = 1;
        while (depth != 0) {
            switch (parser.next()) {
                case XmlPullParser.END_TAG:
                    depth--; // If encountered an end tag, we went "up".
                    break;
                case XmlPullParser.START_TAG:
                    depth++; // If encountered a start tag, we went "down".
                    break;
            }
        } // end while
    } // end method skip
}

package uk.ac.uea.gitguud.framework.convenience;

/**
 * Tuple.java
 *
 * Convenience class for holding multiple values of different types. Has no "equals()" method
 * implementation.
 * Created by Nastja on 12/9/2016.
 */
public class Tuple {

    /**
     * A pair: contains two generic values. You can use {@link android.util.Pair} instead.
     *
     * @param <T1>
     * @param <T2>
     */
    public static class Two<T1, T2> {
        final T1 ITEM_1;
        final T2 ITEM_2;

        public Two(T1 item1, T2 item2) {
            this.ITEM_1 = item1;
            this.ITEM_2 = item2;
        }

        public T1 item1() {
            return this.ITEM_1;
        }

        public T2 item2() {
            return this.ITEM_2;
        }
    }


    /**
     * Triple.
     *
     * @param <T1>
     * @param <T2>
     * @param <T3>
     */
    public static class Three<T1, T2, T3> extends Two<T1, T2>{
        final T3 ITEM_3;

        public Three(T1 item1, T2 item2, T3 item3) {
            super(item1, item2);
            this.ITEM_3 = item3;
        }

        public T3 item3() {
            return this.ITEM_3;
        }
    }


    /**
     * Quadruple.
     *
     * @param <T1>
     * @param <T2>
     * @param <T3>
     * @param <T4>
     */
    public static class Four<T1, T2, T3, T4> extends Three<T1, T2, T3> {
        final T4 ITEM_4;

        public Four(T1 item1, T2 item2, T3 item3, T4 item4) {
            super(item1, item2, item3);
            this.ITEM_4 = item4;
        }

        public T4 item4() {
            return this.ITEM_4;
        }
    }

    /**
     * Five elements.
     *
     * @param <T1>
     * @param <T2>
     * @param <T3>
     * @param <T4>
     * @param <T5>
     */
    public static class Five<T1, T2, T3, T4, T5> extends Four<T1, T2, T3, T4> {
        final T5 ITEM_5;

        public Five(T1 item1, T2 item2, T3 item3, T4 item4, T5 item5) {
            super(item1, item2, item3, item4);
            this.ITEM_5 = item5;
        }

        public T5 item5() {
            return this.ITEM_5;
        }
    }

}

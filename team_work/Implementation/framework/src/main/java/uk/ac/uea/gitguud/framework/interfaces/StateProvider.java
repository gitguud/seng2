package uk.ac.uea.gitguud.framework.interfaces;

/**
 * StateProvider.java
 *
 * Subject of the observer design pattern.
 * Created by Pavel Solodilov on 04/11/16.
 */
public interface StateProvider {

    /**
     * Attaches an observer to this state provider.
     *
     * @param observer Observer to attach.
     */
    void attach(StateObserver observer);

    /**
     * Detaches an observer from the state provider.
     *
     * @param observer Observer to detach.
     */
    void detach(StateObserver observer);

    /**
     * Notifies all of the observers that an update has happened.
     */
    void changeNotify();
}

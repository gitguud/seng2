package uk.ac.uea.gitguud.framework.model;

import android.location.Location;
import android.util.Log;
import android.util.Pair;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import uk.ac.uea.gitguud.framework.convenience.AndroidSpecific;
import uk.ac.uea.gitguud.framework.interfaces.ParseStrategy;

/**
 * JSONToPOIParser.java
 * Provides a facility to parse a JSON string into pairs of a string (category name), and a point of
 * interest.
 *
 * Please note that the category and POI names are case-insensitive. Tags are case-sensitive.
 * As of writing, 24/01/2017, a typical schema looks like this:
 *
 * {
 *     "src": "Source location", <!-- Optional. Source without name gets 'personal' as name. -->
 *     "name": "Name of the source",  <!-- Optional. -->
 *     "categories": {
 *          "restaurants": {   <!-- Name of the category. -->
 *              "Campus Kitchen": { <!-- Name of the POI. -->
 *                  "location": { <!-- The location is mandatory. -->
 *                      "name": "UEA Campus",
 *                      "latitude": 52.621543,
 *                      "longitude": 1.240704
 *                  },
 *                  "description": "Some descriptive text.", <!-- The description is optional. -->
 *                  "coordinates": "The coordinates for the location using the UEA campus map. Just a string."
 *                      <!-- Optional. Note that this is outside the location object. -->
 *              },
 *              "Some Other Restaurant": {
 *                  ...
 *              },
 *              ...
 *          },
 *          "Some other category": {
 *              ...
 *          },
 *          ...
 *     }
 * }
 *
 * Created by Pavel Solodilov on 24/01/17.
 */
public class JSONToPOIParser implements ParseStrategy<Pair<String, ArrayList<PointOfInterest>>> {
    private final String LOG_TAG = AndroidSpecific.getLogTag(this);

    private final static String TAG_SOURCE = "src";
    private final static String TAG_SOURCE_NAME = "name";
    private final static String TAG_CATEGORIES = "categories";

    private final static String TAG_LOCATION = "location";
    private final static String TAG_LOCATION_NAME = "name";
    private final static String TAG_LATITUDE = "latitude";
    private final static String TAG_LONGITUDE = "longitude";

    private final static String TAG_DESCRIPTION = "description";
    private final static String TAG_COORDINATES = "coordinates";

    private Pair<String, String> source = null;

    /**
     * Constructs an array list of pairs, which contains the name of the category and a POI object.
     *
     * @param jsonString JSON string to parse.
     * @return An empty array list if the given string was empty, and null if it was null or
     *         malformed. Returns an array list with pair entries on successful parse.
     */
    @Override
    public ArrayList<Pair<String, ArrayList<PointOfInterest>>> parse(String jsonString) {

        ArrayList<Pair<String, ArrayList<PointOfInterest>>> result = new ArrayList<>();
        if (jsonString != null && jsonString.isEmpty()) return result;

        // Parse the JSON.
        try {
            JSONObject json = new JSONObject(jsonString);
            result.addAll(readCategories(json));
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(LOG_TAG, e.getMessage());
            return null;
        }

        return result;
    }


    /**
     * Reads categories from the JSON object.
     *
     * @param data JSON object to parse.
     * @return Array list of categories containing points of interest.
     * @throws JSONException If any of the required fields were not found.
     */
    private ArrayList<Pair<String, ArrayList<PointOfInterest>>> readCategories(JSONObject data)
        throws JSONException {

        // Try getting the source URL and name.
        String sourceName;
        try {
            sourceName = data.getString(TAG_SOURCE_NAME);
        } catch (JSONException e) {
            sourceName = null;
        }

        String sourceUri;
        try {
            sourceUri = data.getString(TAG_SOURCE);
        } catch (JSONException e) {
            sourceUri = null;
        }

        if (sourceName == null && sourceUri != null) {
            sourceName = "PERSONAL";
        }

        if (sourceUri != null) {
            source = new Pair<>(sourceName, sourceUri);
        }

        // Create the resulting array list.
        ArrayList<Pair<String, ArrayList<PointOfInterest>>> result = new ArrayList<>();

        // Get the category object and names of its contents.
        JSONObject categoriesObject = data.getJSONObject(TAG_CATEGORIES);
        JSONArray categoryNames = categoriesObject.names();

        // The get the categories as array.
        JSONArray categories = categoriesObject.toJSONArray(categoryNames);
        JSONObject category;
        String categoryName;

        // Go through each category, and add all of them into the result.
        for (int i = 0; i < categories.length(); i++) {
            categoryName = categoryNames.getString(i);
            category = categories.getJSONObject(i);

            result.add(new Pair<>(categoryName, readCategoryPOIs(category)));
        }

        return result;
    }


    /**
     * Reads all POIs from a single category.
     *
     * @param categoryJson JSON containing objects from a single category.
     * @return List of all of the POIs in the JSON.
     */
    private ArrayList<PointOfInterest> readCategoryPOIs(JSONObject categoryJson)
            throws JSONException {

        // Get the names of all of the POIs, and then the POIs themselves.
        JSONArray poiNames = categoryJson.names();
        JSONArray pois = categoryJson.toJSONArray(poiNames);

        ArrayList<PointOfInterest> result = new ArrayList<>();

        // Loop through each of the POIs, reading them in.
        JSONObject poi;
        String poiName;
        for (int i = 0; i < pois.length(); i++) {
            poi = pois.getJSONObject(i);
            poiName = poiNames.getString(i);

            result.add(readPOI(poiName, poi));
        }

        return result;
    }

    /**
     * Reads in a single POI object.
     *
     * @param poiName Name of the POI.
     * @param poiJson POI JSON to read.
     * @return POI constructed from the containing information.
     */
    private PointOfInterest readPOI (String poiName, JSONObject poiJson) throws JSONException {

        // Read all of the needed data.
        JSONObject locationJson = poiJson.getJSONObject(TAG_LOCATION);
        String locationName = locationJson.getString(TAG_LOCATION_NAME);
        double latitude = locationJson.getDouble(TAG_LATITUDE);
        double longitude = locationJson.getDouble(TAG_LONGITUDE);

        String description = poiJson.optString(TAG_DESCRIPTION);
        String coordinates = poiJson.optString(TAG_COORDINATES);

        // Location with provider of this class name.
        Location location = new Location(this.getClass().getName());
        location.setLatitude(latitude);
        location.setLongitude(longitude);

        // Make sure that the description and the coordinates are null if empty.
        if (description != null && description.isEmpty()) {
            description = null;
        }

        if (coordinates != null && coordinates.isEmpty()) {
            coordinates = null;
        }

        // Create the POI and set the additional bits: the source and coordinates.
        PointOfInterest poi = new PointOfInterest(poiName, location, locationName, description);
        poi.setCoordinateString(coordinates);
        if (source != null) {
            poi.setSource(source);
        }

        return poi;
    }
}

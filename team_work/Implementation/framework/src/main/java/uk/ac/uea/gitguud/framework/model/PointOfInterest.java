package uk.ac.uea.gitguud.framework.model;

import android.graphics.drawable.Drawable;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Pair;

import uk.ac.uea.gitguud.framework.convenience.ErrorChecking;

import java.util.Comparator;

/**
 * PointOfInterest.java
 *
 * Models a single point of interest: a shop, a cafe, etc.
 * May also be used as a universal data point for bus times, etc.
 * Created by Katrina on 13/11/2016.
 */
public class PointOfInterest {
    /**
     * Source consists of two elements.
     * <br/>- <b>First</b> element is the <b>name</b> of the source.
     * <br/>- <b>Second</b> element is the <b>URL</b> of the source.
     */
    private Pair<String, String> source;
    private Drawable image;

    private String name;
    private String locationString;
    private String description;
    private String coordinateString;
    private Location location;

    /**
     * Constructor. Sets the image and source to null.
     *
     * @param name Name for POI.
     * @param location Location of the POI.
     * @param locationString Name of the location of the POI.
     * @param description Description of the point of interest.
     * @throws Exception if any of the setters' requirements have been violated.
     *         See setters for more details.
     */
    public PointOfInterest(String name, Location location, String locationString,
                           String description) {
        // Define in terms of the full constructor.
        this(name, location, locationString, description, null, null);
    }

    /**
     * Full constructor.
     *
     * @param name Name for POI.
     * @param location Location of the POI.
     * @param locationString Name of the location of the POI.
     * @param description Description of the point of interest.
     * @param source Source where the POI came from (first element is the name, and second one is
     *               the URL).
     * @param image Image of the POI.
     * @throws Exception if any of the setters' requirements have been violated.
     *         See setters for more details.
     */
    public PointOfInterest(String name, Location location, String locationString,
                           String description, Pair<String, String> source, Drawable image) {
        this.setName(name)
                .setLocation(location, locationString)
                .setDescription(description)
                .setSource(source)
                .setImage(image);
    }

    /**
     * Accessor for the source (where the data came from) of the POI.
     *
     * @return Source of the point of interest.
     */
    @Nullable
    public Pair<String, String> getSource() {
        return this.source;
    }

    /**
     * Accessor for the image of the POI.
     *
     * @return Image of the entry if present.
     */
    @Nullable
    public Drawable getImage() {
        return this.image;
    }

    /**
     * Accessor for the name of the POI.
     *
     * @return Name of the POI.
     */
    @NonNull
    public String getName() {
        return this.name;
    }

    /**
     * Accessor for the location of the POI.
     * <p>
     * Returns a copy of the corresponding object, and therefore any changes to it will not affect
     * the container.
     *
     * @return Location of the POI.
     */
    @NonNull
    public Location getLocation() {
        return new Location(this.location);
    }

    /**
     * Location string accessor.
     *
     * @return Location string of this POI.
     */
    @Nullable
    public String getLocationString() {
        return this.locationString;
    }


    /**
     * Description accessor.
     *
     * @return Description of the point of interest.
     */
    @Nullable
    public String getDescription() {
        return this.description;
    }


    /**
     * UEA map coordinates accessor.
     *
     * @return Coordinates on the UEA map.
     */
    @Nullable
    public String getCoordinateString() {
        return this.coordinateString;
    }

    /**
     * Setter for the source of the POI.
     *
     * @param source Source of the entry. The first element of the pair is the name of the source,
     *               and the second one is the URL or some other type of source string
     *               (e.g. storage path). Might be null for no path.
     * @return This object for chaining.
     */
    public PointOfInterest setSource(Pair<String, String> source) {
        this.source = source;
        return this;
    }

    /**
     * Setter for the image of the POI.
     *
     * @param image Image to associate with the entry. Leave as null for no image.
     * @return This object for chaining.
     */
    public PointOfInterest setImage(Drawable image) {
        this.image = image;
        return this;
    }

    /**
     * Sets the name for a point of interest.
     *
     * @param name New name.
     * @return This point of interest for chaining.
     * @throws NullPointerException if the provided name is null.
     * @throws IllegalArgumentException if the provided name is an empty string.
     */
    @NonNull
    public PointOfInterest setName(String name) throws NullPointerException {
        ErrorChecking.getInstance()
                .throwOnNull(name, "The name cannot be null.")
                .throwOnEmptyString(name, "The name cannot be an empty string.");

        this.name = name;
        return this;
    }

    /**
     * Sets a new location of the point of interest.
     * <p>
     * The provided Location object will be copied, and therefore any modifications to the POI
     * from outside of the object are impossible.
     *
     * @param location New location.
     * @param locationString Address new location. Might be null if there is nothing to add.
     * @return This point of interest for chaining.
     * @throws NullPointerException if the location object is null.
     */
    @NonNull
    public PointOfInterest setLocation(Location location, String locationString) {

        ErrorChecking.getInstance().throwOnNull(location, "The location cannot be null.");

        this.location = new Location(location);
        this.locationString = locationString;
        return this;
    }


    /**
     * Sets a description of the POI.
     *
     * @param description Description of the point of interest. May be null.
     * @return This point of interest for chaining.
     */
    @NonNull
    public PointOfInterest setDescription(String description) {
        this.description = description;
        return this;
    }

    /**
     * Sets the UEA map coordinate string of the POI.
     *
     * @param coordinateString New UEA map coordinates.
     * @return This object for chaining.
     */
    @NonNull
    public PointOfInterest setCoordinateString(String coordinateString) {
        this.coordinateString = coordinateString;
        return this;
    }

    /**
     * Updates the data within this point of interest with another one's. All of the internal
     * objects except for the name are going to be replaced, including the picture and the location.
     * When it comes to the source, only the name of the source is going to be replaced (first
     * element of the source pair).
     *
     * @param other Other point of interest to get data from. Does nothing if the provided other
     *              point of interest is null or is a reference pointing to this (same) object; e.g.
     *              do not copy from self.
     * @return This object for chaining.
     */
    @NonNull
    public PointOfInterest updateWith(PointOfInterest other) {

        // Do nothing on null or same reference.
        if (other == null || other == this) {
            return this;
        }

        // Set basic variables.
        this.setLocation(other.location, other.locationString)
                .setDescription(other.description)
                .setImage(other.image)
                .setCoordinateString(other.coordinateString);

        // Perform error checking before assigning the new source value.
        Pair<String, String> srcThis = this.getSource();
        Pair<String, String> srcOther = other.getSource();

        // Only reset the source name if the sources are present and the names are not equal.
        if (srcOther != null && srcThis != null && !srcThis.first.equals(srcOther.first)) {
            this.setSource(new Pair<>(srcOther.first, srcThis.second));
        }

        return this;
    }


    /**
     * A point of interest is considered unique if it has a unique name and location string
     * combination.
     *
     * @return Hash code of the point of interest.
     */
    @Override
    public int hashCode() {
        return (this.getName() + this.getLocationString()).hashCode();
    }

    /**
     * Compares two points of interest. They are equal if their names match (case-insensitive), and
     * so do their source URIs, if present.
     *
     * @param other Another POI to compare this one to. Will throw a {@link ClassCastException} if
     *              the provided object was not of type PointOfInterest.
     * @return True if the POIs are equal, and false if not.
     */
    @Override
    public boolean equals(Object other) {
        PointOfInterest otherPOI = (PointOfInterest) other;

        Pair<String, String> srcThis = this.getSource();
        Pair<String, String> srcOther = otherPOI.getSource();


        if (srcThis == srcOther) {
            // If the sources are equal (most probably null), check if their names match.
            return this.getName().equalsIgnoreCase(otherPOI.getName());
        } else if (srcThis != null && srcOther != null && srcThis.second.equals(srcOther.second)) {
            // If the sources are both non-null, and their URLs are equal, also check if their names
            // match.
            return this.getName().equalsIgnoreCase(otherPOI.getName());
        }

        // If the source URLs were not equal, return false as these were not equal.
        return false;
    }

    /**
     * Useful for debugging.
     *
     * @return String representing the POI.
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("POI ");
        sb.append(this.name);

        if (this.locationString != null) {
            sb.append(" located at ");
            sb.append(this.locationString);
        }

        sb.append(" (");
        sb.append(this.location.getLatitude());
        sb.append(", ");
        sb.append(this.location.getLongitude());

        if (this.coordinateString != null) {
            sb.append(", UEA Map: ");
            sb.append(this.coordinateString);
        }

        sb.append(").\n");

        if (this.description != null) {
            sb.append("Description: ");
            sb.append(this.description);
        }

        return sb.toString();
    }

    /**
     * Comparator static inner class. Compares points of interest by name (case-insensitive).
     */
    public static class CompareByName implements Comparator<PointOfInterest> {

        @Override
        public int compare(PointOfInterest poi1, PointOfInterest poi2) {
            return poi1.getName().toUpperCase().compareTo(poi2.getName().toUpperCase());
        }
    }
}

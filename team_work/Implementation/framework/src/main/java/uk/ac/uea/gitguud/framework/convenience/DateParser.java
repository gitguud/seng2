package uk.ac.uea.gitguud.framework.convenience;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * DateParser.java
 *
 * Contains utilities to parse dates.
 * Created by Pavel Solodilov on 21/11/16.
 */
public class DateParser {
    private static String iso8601LongFmt = "yyyy-MM-dd'T'HH:mm:ss'Z'";
    private static String iso8601ShortFmt = "yyyyMMdd'T'HHmmss'Z'";
    private static CharSequence iso8601LongFmtLocator = "-";

    /**
     * Converts an ISO8601 date to a localised date.
     * Allowed formats are (21st November 2016, 12:02:30 UTC).
     *  2016-11-21T12:02:30+00.00
     *  2016-11-21T12:02:30Z
     *  20161121T120230Z
     *
     * @param dateString Date string to convert.
     * @param locale Locale to convert into.
     * @return Converted date.
     * @throws ParseException if the date was in incorrect format.
     */
    public static Date toISO8601(String dateString, Locale locale) throws ParseException {

        // Convert the ending '+00.00' into 'Z' to simplify parsing.
        if (dateString.endsWith("+00.00")) {
            dateString = dateString.substring(0, dateString.length() - 6) + "Z";
        }

        // If the string is in the long 8601 format, use the long one.
        /// Otherwise use the short one.
        SimpleDateFormat fmt;
        if (dateString.contains(iso8601LongFmtLocator)) {
            fmt = new SimpleDateFormat(iso8601LongFmt, locale);
        } else {
            fmt = new SimpleDateFormat(iso8601ShortFmt, locale);
        }

        // Parse and return the date.
        return fmt.parse(dateString);
    }
}

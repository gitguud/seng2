package uk.ac.uea.gitguud.framework.interfaces;

import java.util.ArrayList;

/**
 * ParseStrategy.java
 *
 * This interface provides access to data parsing facilities.
 *
 * @param <T> Type of data to parse the string to.
 * Created by Pavel Solodilov on 05/11/16.
 */
public interface ParseStrategy<T> {

    /**
     * Parses string into a given object type.
     *
     * @param data Data to parse the T object from. Assumed never to be null.
     * @return List of objects from parsed data. Should be null on malformatted data.
     */
    ArrayList<T> parse(String data);
}

package uk.ac.uea.gitguud.framework.interfaces;

import android.graphics.drawable.Drawable;

/**
 * ExpandableListAdapterInfoProvider.java
 *
 * This interface is used to provide various information to an expandable list view adapter.
 * Created by Pavel Solodilov on 26/11/16.
 */
public interface ExpandableListAdapterResourceProvider {

    /**
     * Accessor for a group's child whole layout ID.
     *
     * @return The group's child layout ID that can be used to access the child's layout.
     */
    int childLayoutId();

    /**
     * Accessor for a group's layout ID.
     *
     * @return The group's layout ID that can be used to access the group's layout.
     */
    int groupLayoutId();

    /**
     * Accessor for a group's child text view ID
     *
     * @return The group's child text view ID.
     */
    int childTextViewId();


    /**
     * Accessor for a group's indicator image view ID.
     *
     * @return The group's image view ID; the image view must contain the group indicator image.
     */
    int groupIndicatorImageViewId();

    /**
     * Accessor for a group's header text view ID
     *
     * @return The group's header text view ID.
     */
    int groupTextViewId();

    /**
     * Accessor for the group layout's background colour.
     *
     * @return The colour corresponding to group's background colour.
     */
    int groupBgColour();

    /**
     * Accessor for the group layout's alternating background colour. This colour is going to be
     * used with the group's background colour in order to create an alternating pattern, making
     * the individual group headers easier to discern.
     *
     * @return The colour corresponding to group's alternating background colour.
     */
    int altBgColour();

    /**
     * Colour of the indicator to set.
     *
     * @return Colour of the indicator.
     */
    int indicatorColour();

    /**
     * Image to set as expanded group indicator.
     *
     * @return Expanded indicator. Null if not set.
     */
    Drawable expandedGroupIndicator();

    /**
     * Image to set as collapsed group indicator.
     *
     * @return Collapsed indicator. Null if not set.
     */
    Drawable collapsedGroupIndicator();

    /**
     * Image to set for empty group indicator.
     *
     * @return Empty indicator. Null if not set.
     */
    Drawable emptyGroupIndicator();
}

package uk.ac.uea.gitguud.framework.interfaces;

/**
 * ViewTextProvider.java
 * Simply allows an object to show its textual representation; analog to toString().
 *
 * Created by Pavel Solodilov on 25/11/16.
 */
public interface ViewTextProvider {
    String getText();
}

package uk.ac.uea.gitguud.framework;

import android.Manifest;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import uk.ac.uea.gitguud.framework.convenience.AndroidSpecific;
import uk.ac.uea.gitguud.framework.interfaces.FileIO;

/**
 * AndroidFileIO.java
 * <p>
 * This class, implementing the FileIO interface, provides a uniform method to access different
 * storage resources and assets.
 * <p>
 * It essentially encapsulates all of the Android file access methods.
 */
public class AndroidFileIO implements FileIO {

    private static AndroidFileIO instance;

    //the storage paths are taken using the Environment class
    //for both the internal storage and external storage
    private String externalStoragePath;
    private String internalStoragePath;

    //we are using internal storage by default
    private boolean useExternal = false;

    private AndroidFileIO() {
        this.externalStoragePath = Environment.getExternalStorageDirectory().getAbsolutePath();
        this.internalStoragePath = Environment.getDataDirectory().getAbsolutePath();
    }

    /**
     * Accessor for an instance of the AndroidFileIO object.
     * Uses internal storage by default.
     *
     * @return Instance of the object.
     */
    public static AndroidFileIO getInstance() {
        if (AndroidFileIO.instance == null) {
            AndroidFileIO.instance = new AndroidFileIO();
        }
        return AndroidFileIO.instance;
    }


    /**
     * Sets the file IO to use internal storage.
     *
     * @return This object.
     */
    public AndroidFileIO useInternal() {
        this.useExternal = false;
        return this;
    }

    /**
     * Sets the file IO to use external storage.
     *
     * @return This object.
     */
    public AndroidFileIO useExternal() {
        this.useExternal = true;
        return this;
    }


    /**
     * Currently always uses internal storage.
     *
     * @param dirname
     * @param file
     * @param image
     * @param context
     * @return
     */
    public void saveImageToStorageAsync(final String dirname, final String file, final Bitmap image,
                                        final Context context) {

        new Thread(new Runnable() {
            @Override
            public void run() {
                ContextWrapper cw = new ContextWrapper(context);

                // Get the path to write into and create it.
                File dir = cw.getDir(dirname, Context.MODE_PRIVATE);
                File path = new File(dir, file);

                FileOutputStream fos = null;

                try {
                    // Compress the image into the needed file.
                    fos = new FileOutputStream(path);
                    image.compress(Bitmap.CompressFormat.JPEG, 100, fos);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                    return;
                } finally {
                    try {
                        fos.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                        return;
                    }
                }
            }
        }).start();

        return;
    }

    /**
     * Currently only uses internal storage.
     *
     * @param dirname
     * @param file
     * @param context
     * @return
     */
    public Bitmap loadImageFromStorage(String dirname, String file, Context context) {
        ContextWrapper cw = new ContextWrapper(context);

        File dir = cw.getDir(dirname, Context.MODE_PRIVATE);
        File path = new File(dir, file);

        try {
            return BitmapFactory.decodeStream(new FileInputStream(path));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Reads a file from internal or external storage and returns the result as a string.
     *
     * @param fileName Name of the file to load.
     * @param context  The Context for the file to be opened in.
     * @return String representing the string read from the file and null if any of the
     *                parameters are null or empty, or if the string is empty.
     */
    @Override
    public String readFile(String fileName, Context context) {
        if (fileName == null || fileName.equals("") || context == null)
            return null;

        StringBuilder text = new StringBuilder();
        String retStr;
        String line;
        try {
            BufferedReader reader;
            if (!this.useExternal) {
                InputStream is = context.openFileInput(fileName);
                InputStreamReader isr = new InputStreamReader(is);
                reader = new BufferedReader(isr);
                while ((line = reader.readLine()) != null) {
                    text.append(line);
                    text.append("\n");
                }
                reader.close();
                isr.close();
                is.close();
            } else {
                // Assume thisActivity is the current activity
                File file = new File(this.externalStoragePath, fileName);
                reader = new BufferedReader(new FileReader(file));
                while ((line = reader.readLine()) != null) {
                    text.append(line).append("\n");
                }
                reader.close();
            }



            retStr = text.toString();
            if(retStr.endsWith("\n"))
                retStr = retStr.substring(0,retStr.length()-1);

        } catch (FileNotFoundException e) {
            Log.e(AndroidSpecific.getLogTag(this), e.getMessage());
            return null;
        } catch (IOException e) {
            Log.e(AndroidSpecific.getLogTag(this), e.getMessage());
            return null;
        }

        if (retStr == null || retStr.isEmpty())
            return null;

        return retStr;

    }

    /**
     * Allows to write to a file in external or internal memory with specified name, creating
     * it if not existent, and returns true if the operation was successful and false otherwise.
     *
     * @param fileName Name of the file to access.
     * @param contents The contents to be written to the file.
     * @param context  The Context for the file to be opened in.
     * @return true if the file was successfully written to and false otherwise. Also, the method
     * returns false if any of the parameters are null or empty or of you do not have permission.
     * @throws FileNotFoundException If the output stream cannot be opened for the given file for
     *                               any reason.
     * @throws IOException           If the requested file was not found and could not be created,
     *                               or could not be accessed.
     */
    @Override
    public boolean writeFile(String fileName, String contents, Context context) {

        if (fileName == null || contents == null || fileName.equals("") || contents.equals(""))
            return false;

        try {
            if (!this.useExternal) {
                OutputStreamWriter writer = new OutputStreamWriter(
                        context.openFileOutput(fileName, Context.MODE_PRIVATE));
                writer.append(contents);
                writer.close();
            } else {
                if(ContextCompat.checkSelfPermission(context,
                        Manifest.permission.WRITE_CALENDAR)!=PackageManager.PERMISSION_GRANTED){
                        return false;
                }
                File file = new File(this.externalStoragePath, fileName);
                FileOutputStream fos = new FileOutputStream(file);
                OutputStreamWriter writer = new OutputStreamWriter(fos);
                writer.append(contents);
                writer.close();
            }
            return true;

        } catch (FileNotFoundException e) {
            Log.e(AndroidSpecific.getLogTag(this), e.getMessage());
            return false;
        } catch (IOException e) {
            Log.e(AndroidSpecific.getLogTag(this), e.getMessage());
            return false;
        }
    }


    /**
     * Allows to get access to the shared preferences file of this application.
     *
     * @param context Context of the app file preferences of which to get.
     * @return Shared preferences handler for this application context.
     */
    public SharedPreferences getSharedPref(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }
}

package uk.ac.uea.gitguud.framework.view;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import uk.ac.uea.gitguud.framework.convenience.ErrorChecking;
import uk.ac.uea.gitguud.framework.interfaces.ExpandableListAdapterResourceProvider;
import uk.ac.uea.gitguud.framework.interfaces.ViewTextProvider;

/**
 * GuudExpandableListAdapter.java
 *
 * A custom ExpandableListView adapter that gives access to some custom functionality.
 * In order to use it, you should have an activity of roughly the following structure:
 * <ActivityLayout XML_FILE_1:ACTIVITY>
 *     <ExpandableListView android:groupIndicator="@Null" --THIS IS IMPORTANT-- >
 *          <RelativeLayout XML_FILE_2:GROUPS>
 *              <TextView/>
 *              <ImageView/>
 *              <inside>
 *                      <ChildrenLayout>
 *                      <TextView XML_FILE_3:GROUP_CHILDREN />
 *                      </ChildrenLayout>
 *                         ... As many of these insides, children, as you need.
 *              <inside/>
 *          <RelativeLayout/>
 *             ... As many of these relative layouts, group headers, as you need.
 *     </ExpandableListView>
 * <ActivityLayout/>
 * Created by Pavel Solodilov on 25/11/16.
 */
public class GuudExpandableListAdapter<T extends ViewTextProvider, Y extends ViewTextProvider>
        extends BaseExpandableListAdapter {
    private Context context;
    private List<T> groups;
    private Hashtable<String, List<Y>> children;
    private ExpandableListAdapterResourceProvider r;

    /**
     * Constructor.
     *
     * @param context Context of the app.
     * @param groups List of names of groups.
     * @param children Group children.
     * @param r Resource provider for colours and various bits of views inside the expandable list
     *          view.
     * @throws Exception As provided in the setters.
     */
    public GuudExpandableListAdapter(Context context, List<T> groups,
                                     Hashtable<String, List<Y>> children,
                                     ExpandableListAdapterResourceProvider r) {
        this.setContext(context).setData(groups, children).setResourceProvider(r);
    }


    /**
     * Sets the resource provider for the adapter.
     *
     * @param r New resource provider.
     * @return This object.
     * @throws NullPointerException if the provided resource provider is null.
     */
    public GuudExpandableListAdapter setResourceProvider(ExpandableListAdapterResourceProvider r) {
        ErrorChecking.getInstance().throwOnNull(r, "The resource provider cannot be null.");

        this.r = r;
        return this;
    }


    /**
     * Sets the context of the adapter.
     *
     * @param context New context.
     * @return This object.
     * @throws NullPointerException if the provided context is null.
     */
    public GuudExpandableListAdapter setContext(Context context) {
        ErrorChecking.getInstance().throwOnNull(context, "The context cannot be null.");

        this.context = context;
        return this;
    }


    /**
     * Sets that the expandable list view is supposed to access.
     * Is is assumed that the group headers match the key objects that point at children lists in
     * the provided children hashtable.
     *
     * @param groups Group objects. Assumed to already be sorted into correct order. If null, will
     *               be initialised to an empty list.
     * @param children Hashtable that consists of group headers as keys and a list of children
     *                 objects as values. The children objects are assumed to already be sorted
     *                 into correct order. If null, will be initialised to an empty hashtable.
     * @return This object.
     * @throws IllegalArgumentException If only one of the parameters is null, and the other one
     *                                  is not.
     */
    public GuudExpandableListAdapter setData(List<T> groups,
                                             Hashtable<String, List<Y>> children) {
        if (groups == null && children == null) {
            this.groups = new ArrayList<>();
            this.children = new Hashtable<>();
        } else if (groups != null && children != null) {
            this.groups = groups;
            this.children = children;
        } else {
            throw new IllegalArgumentException("Both the groups list and the children hashtable " +
                    "have to be either null or not null.");
        }

        return this;
    }


    /**
     * Inflates the provided ID into a view, and then into the provided ViewGroup.
     *
     * @param id ID to inflate into view.
     * @param group Group to inflate into. Leave null if not needed.
     * @return Newly inflated view.
     */
    private View inflate(int id, ViewGroup group) {
        LayoutInflater inflater =
                (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        return inflater.inflate(id, group);
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        // Retrieve the child text.
        final String childText = (String)getChild(groupPosition, childPosition);

        // If the child view was null, inflate a new one.
        if (convertView == null) {
            convertView = this.inflate(this.r.childLayoutId(), null);
        }

        // Get the the child text view and set its text.
        ((TextView) convertView.findViewById(this.r.childTextViewId())).setText(childText);
        return convertView;
    }


    /**
     * Stylises the group headers depending on their alternating colours.
     *
     * @param v View to change the background of.
     * @param pos Position (starting from zero): if odd, will use the alternative colour.
     * @return This object.
     */
    private GuudExpandableListAdapter styliseGroupHeaders(View v, int pos) {

        // The set colour depends on the group position. If odd (indexing starts at 0),
        // set the main colour; otherwise set the alternating one.
        if (pos % 2 == 0) {
            v.setBackgroundColor(this.r.groupBgColour());
        } else {
            v.setBackgroundColor(this.r.altBgColour());
        }

        return this;
    }


    /**
     * Stylises the group indicator.
     *
     * @param expanded If true, sets the view to use the expanded indicator, if provided, and other
     *                 wise sets it to use the collapsed indicator.
     * @param v View to change the indicator of.
     * @param key Key to search the children hashtable and see whether the view has no children. If
     *            this has been provided, and the view has no children, sets the indicator to the
     *            empty one, if provided. This key check overrides the expanded check, so an empty
     *            view will still show the empty indicator no matter if it is expanded or not.
     * @return This object.
     */
    private GuudExpandableListAdapter styliseGroupIndicator(boolean expanded, View v, String key) {
        // Select the group indicator image;
        Drawable img;
        if (key != null && this.children.get(key).isEmpty()) {
            img = this.r.emptyGroupIndicator();
        } else if (expanded) {
            img = this.r.expandedGroupIndicator();
        } else {
            img = this.r.collapsedGroupIndicator();
        }

        // Set the group indicator image if present.
        if (img != null) {

            // Colour the indicator.
            // TODO this works with black things only.
            img.setColorFilter(this.r.indicatorColour(), PorterDuff.Mode.SRC_ATOP);

            // Retrieve the image view and set the indicator.
            ((ImageView) v.findViewById(this.r.groupIndicatorImageViewId())).setImageDrawable(img);
        }

        return this;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {

        // Get the header title.
        String headerTitle = (String) getGroup(groupPosition);


        // If the group view was null, inflate a new one.
        if (convertView == null) {
            convertView = this.inflate(this.r.groupLayoutId(), null);
        }

        // Stylise the group headers and the indicator.
        this.styliseGroupHeaders(convertView, groupPosition)
               .styliseGroupIndicator(isExpanded, convertView, headerTitle);

        // Get the text view of the group header, and set its text.
        ((TextView) convertView.findViewById(this.r.groupTextViewId())).setText(headerTitle);

        return convertView;
    }


    /**
     * Gets a conrete object of the child.
     *
     * @param groupPosition Position of the group.
     * @param childPosititon Position of the child.
     * @return Child at given positions.
     */
    public Y getChildConcrete(int groupPosition, int childPosititon) {
        String groupHeader = this.groups.get(groupPosition).getText();
        return this.children.get(groupHeader).get(childPosititon);
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        // Retrieves a child by the group position and child position.
        String groupHeader = this.groups.get(groupPosition).getText();
        return this.children.get(groupHeader).get(childPosititon).getText();
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        String groupHeader = this.groups.get(groupPosition).getText();
        return this.children.get(groupHeader).size();
    }

    /**
     * Gets a concrete object of the group.
     *
     * @param groupPosition Position of the group.
     * @return Group at given position.
     */
    public T getGroupConcrete(int groupPosition) {
        return this.groups.get(groupPosition);
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this.groups.get(groupPosition).getText();
    }

    @Override
    public int getGroupCount() {
        return this.groups.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}

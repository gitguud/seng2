package uk.ac.uea.gitguud.framework.interfaces;

import android.content.Context;
import android.content.SharedPreferences;

import java.io.IOException;

public interface FileIO {
    String readFile(String file, Context context) throws IOException;

    boolean writeFile(String file, String contents, Context context) throws IOException;

    SharedPreferences getSharedPref(Context context);
}

package uk.ac.uea.gitguud.framework.convenience;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * SupportFuncs.java
 * Contains common simple functions for different operations.
 *
 * Created by Pavel Solodilov on 25/11/16.
 */
public class SupportFuncs {

    // TODO set locale in some other way.
    private static SimpleDateFormat ddMMyyyy = new SimpleDateFormat("ddMMyyyy", Locale.UK);
    private static SimpleDateFormat dd_MMMM_yyyy = new SimpleDateFormat("dd MMMM yyyy", Locale.UK);
    private static SimpleDateFormat hh_mm = new SimpleDateFormat("HH:mm", Locale.UK);

    /**
     * Compares two dates on basis of their day, month and year. So, 20th November 2016 13:30:26
     * is equal to 20th November 2016 18:12:24, but not equal to any other date.
     *
     * @param d1 First date.
     * @param d2 Second date.
     * @return True if the dates happen on the same day.
     */
    public static boolean sameDay(Date d1, Date d2) {
        String d1String = ddMMyyyy.format(d1);
        String d2String = ddMMyyyy.format(d2);

        return d1String.equals(d2String);
    }

    /**
     * Formats a date to string. Format (e.g. 26th Nov 2016): 26 November 2016.
     *
     * @param date Date to format.
     * @return Formatted string.
     */
    public static String dateToString(Date date) {
        return dd_MMMM_yyyy.format(date);
    }

    /**
     * Formats a date to time string. Format HH:mm.
     *
     * @param date Date to format.
     * @return Formatted string.
     */
    public static String dateToTime(Date date) {
        return hh_mm.format(date);
    }

    /**
     * Formats a time span to string. Format HH:mm-HH:mm.
     *
     * @param span Time span to format.
     * @return Formatted string.
     */
    public static String timespanToTime(TimeSpan span) {
        return hh_mm.format(span.getStartTime()) + "-" + hh_mm.format(span.getEndTime());
    }

    /**
     * This enum models different time values.
     */
    public enum TimeValue {
        NONE              ("Never/Right before",   0),
        FIVE_MINUTES      ("5 Minutes",  300),
        TEN_MINUTES       ("10 Minutes", 600),
        THIRTY_MINUTES    ("30 Minutes", 1800),
        ONE_HOUR          ("1 Hour",     3600),
        TWELVE_HOURS      ("12 Hours",   43200),
        TWENTY_FOUR_HOURS ("24 Hours",   86400);

        public String string;
        public final int SECONDS;

        /**
         * Constructor.
         *
         * @param string String that represents the enum.
         * @param seconds Amount of seconds in an enum time value representation.
         */
        TimeValue (String string, int seconds) {
            this.string  = string;
            this.SECONDS = seconds;
        }

        /**
         * Returns index of enum by its value.
         *
         * @param seconds Amount of seconds to find.
         * @return Index of the enum that contains these seconds.
         */
        public static int indexByValue(int seconds) {
            TimeValue[] values = TimeValue.values();
            for (int i = 0; i < values.length; i++) {
                if (values[i].SECONDS == seconds) {
                    return i;
                }
            }
            return -1;
        }

        /**
         * Simply returns the STRING representation of the enum.
         *
         * @return String representation of the enum.
         */
        @Override
        public String toString() {
            return this.string;
        }
    }
}

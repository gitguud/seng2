package uk.ac.uea.gitguud.framework.convenience;

import java.util.Date;

/**
 * ErrorChecking.java
 *
 * Contains a set of useful error checking functions. Allows chaining of methods, but cannot be
 * instantiated.
 * Created by Pavel Solodilov on 05/11/16.
 */
public class ErrorChecking {

    private static ErrorChecking instance;

    /**
     * Private constructor for singleton pattern.
     */
    private ErrorChecking() {
    }

    /**
     * Singleton pattern accessor.
     *
     * @return Instance of an ErrorChecking class.
     */
    public static ErrorChecking getInstance() {
        if (ErrorChecking.instance == null) {
            ErrorChecking.instance = new ErrorChecking();
        }

        return ErrorChecking.instance;
    }

    /**
     * Checks whether the start date is before the end date.
     *
     * @param startDate Start date to check against.
     * @param endDate End date to check against.
     * @param equalAccepted True if start and end dates are allowed to be equal, e.g. the same date.
     * @return True if start date is before the end date, and false otherwise.
     */
    public static boolean startDateBeforeEndDate(Date startDate, Date endDate,
                                                 boolean equalAccepted) {
        long difference = endDate.getTime() - startDate.getTime();

        if (equalAccepted) {
            return difference >= 0;
        }

        return difference > 0;
    }


    /**
     * Throws a NullPointerException with provided message if the given object is null.
     * Assumes that the message is never null.
     *
     * @param obj Object to check if null.
     * @param msg Message to set for the exception.
     * @return This singleton; for chaining.
     */
    public ErrorChecking throwOnNull(Object obj, String msg) {
        if (obj == null) {
            throw new NullPointerException(msg);
        }

        return ErrorChecking.getInstance();
    }

    /**
     * Throws a NullPointerException with provided message if the given string is an empty string.
     * Assumes that the message and the provided string are never null.
     *
     * @param str String to check if empty.
     * @param msg Message to set for the exception.
     * @return This singleton; for chaining.
     */
    public ErrorChecking throwOnEmptyString(String str, String msg) {
        if (str.equals("")) {
            throw new IllegalArgumentException(msg);
        }

        return ErrorChecking.getInstance();
    }
}

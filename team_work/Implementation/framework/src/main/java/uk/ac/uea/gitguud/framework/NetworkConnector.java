package uk.ac.uea.gitguud.framework;

import android.content.Context;
import android.util.Log;

import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.Response;

import java.nio.charset.Charset;

/**
 * NetworkConnector.java
 *
 * Used to provide default way of network communication for Android applications.
 * Created by Pavel Solodilov on 07/11/16.
 */
public class NetworkConnector {
    private static NetworkConnector instance;

    /**
     * Default constructor.
     */
    private NetworkConnector() {

    }

    /**
     * Accessor for an instance of an object; singleton pattern.
     *
     * @return Instance of an object.
     */
    public static NetworkConnector getInstance() {
        if (NetworkConnector.instance == null) {
            NetworkConnector.instance = new NetworkConnector();
        }

        return NetworkConnector.instance;
    }

    /**
     * Asynchronously fetches all of the data from a uri.
     *
     * @param context Context of the app.
     * @param uri URI to fetch the data from.
     * @param callback Object to process a onSync with data. The onSync will receive a response
     *                 object to deal with errors outside the network connection. The exception bit
     *                 of the onSync will be null if everything went fine, and not null if
     *                 something went wrong.
     */
    public void fetchFromURI(Context context, String uri,
                             FutureCallback<Response<String>> callback) {
        Ion.with(context)                            // Use the given context.
                .load(uri)                           // Load the given URI.
                .setLogging("LOG/ION", Log.DEBUG)    // Set up logging.
                .asString(Charset.defaultCharset())  // Retrieve the result as string.
                .withResponse()                      // Include response headers.
                .setCallback(callback);              // Use the given onSync.
    }
}

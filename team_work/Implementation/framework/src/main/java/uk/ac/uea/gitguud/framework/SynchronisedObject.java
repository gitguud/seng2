package uk.ac.uea.gitguud.framework;

import java.util.ArrayList;

import uk.ac.uea.gitguud.framework.interfaces.StateObserver;
import uk.ac.uea.gitguud.framework.interfaces.StateProvider;

/**
 * SynchronisedObject.java
 * Implementation of observer pattern's provider.
 *
 * @param <T> Type of object that the object provider.
 * Created by Pavel Solodilov on 04/11/16.
 */
public abstract class SynchronisedObject<T> implements StateProvider {
    private final ArrayList<StateObserver> STATE_OBSERVERS;

    /**
     * Default initialisation constructor.
     */
    public SynchronisedObject() {
        this.STATE_OBSERVERS = new ArrayList<>();
    }

    /**
     * {@link StateProvider#attach(StateObserver)}
     */
    public void attach(StateObserver observer) {
        this.STATE_OBSERVERS.add(observer);
    }

    /**
     * {@link StateProvider#detach(StateObserver)}
     */
    public void detach(StateObserver observer) {
        this.STATE_OBSERVERS.remove(observer);
    }

    /**
     * {@link StateProvider#changeNotify()}
     */
    public void changeNotify() {
        for (StateObserver observer : this.STATE_OBSERVERS) {
            observer.updateNotify();
        }
    }

    /**
     * Update the entries of the object with new values.
     *
     * @param newEntries New entries to use.
     */
    public abstract void updateEntries(ArrayList<T> newEntries);
}

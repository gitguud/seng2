package uk.ac.uea.gitguud.framework;

import android.content.Context;
import android.util.Log;

import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Response;

import java.util.ArrayList;

import uk.ac.uea.gitguud.framework.convenience.AndroidSpecific;
import uk.ac.uea.gitguud.framework.convenience.SupportFuncs;
import uk.ac.uea.gitguud.framework.interfaces.ParseStrategy;
import uk.ac.uea.gitguud.framework.interfaces.SyncCallback;

/**
 * Synchroniser.java
 *
 * This class operates as a facade for the network and data storage.
 *
 * Created by Pavel Solodilov on 05/11/16.
 */
public class Synchroniser {
    private static Synchroniser instance;

    /**
     * Constructor for an instance.
     */
    private Synchroniser () {
    }

    /**
     * Provides access to the synchroniser.
     *
     * @return Sychroniser.
     */
    public static Synchroniser getInstance() {

        // Singleton pattern implementation.
        if (Synchroniser.instance == null) {
            Synchroniser.instance = new Synchroniser();
        }

        return Synchroniser.instance;
    }

    /**
     * This method is used to download data from network and save it to offline storage, or load it
     * from offline storage. It also parses and passes the parsed data to the object it is ought to
     * update.
     * <p>
     * This method can operate in multiple modes, and therefore simplifies the operation of saving
     * and retrieving data. It is a facade method covering complex operations within the framework.
     * <p>
     * Overall, the relation of bits is like this:
     * 1) URI -> Load data from network -> GOT data -> context and filename   -> SAVE data -> goto 3
     *                                                 NO context or filename -> goto 3
     * 2) NO URI -> Load data from storage -> context and filename   -> GOT data -> goto 3
     *                                        NO context or filename -> NO data -> done
     * 3) GOT data -> parser and target   -> parse and pass -> done
     *                NO parser or target -> done
     * <p>
     * <h3>Supported operations and their requirements</h3>
     * Load from network - save to offline storage - parse: uri, context, filename, parser, target
     * Load from network - do not save to offline storage - parse: uri, NULL, NULL, parser, target
     * Load from network - save to offline storage: uri, context, filename, NULL, NULL
     * Load from offline storage - parse: NULL, context, filename, parser, target
     * Other operations are not supported, and will simply return without doing anything.
     *
     * @param uri Network URI to load the data from. If it is provided, the method will download
     *            data from this URI, and save it to offline storage. Then it will parse it and pass
     *            to target object. If it is null or an empty string, data will be loaded from
     *            offline storage.
     * @param context Context of the app. This is used to access correct part of device storage. If
     *                it is null, no loading or saving the data from or onto offline storage will
     *                happen.
     * @param filename Name of the file to save the data into. If it is null or an empty string, the
     *                 data will not be loaded or stored to/from offline storage.
     * @param parser Parser that is to parse the data and construct a type of object that the target
     *               can accept. If it is null, or no data was retrieved, the method stops execution
     *               and does not pass any data to the target.
     * @param target Target object to pass the parsed data to. If it is null, no data will be parsed
     *               by the parser.
     * @param callbackable Object that will get notified of the status of the synchronisation. The
     *                     type of error will depend on the failed or succeeded operation. The
     *                     second parameter passed will offer explanation for the error or success
     *                     factor, but may be null. If this is null, will not perform callbacks.
     * @param <T> Type of object the data is to be parsed and converted into for the target to use.
     */
    public <T> void synchronise(String uri, Context context, String filename,
                                ParseStrategy<T> parser, SynchronisedObject<T> target,
                                SyncCallback callbackable) {
        new SynchronisationTask<T>(uri, context, filename, parser, target, callbackable).perform();
    } // end synchronise


    public class SynchronisationTask<T> implements FutureCallback<Response<String>> {
        public final String URI;
        public final Context CONTEXT;
        public final String FILENAME;
        public final ParseStrategy<T> PARSER;
        public final SynchronisedObject<T> TARGET;
        public final SyncCallback CALLBACK;

        private final boolean FETCH_FROM_NETWORK;
        private final boolean USE_STORAGE;
        private final boolean SHOULD_PARSE;

        /**
         * Constructor for a single synchronisation task.
         * This can be passed to a Synchroniser in order to be performed.
         *
         * @param uri Network URI to load the data from. If it is provided, the method will download
         *            data from this URI, and save it to offline storage. Then it will parse it and
         *            pass to target object. If it is null or an empty string, data will be loaded
         *            from offline storage. The data will also be downloaded from offline if network
         *            is not available.
         * @param context Context of the app. This is used to access correct part of device storage.
         *                If it is null, no loading or saving the data from or onto offline storage
         *                will happen.
         * @param filename Name of the file to save the data into. If it is null or an empty string,
         *                 the data will not be loaded or stored to/from offline storage.
         * @param parser Parser that is to parse the data and construct a type of object that the
         *               target can accept. If it is null, or no data was retrieved, the method
         *               stops execution and does not pass any data to the target.
         * @param target Target object to pass the parsed data to. If it is null, no data will be
         *               parsed by the parser.
         * @param callback Object that will get notified of the status of the synchronisation.
         */
        public SynchronisationTask(String uri, Context context, String filename,
                                   ParseStrategy<T> parser, SynchronisedObject<T> target,
                                   SyncCallback callback) {
            this.URI      = uri;
            this.CONTEXT  = context;
            this.FILENAME = filename;
            this.TARGET   = target;
            this.PARSER   = parser;
            this.CALLBACK = callback;

            // Download from online if the URI was provided, and if the device is online.
            this.FETCH_FROM_NETWORK = (
                    this.URI != null && !this.URI.equals("") &&
                    this.CONTEXT != null && AndroidSpecific.isNetworkAvailable(context)
                    );

            // Only perform the parsing and updating the target if both the parser and the target
            // are present.
            this.SHOULD_PARSE = (this.PARSER != null && this.TARGET != null);

            // Perform storage manipulations only if the filename and context were provided.
            this.USE_STORAGE =
                    (this.CONTEXT != null && this.FILENAME != null && !this.FILENAME.equals(""));
        }

        /**
         * Asynchronous method. Performs a synchronisation. See {@link Synchroniser#synchronise(
         * String, Context, String, ParseStrategy, SynchronisedObject, SyncCallback)} for more
         * details on the process.
         */
        public void perform() {

            // Priority #1 - network use.
            if (this.FETCH_FROM_NETWORK) {
                // Download from the network and handle the data elsewhere (see SyncCallback).
                NetworkConnector.getInstance().fetchFromURI(this.CONTEXT, this.URI, this);

            } else if (this.USE_STORAGE && this.SHOULD_PARSE) {
                // If no network, load data from offline.
                // Always leads to a callback.
                String data = AndroidFileIO.getInstance().readFile(this.FILENAME, this.CONTEXT);

                if (data == null) {
                    // If the data was not loaded, callback and notify the object of failing to
                    // fetch data from offline storage.
                    Log.e(AndroidSpecific.getLogTag(this), "Failed to load from file IO.");
                    this.callback(SyncCallback.Status.FAIL_OFFLINE_FETCH);
                } else {
                    // If the data was loaded, parse it and update the target.
                    // This always performs a callback.
                    this.parseAndUpdateTarget(data);
                }
            } else {
                // Callback no matter what.
                Log.w(AndroidSpecific.getLogTag(this), "Loading skipped.");
                this.callback(SyncCallback.Status.NO_CHANGE);
            } // end if
        }

        /**
         * This onSync is used to handle data from the network to send to the target.
         * If online loading has failed, will try to load from offline.
         *
         * @param e Exception that might have been thrown during the data fetch.
         * @param response Response with data to process.
         */
        public void onCompleted(Exception e, Response<String> response) {

            if (e != null) {
                // If an exception was thrown, log the fault, and make a failure onSync.
                Log.e(AndroidSpecific.getLogTag(this),
                        e.getClass().getName() + ": " + e.getMessage());


                this.callback(SyncCallback.Status.FAIL_ONLINE_FETCH);

            } else if (response.getHeaders().code() >= 400) {
                // If the response code was over 400, it means that it was an error.
                // Log the fault, and make a failure onSync.
                Log.e(AndroidSpecific.getLogTag(this),
                        "Error HTTP code of " + response.getHeaders().code());

                this.callback(SyncCallback.Status.FAIL_ONLINE_FETCH);

            } else if (response.getResult() == null) {
                // If the result was null, it was an error and should be logged as such.
                Log.e(AndroidSpecific.getLogTag(this), "No response result received.");

                this.callback(SyncCallback.Status.FAIL_ONLINE_FETCH);
            } else {
                // If the data was loaded, store it offline and then parse it and update the target.

                // Log if failed to store the data.
                if (!AndroidFileIO.getInstance().writeFile(this.FILENAME, response.getResult(),
                                                            this.CONTEXT)) {
                    Log.e(AndroidSpecific.getLogTag(this),
                            "Failed to write to file " + this.FILENAME);
                }

                // This always makes a callback.
                this.parseAndUpdateTarget(response.getResult());
            } // end if
        }

        /**
         * Parses the provided data using the parser and sends the result to the target.
         * Always makes a callback.
         *
         * @param data Data to parse and pass to the target. Assumed to be not null.
         */
        private void parseAndUpdateTarget(String data) {
            ArrayList<T> targetUpdate = this.PARSER.parse(data);

            // Try to get the updated objects. If failed, callback of failure.
            if (targetUpdate == null) {
                Log.e(AndroidSpecific.getLogTag(this), "Failed to parse data: " + data);
                this.callback(SyncCallback.Status.FAIL_PARSE);
            } else {
                // Else, pass the data to the target and make the success callback.
                this.TARGET.updateEntries(this.PARSER.parse(data));
                this.callback(SyncCallback.Status.SUCCESS);
            }
        }


        /**
         * Performs a callback on the callback object with provided status.
         * Assumes that the status is never null. If the callback object is null, ignores the call.
         */
        private void callback(SyncCallback.Status status) {
            if (this.CALLBACK != null) {
                this.CALLBACK.onSync(status);
            }
        }
    } // end class SynchronisationTask
}

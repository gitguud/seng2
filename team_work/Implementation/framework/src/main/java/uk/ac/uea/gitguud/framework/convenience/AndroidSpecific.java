package uk.ac.uea.gitguud.framework.convenience;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;

/**
 * AndroidSpecific.java
 *
 * Contains helpful Android specific functions.
 * Created by Pavel Solodilov on 05/11/16.
 */
public class AndroidSpecific {

    /**
     * Gets an appropriate tag name of the class for Android logging.
     * <p>
     * Uses the object class's name for logging.
     *
     * @param object Object to get the tag name for.
     * @param <T> Any type of object will work.
     * @return Tag name for Android logging.
     */
    public static <T> String getLogTag(T object) {
        return object.getClass().getName();
    }

    /**
     * Checks whether the device is connected to network or not.
     *
     * @param context Context of the application to access the network state.
     * @return True if network is available, and false otherwise.
     */
    public static boolean isNetworkAvailable(Context context) {

        // Get the connectivity manager and active network information.
        ConnectivityManager cm =
                (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();

        // Access to network is determined by availability of the network information and whether
        // it is connected or not.
        return (netInfo != null && netInfo.isConnected());
    }

    /**
     * Creates and displays a short toast.
     *
     * @param context Context to display the toast in.
     * @param message Message to display.
     */
    public static void shortToast(Context context, String message) {
        showToast(context, message, Toast.LENGTH_SHORT);
    }

    /**
     * Creates a displays a short toast.
     *
     * @param context Context to display the toast in.
     * @param messageId ID of the string containing the message.
     */
    public static void shortToast(Context context, int messageId) {
        String message = context.getResources().getString(messageId);
        shortToast(context, message);
    }

    /**
     * Creates and displays a toast.
     *
     * @param context Context to display the toast in.
     * @param message Message to display.
     * @param length Length of the toast (e.g. Toast.LENGTH_SHORT)
     */
    public static void showToast(Context context, String message, int length) {
        Toast.makeText(context, message, length).show();
    }

    /**
     * Reads a string from resource file as a whole. Do not use for big files.
     *
     * @param context Context of the app containing the raw resource text file.
     * @param resourceId The ID of the text file (e.g. xml).
     * @return String of textual data that is contained within the file.
     * @throws IOException if the file could not be opened or read from.
     */
    public static String stringFromRawResource(Context context, int resourceId) throws IOException {

        InputStream stream = context.getResources().openRawResource(resourceId);

        byte[] buffer = new byte[stream.available()];
        stream.read(buffer);
        stream.close();
        return new String(buffer);
    }
}

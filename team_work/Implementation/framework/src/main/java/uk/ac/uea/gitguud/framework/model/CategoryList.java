package uk.ac.uea.gitguud.framework.model;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Pair;


import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Map;

import uk.ac.uea.gitguud.framework.SynchronisedObject;
import uk.ac.uea.gitguud.framework.convenience.ErrorChecking;

import static uk.ac.uea.gitguud.framework.convenience.AndroidSpecific.shortToast;

/**
 * CategoryList.java
 *
 * Models a list of categories of points of interest. Note that the names of the categories are
 * case-insensitive.
 * Created by Pavel Solodilov on 19/11/16.
 */
public class CategoryList extends SynchronisedObject<Pair<String, ArrayList<PointOfInterest>>> {
    private Hashtable<String, ArrayList<PointOfInterest>> categories;

    private static CategoryList instance;

    /**
     * Accessor for a singleton instance of category list.
     *
     * @return Instance.
     */
    public static CategoryList getInstance() {
        if (CategoryList.instance == null) {
            CategoryList.instance = new CategoryList();
        }

        return CategoryList.instance;
    }


    /**
     * Default constructor.
     */
    private CategoryList() {
        this.categories = new Hashtable<>();
    }

    /**
     * Adds all of the categories from the provided collection into this category list, merging
     * their entries.
     *
     * @param categories Categories to load (add or update). Does nothing on null.
     * @param context Context of the application in order to show toast on error.
     * @return This category list for chaining.
     */
    @NonNull
    public CategoryList loadCategories(Collection<Pair<String,
            ArrayList<PointOfInterest>>> categories, Context context) throws NullPointerException {

        if (categories != null) {
            this.updateEntries(new ArrayList<Pair<String, ArrayList<PointOfInterest>>>(categories));
        } else {
            shortToast(context, "Failed to load categories");
        }

        return this;
    }

    /**
     * Adds or updates a category in the category list. It will be updated if it already exists by
     * merging them together: updating the present points of interest and adding new ones. Note that
     * the points of interest will always be sorted by name.
     *
     * @param category Name of the category to add. Note that the names of the categories are
     *                 case-insensitive.
     * @param pois Points of interest to add to the category. If null, no points of interest will be
     *             added. Likewise, if the list is null, it will not be added.
     * @return This category list for chaining.
     * @throws NullPointerException if the provided category name is null.
     * @throws IllegalArgumentException if the provided category name is an empty string.
     */
    public boolean addOrUpdateCategory(String category, ArrayList<PointOfInterest> pois) {

        ErrorChecking.getInstance()
                .throwOnNull(category, "The category name cannot be null.")
                .throwOnEmptyString(category, "The category cannot be an empty string.");

        // If no POIs were provided, done.
        if (pois == null || pois.isEmpty()) {
            return false;
        }

        // Case insensitive: all caps.
        category = category.toUpperCase();

        // Try getting the category.
        ArrayList<PointOfInterest> thisPois = this.categories.get(category);

        // If no such category exists, add it.
        if (thisPois == null) {
            this.categories.put(category, pois);
            return true;
        }

        Comparator<PointOfInterest> cmpByName = new PointOfInterest.CompareByName();

        // Merge-add new points of interest.
        ArrayList<PointOfInterest> toAdd = new ArrayList<>();
        for (PointOfInterest newPoi : pois) {

            // Try finding a match.
            int foundAt = -1;
            int index=0;
            for(PointOfInterest pointOfInterestToSearchFor: thisPois) {
                if (pointOfInterestToSearchFor.getName().toUpperCase().
                        equals(newPoi.getName().toUpperCase())) {
                    foundAt=index;
                    break;
                }
                index++;
            }

            // If found, update the info.
            /// If not found, simply set to be added.
            if (foundAt >= 0) {
                thisPois.get(foundAt).updateWith(newPoi);
            } else {
                toAdd.add(newPoi);
            }
        } // end for each

        // Finally, add all of the new entries and sort the array list by name.
        thisPois.addAll(toAdd);
        Collections.sort(thisPois, cmpByName);
        return false;
    }

    /**
     * Removes a category from the category list.
     * Nothing will happen if it is not there.
     *
     * @param category Name of the category to remove. Note that the names of the categories are
     *                 case-insensitive.
     * @return True if the category has been removed, and false if it was not there to begin with.
     * @throws NullPointerException if the provided category name is null;
     */
    public boolean removeCategory(String category) {
        ErrorChecking.getInstance().throwOnNull(category, "The category name cannot be null.");
        category = category.toUpperCase();
        return this.categories.remove(category) != null;
    }

    /**
     * Accessor for all of the categories.
     *
     * @return Names of all of the categories; unsorted.
     */
    @NonNull
    public Enumeration<String> getCategories() {
        return this.categories.keys();
    }

    /**
     * Accessor for all points of interest of a category.
     *
     * @param category Name of the category to retrieve points of interest from. Note that the names
     *                 of the categories are case-insensitive.
     * @return Points of interest of provided category. Note that all of the contained points of
     *         interest are sorted by name {@link PointOfInterest.CompareByName}. It might be null
     *         if such a category does not exist.
     * @throws NullPointerException if the provided category name is null.
     */
    @Nullable
    public PointOfInterest[] getPointsOfInterest(String category) {

        // Get the points of interest of the category. If not found, return null.
        ArrayList<PointOfInterest> pois = getPointsOfInterestAsArrayList(category);
        if (pois == null) return null;

        // Convert the array list into an array and return it.
        PointOfInterest[] poisArray = new PointOfInterest[pois.size()];
        poisArray = pois.toArray(poisArray);
        return poisArray;
    }

    /**
     * Accessor for all points of interest of a category.
     *
     * @param category Name of the category to retrieve points of interest from. Note that the names
     *                 of the categories are case-insensitive.
     * @return Points of interest of provided category. Note that all of the contained points of
     *         interest are sorted by name {@link PointOfInterest.CompareByName}. It might be null
     *         if such a category does not exist.
     * @throws NullPointerException if the provided category name is null.
     */
    @Nullable
    public ArrayList<PointOfInterest> getPointsOfInterestAsArrayList(String category) {
        ErrorChecking.getInstance().throwOnNull(category, "The category name cannot be null.");

        category = category.toUpperCase();

        // Get the points of interest of the category. If not found, return null.
        ArrayList<PointOfInterest> pois = this.categories.get(category);
        if (pois == null) return null;

        return new ArrayList<>(pois);
    }


    /**
     * Adds a point of interest in the category.
     * Replaces the values of the existing POI if it already existed in this category.
     *
     * @param category Name of the category to add the POI to. Note that the names
     *                 of the categories are case-insensitive.
     * @param poi Point of interest to add.
     * @return True if the point of interest has been added successfully, and false if it was a
     *         duplicate and has not been updated with or such a category was not found.
     * @throws NullPointerException if either of the provided objects are null.
     */
    public boolean addOrUpdatePointOfInterest(String category, PointOfInterest poi) {
        ErrorChecking.getInstance()
                .throwOnNull(category, "The category name cannot be null.")
                .throwOnNull(poi, "The point of interest cannot be null.");

        // Find a category using case-insensitive search.
        category = category.toUpperCase();
        ArrayList<PointOfInterest> pois = this.categories.get(category);

        // If such a category does not exist, return false.
        if (pois == null) return false;

        // Try finding a matching POI.
        Comparator<PointOfInterest> cmp = new PointOfInterest.CompareByName();
        int index = Collections.binarySearch(pois, poi, cmp);

        // If a match has been found, update it.
        /// Otherwise add the new POI and sort the array list.
        if (index >= 0) {
            pois.get(index).updateWith(poi);
            return false;
        } else {
            pois.add(poi);
            Collections.sort(pois, cmp);
            return true;
        }
    }

    /**
     * Removes a point of interest from the category.
     * Does nothing if the POI was not there.
     *
     * @param category Name of the category to remove the POI from. Note that the names of the
     *                 categories are case-insensitive.
     * @param poi Point of interest to remove. For matching criteria see
     *            {@link PointOfInterest#equals(Object)}.
     * @return True if the point of interest has been removed successfully,
     *         and false if it has not been present in the category.
     * @throws NullPointerException if either of the objects are null.
     */
    public boolean removePointOfInterest(String category, PointOfInterest poi) {
        ErrorChecking.getInstance()
                .throwOnNull(category, "The name of the category cannot be null.")
                .throwOnNull(poi, "The point of interest cannot be null.");

        ArrayList<PointOfInterest> allPois = this.categories.get(category.toUpperCase());

        if (allPois == null) return false;

        Comparator<PointOfInterest> cmp = new PointOfInterest.CompareByName();
        int index = Collections.binarySearch(allPois, poi, cmp);

        if (index >= 0) {
            allPois.remove(index);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Removes everything inside the hashtable: all categories and every POI associated with them.
     */
    public void removeEverything(){
        categories.clear();
        this.changeNotify();
    }

    /**
     * Finds a point of interest by its name.
     *
     * @param category Category to search for this POI from. Note that the names of the categories
     *                 are case-insensitive.
     * @param name Name of the POI to look for. Case-insensitive.
     * @return POI with matching name. Null if the POI or the category was not found.
     * @throws NullPointerException If either of the provided objects are null.
     */
    public PointOfInterest findPOIByName(String category, String name) {
        ErrorChecking.getInstance().throwOnNull(name, "The name of a POI cannot be null.");

        // This will handle the exception throwing and case-insensitivity.
        PointOfInterest[] allPois = this.getPointsOfInterest(category);

        if (allPois == null) return null;

        for (PointOfInterest poi : allPois) {
            if (poi.getName().equalsIgnoreCase(name)) {
                return poi;
            }
        } // end for each

        return null;
    }

    /**
     * Returns all of the POIs whose names (case-insensitive) contain the provided string.
     * Searches all of the categories.
     *
     * @param searchSeq The search sequence. If it is an empty string, simply returns the first
     *                  {limit} encountered POIs.
     * @param limit Maximum amount of entries to return. Leave 0 or negative for unlimited.
     * @return All points of interest whose names contain the provided string. Note that these might
     *         mostly be from a single category and might not be sorted. The first element of the
     *         pair is the name of the category this point of interest is located in.
     * @throws NullPointerException if the provided string is null.
     */
    public ArrayList<Pair<String, PointOfInterest>> searchPOIByName(String searchSeq, int limit) {
        ErrorChecking.getInstance().throwOnNull(searchSeq, "The search sequence cannot be null.");

        // If the limit is 0 or negative, simply set the limit to max integer value.
        if (limit <= 0) limit = Integer.MAX_VALUE;

        // Pre-allocate the result array list.
        ArrayList<Pair<String, PointOfInterest>> result = new ArrayList<>();

        // Case-insensitive search sequence.
        searchSeq = searchSeq.toUpperCase();
        String poiName;

        // Go thorough all of the points of interests in all of the categories.
        for (Map.Entry<String, ArrayList<PointOfInterest>> thisPois : this.categories.entrySet()) {
            for (PointOfInterest poi : thisPois.getValue()) {

                // If reached the limit, return.
                if (result.size() >= limit) {
                    return result;
                }

                // Case-insensitive POI name.
                poiName = poi.getName().toUpperCase();

                if (poiName.contains(searchSeq)) {
                    result.add(new Pair<>(thisPois.getKey(), poi));
                }
            } // end for each POI
        } // end for each category

        return result;
    }

    /**
     * The update of entries is incredibly simple: add any new entries, update any of the matching
     * entries, and retain all of the others.
     *
     * @param newEntries New set of categories to add.
     */
    @Override
    public void updateEntries(ArrayList<Pair<String, ArrayList<PointOfInterest>>> newEntries) {
        // Go through each of the new categories, and add or update all of them.
        for (Pair<String, ArrayList<PointOfInterest>> newCategory : newEntries) {
            this.addOrUpdateCategory(newCategory.first, newCategory.second);
        }

        this.changeNotify();
    }
}

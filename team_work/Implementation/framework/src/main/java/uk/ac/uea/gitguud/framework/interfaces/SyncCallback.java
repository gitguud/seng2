package uk.ac.uea.gitguud.framework.interfaces;

/**
 * SyncCallback.java
 *
 * This interface is used to mark a class that can react to callbacks from the Synchroniser class.
 * Created by Pavel Solodilov on 07/11/16.
 */
public interface SyncCallback {
    enum Status {
        FAIL_ONLINE_FETCH,
        FAIL_OFFLINE_FETCH,
        FAIL_PARSE,
        NO_CHANGE,
        SUCCESS
    }

    /**
     * A callback after sync attempt for an action.
     *
     * @param status Status of the sync operation.
     */
    void onSync(Status status);
}

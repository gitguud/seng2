package uk.ac.uea.gitguud.framework;

import org.junit.Test;

import java.util.Calendar;
import java.util.Date;

import uk.ac.uea.gitguud.framework.convenience.TimeSpan;

import static junit.framework.Assert.fail;

/**
 * TimeSpanTest.java
 *
 * Unit test suite to test the TimeSpan class' functionality.
 * Created by Pavel Solodilov on 21/11/16.
 */
public class TimeSpanTest {

    private Calendar cal = Calendar.getInstance();
    private Date start = createDate(2016, 11, 10, 10, 30);
    private Date end = createDate(2016, 11, 10, 11, 30);

    private Date createDate(int year, int month, int date, int hour, int minutes) {
        this.cal.set(year, month, date, hour, minutes);
        return this.cal.getTime();
    }


    @Test(expected = NullPointerException.class)
    public void testSetStartAndEndToNull() {
        System.out.println("Test TimeSpan(null, null)");
        new TimeSpan(null, null);
    }

    @Test(expected = NullPointerException.class)
    public void testSetStartToNull() {
        System.out.println("Test TimeSpan(null, Date)");
        new TimeSpan(null, end);
    }

    @Test(expected = NullPointerException.class)
    public void testSetEndToNull() {
        System.out.println("Test TimeSpan(Date, null)");
        new TimeSpan(start, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSetStartAfterEnd() {
        System.out.println("Test TimeSpan(end, start)");
        new TimeSpan(end, start);
    }

    @Test
    public void testCreateTimeSpan() {
        System.out.println("Test TimeSpan(start, end)");
        new TimeSpan(start, end);
    }


    @Test
    public void testGetStartTime() {
        System.out.println("Test getStartTime()");
        TimeSpan instance = new TimeSpan(start, end);

        if (start == instance.getStartTime()) {
            fail("The time span is supposed to copy its inner start time whenever returning it.");
        } else if (!start.equals(instance.getStartTime())) {
            fail("The time span returns wrong start time.");
        }
    }

    @Test
    public void testGetEndTime() {
        System.out.println("Test getEndTime()");
        TimeSpan instance = new TimeSpan(start, end);

        if (end == instance.getEndTime()) {
            fail("The time span is supposed to copy its inner end time whenever returning it.");
        } else if (!end.equals(instance.getEndTime())) {
            fail("The time span returns wrong end time.");
        }
    }
}

package uk.ac.uea.gitguud.framework;

import junit.framework.Assert;

import org.junit.Test;

import java.util.Calendar;
import java.util.Date;

import uk.ac.uea.gitguud.framework.convenience.ErrorChecking;

/**
 * Created by Anastassia Segeda on 1/26/2017.
 */

public class ErrorCheckingTest {

    private Calendar cal = Calendar.getInstance();
    private Date start = createDate(2016, 11, 10, 10, 30);
    private Date end = createDate(2016, 11, 10, 11, 30);

    private Date createDate(int year, int month, int date, int hour, int minutes) {
        this.cal.set(year, month, date, hour, minutes);
        return this.cal.getTime();
    }


    @Test
    public void startDateBeforeEndDate(){
        ErrorChecking.getInstance().startDateBeforeEndDate(start, end, false);
        Assert.assertTrue(true);
    }

    @Test
    public void startDateBeforeEndDateTrue(){
        ErrorChecking.getInstance().startDateBeforeEndDate(end, start, false);
        Assert.assertTrue(true);
    }

    @Test
    public void startDateBeforeEndDateEqualFalse(){
        ErrorChecking.getInstance().startDateBeforeEndDate(end, end, false);
        Assert.assertTrue(true);
    }

    @Test
    public void startDateBeforeEndDateEqualTrue(){
        ErrorChecking.getInstance().startDateBeforeEndDate(end, end, true);
        Assert.assertTrue(true);
    }

    @Test (expected = NullPointerException.class)
    public void throwOnNullNullObject(){
        ErrorChecking.getInstance().throwOnNull(null, "msg");

    }

    @Test
    public void throwOnNull(){
        Object obj = new Object();
        ErrorChecking.getInstance().throwOnNull(obj, "msg");

    }

    @Test(expected = IllegalArgumentException.class)
    public void throwOnEmptyStringEmptyString(){
        ErrorChecking.getInstance().throwOnEmptyString("", "message");
    }

    @Test
    public void throwOnEmptyString(){
        ErrorChecking.getInstance().throwOnEmptyString("lalala", "message");
    }

}

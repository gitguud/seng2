package uk.ac.uea.gitguud.framework;

import org.junit.Assert;
import org.junit.Test;

import java.util.Calendar;
import java.util.Date;

import java.text.ParseException;
import java.util.Locale;
import java.util.TimeZone;

import uk.ac.uea.gitguud.framework.convenience.DateParser;

import static junit.framework.Assert.fail;


/**
 * Created by Anastassia Segeda on 1/26/2017.
 */

public class DateParserTest {

    private Calendar cal = Calendar.getInstance();
    private Date createDate(int year, int month, int date, int hour, int minutes, int sec) {
        this.cal.set(year, month, date, hour, minutes, sec);
        cal.setTimeZone(TimeZone.getTimeZone("GMT"));
        return this.cal.getTime();
    }

    //fails because of milliseconds
    private boolean equalDates(Date d1, Date d2) {
        if (d1.getTimezoneOffset() != d2.getTimezoneOffset()) {
            return false;
        } else if(d1.getMinutes() != d2.getMinutes()){
            return false;
        } else if (d1.getHours() != d2.getHours()){
            return false;
        } else if (d1.getSeconds() != d2.getSeconds()){
            return false;
        } else if (d1.getMonth() != d2.getMonth()){
            return false;
        } else if (d1.getYear() != d2.getYear()){
            return false;
        }
        return true;
    }

        /* Allowed formats are (21st November 2016, 12:02:30 UTC).
            *  2016-11-21T12:02:30+00.00
            *  2016-11-21T12:02:30Z
            *  20161121T120230Z*/

    @Test
    public void toISO8601() throws ParseException {
        String format3 = "1995-12-12T15:12:00+00.00";
        String format2 = "1995-12-12T15:12:00Z";
        String format1 = "19951212T151200Z";
        Locale locale = new Locale("UTC");

        Date expDate1 = createDate(1995, 11, 12, 15, 12, 00);

        Date date1 = DateParser.toISO8601(format1, locale);
        if(!equalDates(date1, expDate1)){
            fail("Dates must be equal");
        }

        Date date2 = DateParser.toISO8601(format2, locale);
        if(!equalDates(date2, expDate1)){
            fail("Dates must be equal");
        }


        Date date3 = DateParser.toISO8601(format3, locale);
        if(!equalDates(date3, expDate1)){
            fail("Dates must be equal");
        }


    }

    @Test(expected = ParseException.class)
    public void toISO8601Exception() throws ParseException {
        String format3 = "1995-1212T15:12:00+00.00";
        String format2 = "1995-12-1215:12:00Z";
        String format1 = "19951212T151200";
        Locale locale = new Locale("UTC");

        Date date1 = DateParser.toISO8601(format1, locale);
        Date date2 = DateParser.toISO8601(format2, locale);
        Date date3 = DateParser.toISO8601(format3, locale);

    }
}
